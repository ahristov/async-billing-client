﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.RateCardV3.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Corp.Billing.Shared.ApiClient.ProductTokens
{
    public class ProductTokensToSellableApiClient : BaseApiClient, IProductTokensToSellableApiClient
    {
        public ProductTokensToSellableApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public GetProdTokensToSellableUnitsResponse GetProdTokensToSellableUnits(int userId, ICollection<string> productTokens)
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetProdTokensToSellableUnits + $"/?userId={userId}";

            string pt = "" + GetProducTokensParam(productTokens);
            if (!string.IsNullOrWhiteSpace(pt))
            {
                url = $"{url}&{pt}";
            }

            string apiResult = ApiSvc.WebRequestGet(url);

            var res = JsonConvert.DeserializeObject<GetProdTokensToSellableUnitsResponse>(apiResult);
            return res;
        }

        public GetProdTokensToSellableResponse GetProdTokensToSellable(int userId, ICollection<string> productTokens)
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetProdTokensToSellable + $"/?userId={userId}";

            string pt = "" + GetProducTokensParam(productTokens);
            if (!string.IsNullOrWhiteSpace(pt))
            {
                url = $"{url}&{pt}";
            }

            string apiResult = ApiSvc.WebRequestGet(url);

            var res = JsonConvert.DeserializeObject<GetProdTokensToSellableResponse>(apiResult);
            return res;
        }


        private static string GetProducTokensParam(ICollection<string> productTokens)
        {
            if (productTokens == null || productTokens.Count <= 0)
                return string.Empty;

            var paramName = "productTokens";
            var paramValue = JsonConvert.SerializeObject(productTokens);

            var res = paramName.GetNameValueQueryParameterPairs(paramValue);
            return res;
        }

    }
}
