﻿using Corp.Billing.Shared.Domain.RateCardV3.Services;
using System.Collections.Generic;

namespace Corp.Billing.Shared.ApiClient.ProductTokens
{
    public interface IProductTokensToSellableApiClient
    {
        GetProdTokensToSellableResponse GetProdTokensToSellable(int userId, ICollection<string> productTokens);
        GetProdTokensToSellableUnitsResponse GetProdTokensToSellableUnits(int userId, ICollection<string> productTokens);
    }
}
