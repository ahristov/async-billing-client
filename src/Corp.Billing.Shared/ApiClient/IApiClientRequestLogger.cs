﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Corp.Billing.Shared.ApiClient
{
    public interface IApiClientRequestHelper
    {
        void ProcessRequestHeaders(WebHeaderCollection headers);
        void LogRequest(string url, DateTime createdAt, TimeSpan duration, string request, string response);
    }
}
