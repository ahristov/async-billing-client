﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.RateCard;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.RateCard
{
    public class RateCardApiClient : BaseApiClient, IRateCardService
    {
        public RateCardApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public RateCardBaseResult GetBaseRateCard(RateCardBaseRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetBaseRateCardPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<RateCardBaseResult>(apiResult);
            return result;
        }

        public RateCardAddOnResult GetAddOnRateCard(RateCardAddOnRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetAddOnsRateCardPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<RateCardAddOnResult>(apiResult);
            return result;
        }


        public UnitRateCardResult GetUnitRateCard(UnitRateCardRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetUnitRateCardPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<UnitRateCardResult>(apiResult);
            return result;
        }
    }
}
