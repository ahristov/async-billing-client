﻿using Corp.Billing.Shared.Domain.BillingOrigins.Data;
using System;

namespace Corp.Billing.Shared.ApiClient.BillingOrigins
{
    public interface IBillingOriginsApiClient
    {
        void SetBillingOrigins(SetBillingOriginsRequest request);
        BillingOriginsData GetBillingOriginData(int userId, Guid sessionId);
    }
}
