﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain.PaymentV3.Data.Request;
using Corp.Billing.Shared.Domain.PaymentV3.Data.Response;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.PaymentV3
{
    public class OrderPaymentApiClient : BaseApiClient, IOrderPaymentApiClient
    {
        public OrderPaymentApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public OrderPaymentResponse ProcessPayment(OrderPaymentRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Payment.V3.OrderPayment;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<OrderPaymentResponse>(apiResult);
            return result;
        }
    }
}
