﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Newtonsoft.Json;
using System;
using RequestContracts = Corp.Billing.Shared.Domain.PaymentV3.Data.Request.Events;
using ResponseContracts = Corp.Billing.Shared.Domain.PaymentV3.Data.Response;

namespace Corp.Billing.Shared.ApiClient.PaymentV3
{
    public class EventsPaymentApiClient : BaseApiClient, IEventsPaymentApiClient
    {
        public EventsPaymentApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        /// <summary>
        /// Process event paymets with generic contract.
        /// </summary>
        /// <param name="request">Payment request that holds payment type</param>
        /// <returns>Payment response</returns>
        public ResponseContracts.PaymentResponse ProcessPayment(RequestContracts.PaymentRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Payment.V3.EventsPayment;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<ResponseContracts.PaymentResponse>(apiResult);
            return result;
        }

        [Obsolete]
        public ResponseContracts.PaymentResponse ProcessPayment(RequestContracts.RequestWithSecuredPaymentToken request)
        {
            string url = ServiceUrl + ApiPathsV1.Payment.V3.EventsCreditCardPaymentWithSecuredPaymentToken;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<ResponseContracts.PaymentResponse>(apiResult);
            return result;
        }

        [Obsolete]
        public ResponseContracts.PaymentResponse ProcessPayment(RequestContracts.RequestWithPaymentMethodToken request)
        {
            string url = ServiceUrl + ApiPathsV1.Payment.V3.EventsCreditCardPaymentWithPaymentMethodToken;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<ResponseContracts.PaymentResponse>(apiResult);
            return result;
        }

        [Obsolete]
        public ResponseContracts.PaymentResponse ProcessPaymentWithDefaultPaymentMethod(RequestContracts.RequestWithDefaultPaymentMethod request)
        {
            string url = ServiceUrl + ApiPathsV1.Payment.V3.EventsOneClickPaymentWithDefaultPaymentMethod;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<ResponseContracts.PaymentResponse>(apiResult);
            return result;
        }
    }
}
