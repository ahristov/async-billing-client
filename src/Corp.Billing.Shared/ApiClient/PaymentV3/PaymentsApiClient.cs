﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Newtonsoft.Json;
using RequestContracts = Corp.Billing.Shared.Domain.PaymentV3.Data.Request.OneClickPayment;
using ResponseContracts = Corp.Billing.Shared.Domain.PaymentV3.Data.Response;

namespace Corp.Billing.Shared.ApiClient.PaymentV3
{
    public class PaymentsApiClient : BaseApiClient, IPaymentsApiClient
    {
        public PaymentsApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public ResponseContracts.PaymentResponse ProcessPaymentWithDefaultPaymentMethod(RequestContracts.OneClickPaymentRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Payment.V3.OneClickPaymentWithDefaultPaymentMethod;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<ResponseContracts.PaymentResponse>(apiResult);
            return result;
        }
    }
}
