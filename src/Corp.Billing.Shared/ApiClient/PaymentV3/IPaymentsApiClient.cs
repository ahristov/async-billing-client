﻿using RequestContracts = Corp.Billing.Shared.Domain.PaymentV3.Data.Request.OneClickPayment;
using ResponseContracts = Corp.Billing.Shared.Domain.PaymentV3.Data.Response;

namespace Corp.Billing.Shared.ApiClient.PaymentV3
{
    public interface IPaymentsApiClient
    {
        ResponseContracts.PaymentResponse ProcessPaymentWithDefaultPaymentMethod(RequestContracts.OneClickPaymentRequest request);
    }
}
