﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain.Payments;
using Corp.Billing.Shared.ProxyContracts;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Payments
{
    public class AMWSPaymentApiClient : BaseApiClient, IAMWSPaymentApiClient
    {

        public AMWSPaymentApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public AMWSPaymentResponseMessage Consent(AMWSPaymentRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Payment.Amazon.Consent;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<AMWSPaymentResponseMessage>(apiResult);
            return result;
        }
    }
}
