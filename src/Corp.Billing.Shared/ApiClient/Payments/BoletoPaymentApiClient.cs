﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain.Payments;
using Corp.Billing.Shared.ProxyContracts;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Payments
{

    public class BoletoPaymentApiClient : BaseApiClient, IBoletoPaymentService
    {
        public BoletoPaymentApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public virtual OfflinePaymentResponseMessage Authorize(OfflinePaymentRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.Boleto.BoletoPayment + "/" + ApiPathsV1.Account.Payment.Boleto.BoletoPaymentAuth;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<OfflinePaymentResponseMessage>(apiResult);
            return result;
        }

        public virtual OfflinePaymentResponseMessage ProcessPayment(OfflinePaymentRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.Boleto.BoletoPayment + "/" + ApiPathsV1.Account.Payment.Boleto.BoletoPaymentProcess;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<OfflinePaymentResponseMessage>(apiResult);
            return result;
        }

        public virtual string GetInfo(int trxId)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.Boleto.BoletoPayment + "/" 
                + ApiPathsV1.Account.Payment.Boleto.BoletoPaymentInfo 
                + "?trxId=" + trxId;
            var response = ApiSvc.WebRequestGet(url);
            return response;
        }
    }
}
