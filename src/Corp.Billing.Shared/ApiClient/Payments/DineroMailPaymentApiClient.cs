﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ProxyContracts;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Payments
{
    public class DineroMailPaymentApiClient : BaseApiClient, IDineroMailPaymentService
    {
        public DineroMailPaymentApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public virtual OfflinePaymentResponseMessage Authorize(OfflinePaymentRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.DineroMail.DineroMailPayment + "/" + ApiPathsV1.Account.Payment.DineroMail.DineroMailPaymentAuth;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<OfflinePaymentResponseMessage>(apiResult);
            return result;
        }

        public virtual OfflinePaymentResponseMessage ProcessPayment(DineroMailOfflinePaymentRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.DineroMail.DineroMailPayment + "/" + ApiPathsV1.Account.Payment.DineroMail.DineroMailPaymentProcess;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<OfflinePaymentResponseMessage>(apiResult);
            return result;
        }

        public virtual OfflinePaymentResponseMessage ProcessPaymentNotification(OfflinePaymentRequestMessage request)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.DineroMail.DineroMailPayment + "/" + ApiPathsV1.Account.Payment.DineroMail.DineroMailPaymentProcessNotification;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<OfflinePaymentResponseMessage>(apiResult);
            return result;
        }
    }
}
