﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Promos;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Promos
{
    public class PromoApiClient : BaseApiClient, IPromoService
    {
        public PromoApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        // Promos

        public PromoEligibilityResult GetPromoEligibility(PromoEligibilityRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Promos.GetPromoEligibilityPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<PromoEligibilityResult>(apiResult);
            return result;
        }

        public PromoValidateRedemptionKeyResult ValidateRedemptionKey(PromoValidateRedemptionKeyRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Promos.ValidateRedemptionKeyPath;
            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<PromoValidateRedemptionKeyResult>(apiResult);
            return result;
        }

        public PromoValidationResult GetValidation(PromoValidationRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Promos.GetPromoValidationPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<PromoValidationResult>(apiResult);
            return result;
        }

        public PromoGetInfoResult GetInfo(PromoGetInfoRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Promos.GetPromoInfoPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<PromoGetInfoResult>(apiResult);
            return result;
        }

        public GetPromoDataResult GetPromoData(GetPromoDataRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Promos.GetPromoDataPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<GetPromoDataResult>(apiResult);
            return result;
        }


        // Gifts

        public GiftRedeemResult RedeemGift(GiftRedeemRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Promos.Gift.RedeemGift;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<GiftRedeemResult>(apiResult);
            return result;
        }

        public GiftValidateKeysResult ValidateGiftKeys(GiftValidateKeysRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Promos.Gift.ValidateGiftKeys + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<GiftValidateKeysResult>(apiResult);
            return result;
        }
    }
}
