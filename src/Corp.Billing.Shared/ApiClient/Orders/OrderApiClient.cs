﻿using System;
using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Orders;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Orders
{
    public class OrderApiClient : BaseApiClient, IOrderService
    {
        public OrderApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }


        public CardinalLogInsertResult CardinalLogInsert(CardinalLogInsertRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.CardinalLogInsertPath;
            var apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<CardinalLogInsertResult>(apiResult);
            return result;
        }

        public OrderDiscountsResult GetOrderDiscounts(OrderDiscountsRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.GetOrderDiscountsPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<OrderDiscountsResult>(apiResult);
            return result;
        }

        public OneMonthFeatureCompareResult GetOneMonthFeatureCompare(OneMonthFeatureCompareRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.GetOneMonthFeatureComparePath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<OneMonthFeatureCompareResult>(apiResult);
            return result;
        }

        public OrderSummaryResult GetOrderSummary(OrderSummaryRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            if (string.IsNullOrEmpty(request.OrderList))
                throw new ArgumentException("OrderList parameter must be provided");

            string url = ServiceUrl + ApiPathsV1.Orders.GetOrderSummaryPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<OrderSummaryResult>(apiResult);
            return result;
        }

        public SubscribeResult CreateOrder(SubscribeRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.CreateOrderPath;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<SubscribeResult>(apiResult);
            return result;
        }

        public SubscribeResultV3 CreateOrderV3(SubscribeRequestV3 request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.CreateOrderPathV3;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<SubscribeResultV3>(apiResult);
            return result;
        }



        // Transaction

        public ProcessTransactionResult ProcessTransaction(ProcessTransactionRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.Transaction.ProcessTransactionPath;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<ProcessTransactionResult>(apiResult);
            return result;
        }

        public ProcessPayPalTransactionResult ProcessPayPalTransaction(ProcessPayPalTransactionRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.Transaction.ProcessPayPalTransactionPath;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<ProcessPayPalTransactionResult>(apiResult);
            return result;
        }

        public AppleProcessTransactionResult AppleProcessTransaction(AppleProcessTransactionRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.Transaction.AppleProcessTransactionPath;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<AppleProcessTransactionResult>(apiResult);
            return result;
        }

        public UpdateTrxStatusResult UpdateTransactionStatus(UpdateTrxStatusRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.Transaction.UpdateTransactionStatusPath;
            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<UpdateTrxStatusResult>(apiResult);
            return result;
        }

        public Domain.Orders.UpdateOfflinePaymentStatusResponse UpdateOfflinePaymentStatus(Domain.Orders.UpdateOfflinePaymentStatusRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.Transaction.UpdateOfflineTransactionStatusPath;
            string apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<Domain.Orders.UpdateOfflinePaymentStatusResponse>(apiResult);
            return result;
        }

        public GetOfflineTransferDataResult GetOfflineTransferData(GetOfflineTransferDataRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.Transaction.UpdateOfflineTransactionStatusPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<GetOfflineTransferDataResult>(apiResult);
            return result;
        }


        // Apple

        public AppleRetryQueueInsertResult AppleRetryQueueInsert(AppleRetryQueueInsertRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.Apple.AppleRetryQueueInsertPath;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<AppleRetryQueueInsertResult>(apiResult);
            return result;
        }

        public SearchForAppleOrderResult SearchForAppleOrder(SearchForAppleOrderRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.Apple.SearchForAppleOrderPath + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<SearchForAppleOrderResult>(apiResult);
            return result;
        }


        // Unit products

        public BillEventTicketsResult BillEventTickets(BillEventTicketsRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.UnitProducts.BillEventTicketsPath;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<BillEventTicketsResult>(apiResult);
            return result;
        }

        public ImpulseBuyLastPurchaseResult GetImpulseBuyLastPurchase(ImpulseBuyLastPurchaseRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.UnitProducts.GetImpulseBuyLastPurchase + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<ImpulseBuyLastPurchaseResult>(apiResult);
            return result;
        }


    }
}
