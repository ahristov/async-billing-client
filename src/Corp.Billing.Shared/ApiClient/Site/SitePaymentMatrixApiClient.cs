﻿using Corp.Billing.Shared.Domain.Site;
using System.Collections.Generic;
using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Site
{
    public class SitePaymentMatrixApiClient : BaseApiClient, ISitePaymentMatrixService
    {
        public SitePaymentMatrixApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public IList<SitePaymentMatrix.GetSitePaymentMatrixItem> GetSitePaymentMatrix(SitePaymentMatrix.GetSitePaymentMatrixRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Site.GetSitePaymentMatrix + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<List<SitePaymentMatrix.GetSitePaymentMatrixItem>>(apiResult);
            return result;
        }


        public IList<SitePaymentMatrix.GetSitePaymentMatrixItem> GetSitePaymentMatrixAll()
        {
            string url = ServiceUrl + ApiPathsV1.Site.GetSitePaymentMatrixAll;
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<List<SitePaymentMatrix.GetSitePaymentMatrixItem>>(apiResult);
            return result;
        }
    }
}
