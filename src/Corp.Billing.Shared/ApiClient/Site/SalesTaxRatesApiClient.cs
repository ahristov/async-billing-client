﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Site;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Site
{
    public class SalesTaxRatesApiClient : BaseApiClient, ISalesTaxRatesService
    {
        public SalesTaxRatesApiClient(ApiCommand apiSvc) 
            : base(apiSvc)
        { }

        public SalesTaxRates.GetSalesTaxRatesResponse GetSalesTaxRates(SalesTaxRates.GetSalesTaxRatesRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Site.GetSalesTaxRates + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<SalesTaxRates.GetSalesTaxRatesResponse>(apiResult);
            return result;
        }

        public SalesTaxRates.GetSalesTaxRatesResponse GetSalesTaxRates(SalesTaxRates.GetSalesTaxRatesByUserRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.TaxRates.GetSalesTaxRatesByUser + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<SalesTaxRates.GetSalesTaxRatesResponse>(apiResult);
            return result;
        }
    }
}
