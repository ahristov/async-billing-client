﻿using Corp.Billing.Shared.ApiClient.StandardApiHeaders;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Corp.Billing.Shared.ApiClient
{
    public static class ApiConfiguration
    {
        public const string DefaultBillingServiceHost = "mabillapi.match.com";
        public const string DefaultBillingServiceUrl = "https://mabillapi.match.com/";
        public const string DefaultBillingServiceProxyUrl = "";
        public const int DefaultTimeout = 60000;
        public const int DefaultFileTimeout = 120000;

        public static string BillingServiceHost { get; set; }
        public static string BillingServiceUrl { get; set; }
        public static string BillingServiceProxyUrl { get; set; }
        public static int? OriginCodeBaseID { get; set; }
        public static IApiClientRequestHelper RequestHelper { get; set; }
        public static IApiHeaderValuesResolver StandardHeadersResolver { get; set; }
        public static JsonSerializerSettings JsonSerializerSettings { get; set; }

        // Timeouts

        public static int Timeout { get; set; }
        public static int FileTimeout { get; set; }

        static int? _timeoutGet = null;
        public static int TimeoutGet
        {
            get { return _timeoutGet ?? Timeout;  }
            set { _timeoutGet = value;  }
        }

        static int? _timeoutPost = null;
        public static int TimeoutPost
        {
            get { return _timeoutPost ?? Timeout; }
            set { _timeoutPost = value; }
        }

        static int? _timeoutPut = null;
        public static int TimeoutPut
        {
            get { return _timeoutPut ?? Timeout; }
            set { _timeoutPut = value; }
        }

        static int? _timeoutDelete = null;
        public static int TimeoutDelete
        {
            get { return _timeoutDelete ?? Timeout; }
            set { _timeoutDelete = value; }
        }

        static ApiConfiguration()
        {
            BillingServiceHost = DefaultBillingServiceHost;
            BillingServiceUrl = DefaultBillingServiceUrl;
            BillingServiceProxyUrl = DefaultBillingServiceProxyUrl;
            StandardHeadersResolver = new DefaultApiHeaderValuesResolver();
            Timeout = DefaultTimeout;
            FileTimeout = DefaultFileTimeout;
            JsonSerializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                }
            }; 
        }
    }
}
