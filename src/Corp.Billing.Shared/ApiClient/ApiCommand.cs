﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;

namespace Corp.Billing.Shared.ApiClient
{
    public class ApiCommand
    {
        public const string GetVerb = "GET";
        public const string PostVerb = "POST";
        public const string PutVerb = "PUT";
        public const string DeleteVerb = "DELETE";

        private const string FormEncodedContent = "application/x-www-form-urlencoded";
        private const string ApplicationJsonContent = "application/json";
        public const string OriginCodeBaseIdHeaderName = "OriginCodeBaseID";


        public virtual string WebRequestGet(string url)
        {
            return WebRequestGet(url, ApiConfiguration.TimeoutGet, ApiConfiguration.BillingServiceProxyUrl);
        }

        public string WebRequestGet(string url, int timeout)
        {
            return WebRequestGet(url, timeout, ApiConfiguration.BillingServiceProxyUrl);
        }

        private string WebRequestGet(string url, int timeout, string proxyUrl)
        {
            WebRequest webRequest = WebRequest.Create(new Uri(url));
            webRequest.Timeout = timeout;
            webRequest.Method = GetVerb;

            if (!string.IsNullOrEmpty(proxyUrl))
            {
                var webProxy = new WebProxy(proxyUrl);
                webRequest.Proxy = webProxy;
            }

            this.ProcessHeaders(webRequest.Headers);

            string result = webRequest.ReadResponseAsString(null);
            return result;
        }

        public string WebRequestPost(string url, string postData)
        {
            return WebRequestPostContent(url, postData, FormEncodedContent, PostVerb, ApiConfiguration.TimeoutPost,
                ApiConfiguration.BillingServiceProxyUrl);
        }

        public string WebRequestPost(string url, string postData, int timeout)
        {
            return WebRequestPostContent(url, postData, FormEncodedContent, PostVerb, timeout,
                ApiConfiguration.BillingServiceProxyUrl);
        }

        public string WebRequestPostContent(string url, string postData, string contentType)
        {
            return WebRequestPostContent(url, postData, contentType, PostVerb, ApiConfiguration.TimeoutPost,
                ApiConfiguration.BillingServiceProxyUrl);
        }

        public string WebRequestPostContent(string url, string postData, string contentType, int timeout)
        {
            return WebRequestPostContent(url, postData, contentType, PostVerb, timeout,
                ApiConfiguration.BillingServiceProxyUrl);
        }

        public string WebRequestPostContent(string url, string postData, string contentType, string method, int timeout)
        {
            return WebRequestPostContent(url, postData, contentType, method, timeout,
                ApiConfiguration.BillingServiceProxyUrl);
        }

        public string WebRequestPostContent(string url, string postData, string contentType, string method, int timeout,
            string proxyUrl)
        {
            return WebRequestPostContent(url, postData, contentType, method, timeout, proxyUrl, null);
        }

        private WebRequest BuildWebRequest(string url, string method, int timeout, string proxyUrl, Dictionary<string, string> headers)
        {
            var webRequest = WebRequest.Create(new Uri(url));

            webRequest.Timeout = timeout;
            if (webRequest is HttpWebRequest)
            {
                (webRequest as HttpWebRequest).ReadWriteTimeout = timeout;
            }

            webRequest.Method = method;

            if (!string.IsNullOrEmpty(proxyUrl))
            {
                var webProxy = new WebProxy(proxyUrl);
                webRequest.Proxy = webProxy;
            }

            if (headers != null)
            {
                foreach (string key in headers.Keys)
                {
                    if (!WebHeaderCollection.IsRestricted(key))
                        webRequest.Headers.Add(key, headers[key]);
                }
            }

            this.ProcessHeaders(webRequest.Headers);

            return webRequest;
        }

        protected void ProcessHeaders(WebHeaderCollection headers)
        {
            if (ApiConfiguration.OriginCodeBaseID != null && ApiConfiguration.OriginCodeBaseID.HasValue)
            {
                headers.Add(OriginCodeBaseIdHeaderName, ApiConfiguration.OriginCodeBaseID.Value.ToString());
            }

            ApiClient.StandardApiHeaders.ApiHeadersSetter.SetApiHeaders(headers);

            if (ApiConfiguration.RequestHelper != null)
                ApiConfiguration.RequestHelper.ProcessRequestHeaders(headers);
        }

        public string WebRequestPostContent(string url, string postData, string contentType, string method, int timeout, string proxyUrl, Dictionary<string, string> headers)
        {
            return WebRequestPostContent(url, postData, contentType, method, timeout, proxyUrl, headers, Encoding.UTF8);
        }

        public string WebRequestPostContent(string url, string postData, string contentType, string method, int timeout, string proxyUrl, Dictionary<string, string> headers, Encoding encoding)
        {
            WebRequest webRequest = this.BuildWebRequest(url, method, timeout, proxyUrl, headers);
            webRequest.ContentType = contentType;

            string result = null;
            DateTime start = DateTime.Now;

            byte[] byteArray = encoding.GetBytes(postData);
            webRequest.ContentLength = byteArray.Length;
            using (MemoryStream dataStream = new MemoryStream(byteArray.Length))
            {
                dataStream.Write(byteArray, 0, byteArray.Length);

                try
                {
                    result = webRequest.ReadResponseAsString(dataStream);
                }
                catch (WebException ex)
                {
                    ex.ThrowIfApiError();
                    throw;
                }
            }

            return result;
        }


        public string WebRequestPostValues(string url, NameValueCollection postData)
        {
            string result = null;
            DateTime start = DateTime.Now;
            string response = null;
            string request = "POST " + url;

            if (postData != null && postData.Count > 0)
            {
                request += "\n\n" + postData.ToString();
            }

            try
            {
                using (WebClient client = new WebClient())
                {
                    this.ProcessHeaders(client.Headers);
                    byte[] responseData = client.UploadValues(url, postData);
                    result = response = Encoding.ASCII.GetString(responseData);
                }
            }
            catch (WebException webEx)
            {
                if (webEx.Response != null)
                {
                    var webResponse = webEx.Response as HttpWebResponse;
                    if (webResponse != null)
                    {
                        response = string.Format("{0} {1}\nHTTP/{2} {3} {4}", webResponse.Method, webResponse.ResponseUri, webResponse.ProtocolVersion, (int)webResponse.StatusCode, webResponse.StatusDescription);
                        var responseHeaders = webResponse.Headers.ToString();
                        if (!string.IsNullOrEmpty(responseHeaders))
                        {
                            response += "\n" + responseHeaders;
                        }

                        try
                        {
                            using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                            {
                                result = reader.ReadToEnd();
                                response += result;
                            }
                        }
                        catch (ProtocolViolationException)
                        {
                            // No Response
                            result = null;
                        }
                    }
                }

                var error = WebRequestExtensions.ReadApiError(result);
                if (error != null)
                {
                    throw new ApiException(error, webEx.Response as HttpWebResponse);
                }

                throw;
            }
            finally
            {
                WebRequestExtensions.TryLog(url, start, DateTime.Now - start, request, response);
            }

            return result;
        }

        public virtual string WebRequestPostJson<T>(string url, T data)
        {
            string request = JsonConvert.SerializeObject(data);
            return WebRequestPostContent(url, request, ApplicationJsonContent, PostVerb, ApiConfiguration.TimeoutPost);
        }

        public virtual string WebRequestPutJson<T>(string url, T data)
        {
            string request = JsonConvert.SerializeObject(data);
            return WebRequestPostContent(url, request, ApplicationJsonContent, PutVerb, ApiConfiguration.TimeoutPut);
        }

        public virtual void WebRequestDelete(string url)
        {
            WebRequest webRequest = this.BuildWebRequest(url, DeleteVerb, ApiConfiguration.TimeoutDelete, ApiConfiguration.BillingServiceProxyUrl, null);
            webRequest.ValidateResponse(null);
        }

    }
}
