﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride;
using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.RulesEngine
{
    public class PriceOverrideApiClient : BaseApiClient, IPriceOverrideService
    {
        public PriceOverrideApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public PriceOverrideResult GetPriceOverrideInfo(PCRulesRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.ProductCatalog.PriceOverride.PriceOverrideUrl + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<PriceOverrideResult>(apiResult);
            return result;
        }

        public PriceOverrideResultV2 GetPriceOverrideInfoV2(PCRulesRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.ProductCatalog.PriceOverride.PriceOverrideUrlV2 + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<PriceOverrideResultV2>(apiResult);
            return result;
        }

        public PriceOverrideResultV3 GetPriceOverrideInfoV3(PCRulesRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.ProductCatalog.PriceOverride.PriceOverrideUrlV3 + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<PriceOverrideResultV3>(apiResult);
            return result;
        }

        public bool GetMemberUpgradeEligible(PCRulesRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.ProductCatalog.PriceOverride.MemberUpgradeUrl + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<bool>(apiResult);
            return result;
        }

    }
}
