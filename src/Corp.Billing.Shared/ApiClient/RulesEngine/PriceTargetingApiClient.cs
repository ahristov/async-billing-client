﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.ProductCatalog.PricingEngine;
using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.RulesEngine
{
    public class PriceTargetingApiClient : BaseApiClient, IPriceTargetingService
    {
        public PriceTargetingApiClient(ApiCommand apiSvc)
            : base(apiSvc) { }

        public PriceTargetingInfoResult GetPriceTargetingInfo(PCRulesRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.ProductCatalog.PricingEngine.PriceTargeting + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<PriceTargetingInfoResult>(apiResult);
            return result;
        }

        public PriceTargetingInfoV2Result GetPriceTargetingInfoV2(PCRulesRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.ProductCatalog.PricingEngine.PriceTargetingV2 + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<PriceTargetingInfoV2Result>(apiResult);
            return result;
        }

    }
}
