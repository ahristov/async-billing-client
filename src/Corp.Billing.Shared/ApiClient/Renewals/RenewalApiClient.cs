﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.Domain.Payments;
using Corp.Billing.Shared.ProxyContracts;
using Newtonsoft.Json;
using Corp.Billing.Shared.Domain.Renewals;

namespace Corp.Billing.Shared.ApiClient.Renewals
{
    public class RenewalApiClient : BaseApiClient, IRenewalApiClient
    {

        public RenewalApiClient()
            : base(new ApiCommand())
        { }

        public void RenewUser(int userID)
        {
            string url = this.ServiceUrl + ApiPathsV1.Renewals.RenewUser + userID.ToString();
            string apiResult = ApiSvc.WebRequestPostJson(url, new object[] { });
        }
    }
}
