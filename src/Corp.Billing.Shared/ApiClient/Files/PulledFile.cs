﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ApiClient.Files
{
    public class PulledFile<TAccount>
    {
        public string Contents { get; set; }
        public IEnumerable<TAccount> Accounts { get; set; }
    }
}
