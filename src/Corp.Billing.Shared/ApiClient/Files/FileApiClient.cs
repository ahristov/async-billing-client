﻿using Corp.Billing.Core.Services.Files;
using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.Domain.Renewals;
using System;
using System.Collections.Generic;
using System.Text;

namespace Corp.Billing.Shared.ApiClient.Files
{
    public class FileApiClient : BaseApiClient, IFileApiClient
    {

        public FileApiClient()
            : base(new ApiCommand())
        { }

        public void Push(Uri destination, string contents)
        {
            Push(destination, contents, Encoding.UTF8);
        }

        public void Push(Uri destination, string contents, Encoding encoding)
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("DestinationUri", destination.ToString());

            ApiSvc.WebRequestPostContent(ApiConfiguration.BillingServiceUrl + "api/files/push", contents, "multipart/form-data; charset=" + encoding.BodyName, "POST", ApiConfiguration.FileTimeout,
                ApiConfiguration.BillingServiceProxyUrl, headers);
        }

        public void PushAndRename(Uri destination, Uri renameDestination, string contents)
        {
            PushAndRename(destination, renameDestination, contents, Encoding.UTF8);
        }

        public void PushAndRename(Uri destination, Uri renameDestination, string contents, Encoding encoding)
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("DestinationUri", destination.ToString());
            headers.Add("RenameDestinationUri", renameDestination.ToString());

            ApiSvc.WebRequestPostContent(ApiConfiguration.BillingServiceUrl + "api/files/push", contents, "multipart/form-data; charset=" + encoding.BodyName, "POST", ApiConfiguration.FileTimeout,
                ApiConfiguration.BillingServiceProxyUrl, headers);
        }

        public PulledFile<TAccount> Pull<TAccount>(Uri source, ReplacementOptions options)
        {
            var url = ApiConfiguration.BillingServiceUrl + "api/files/pull?source=" + source.ToString();
            if (options != null)
            {
                url += "&options=" + Newtonsoft.Json.JsonConvert.SerializeObject(options);
            }
            var response = ApiSvc.WebRequestGet(url, ApiConfiguration.FileTimeout);
            var result = Newtonsoft.Json.JsonConvert.DeserializeObject<PulledFile<TAccount>>(response);
            return result;
        }

        public PulledFile<TAccount> Pull<TAccount>(Uri source)
        {
            return this.Pull<TAccount>(source, null);
        }

        public void Delete(Uri source)
        {
            ApiSvc.WebRequestPost(ApiConfiguration.BillingServiceUrl + "api/files/delete?source=" + source.ToString(), string.Empty, ApiConfiguration.FileTimeout);
        }

        public void Move(Uri source, Uri destination)
        {
            ApiSvc.WebRequestPost(ApiConfiguration.BillingServiceUrl + "api/files/move?source=" + source.ToString() + "&destination=" + destination.ToString(), string.Empty, ApiConfiguration.FileTimeout);
        }

        public void Copy(Uri source, Uri destination)
        {
            ApiSvc.WebRequestPost(ApiConfiguration.BillingServiceUrl + "api/files/copy?source=" + source.ToString() + "&destination=" + destination.ToString(), string.Empty, ApiConfiguration.FileTimeout);
        }

        public IEnumerable<FileListingItem> GetDirectoryListing(Uri directory)
        {
            string response = ApiSvc.WebRequestGet(ApiConfiguration.BillingServiceUrl + "api/files/list?source=" + directory.ToString(), ApiConfiguration.FileTimeout);
            var results = Newtonsoft.Json.JsonConvert.DeserializeObject<FileListingItem[]>(response);
            return results;
        }
    }
}
