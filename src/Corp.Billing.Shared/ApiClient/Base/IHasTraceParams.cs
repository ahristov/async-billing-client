﻿namespace Corp.Billing.Shared.ApiClient.Base
{
    public interface IHasTraceParams
    {
        // Trace ///////////////////////////////////////////////////////

        /// <summary>
        /// Gets or sets the trace level.
        /// </summary>
        /// <value>
        /// The trace level.
        /// </value>
        TraceRequestParams.LevelType TraceLevel { get; set; }

        /// <summary>
        /// Gets or sets the trace verbosity.
        /// </summary>
        /// <value>
        /// The trace verbosity.
        /// </value>
        TraceRequestParams.VerbosityType TraceVerbosity { get; set; }
    }
}
