﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Products;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Products
{
    public class ProductApiClient : BaseApiClient, IProductService, IProductApiClient
    {

        public ProductApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }


        // MatchMe

        public MatchMe30DayEligibilityCheckResult MatchMe30DayEligibilityCheck(MatchMe30DayEligibilityCheckRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Products.MatchMe.GetMatchMe30DayEligibilityCheck + "/?" + request.ToQueryString();
            string apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<MatchMe30DayEligibilityCheckResult>(apiResult);
            return result;
        }

        public PutMeInUserListResult PutMeInUserList(PutMeInUserListRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Products.MatchMe.PutMeInUserList;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<PutMeInUserListResult>(apiResult);
            return result;
        }


        // TopSpot

        public TopSpotStatusResult GetTopSpotStatus(TopSpotStatusRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Products.TopSpot.GetTopSpotStatusPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<TopSpotStatusResult>(apiResult);
            return result;
        }

        public ProvisionAndActivateTopSpotResult ProvisionAndActivateTopSpot(ProvisionAndActivateTopSpotRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Products.TopSpot.ProvisionAndActivateTopSpot;
            string apiResult = ApiSvc.WebRequestPostJson(url, request);

            var result = JsonConvert.DeserializeObject<ProvisionAndActivateTopSpotResult>(apiResult);
            return result;
        }


        public ActivateUnclaimedImpulseFeatureResult ActivateUnclaimedImpulseFeature(ActivateUnclaimedImpulseFeatureRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Products.TopSpot.ActivateUnclaimedImpulseFeature;
            var apiResult = ApiSvc.WebRequestPutJson(url, request);

            var result = JsonConvert.DeserializeObject<ActivateUnclaimedImpulseFeatureResult>(apiResult);
            return result;
        }


        // ProfilePro

        public CSAUserPCSStatusResult GetUnitUserStatusPcs(CSAUserPCSStatusRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Products.ProfilePro.GetUnitUserStatusPcsPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<CSAUserPCSStatusResult>(apiResult);
            return result;
        }


        // Unit products

        public UnitUserStatusResult GetUnitUserStatus(UnitUserStatusRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Products.UnitProducts.GetUnitUserStatusPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<UnitUserStatusResult>(apiResult);
            return result;
        }

        public UnitDisplayStatusResult GetUnitDisplayStatus(UnitDisplayStatusRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Products.UnitProducts.GetUnitDisplayStatusPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<UnitDisplayStatusResult>(apiResult);
            return result;
        }

    }
}
