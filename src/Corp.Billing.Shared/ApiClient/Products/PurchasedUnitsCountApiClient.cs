﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Products.PurchasedUnitsCount;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Products
{
    public class PurchasedUnitsCountApiClient : BaseApiClient, IPurchasedUnitsCount
    {
        public PurchasedUnitsCountApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public GetPurchasedUnitsCountResult GetPurchasedUnitsCount(GetPurchasedUnitsCountRequest request)
        {
            string url = ServiceUrl + ApiPathsV1.Products.UnitProducts.GetUnitPurchasedUnitsCountPath + "/?" + request.ToQueryString();
            var apiResult = ApiSvc.WebRequestGet(url);

            var result = JsonConvert.DeserializeObject<GetPurchasedUnitsCountResult>(apiResult);
            return result;
        }
    }
}
