﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.ApiClient;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Products.ImpulseFeatures;
using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;
using Newtonsoft.Json;

namespace Corp.Billing.Shared.ApiClient.Products
{
    public class ImpulseFeaturesApiClient : BaseApiClient, IImpulseFeaturesService
    {
        public ImpulseFeaturesApiClient(ApiCommand apiSvc)
            : base(apiSvc)
        { }

        public GetStatusResult GetImpulseFeaturesStatus(GetStatusRequest req)
        {
            string url = ServiceUrl + ApiPathsV1.Products.ImpulseFeatures.StatusAndActivate + "/?" + req.ToQueryString();

            url = url
                .Replace("{productFeature}", req.ProductFeature.ToString().ToLowerInvariant());

            string apiResult = ApiSvc.WebRequestGet(url);

            var res = JsonConvert.DeserializeObject<GetStatusResult>(apiResult);
            return res;
        }

        public ActivateResult ActivateImpulseFeature(ActivateRequest req)
        {
            string url = ServiceUrl + ApiPathsV1.Products.ImpulseFeatures.StatusAndActivate;

            url = url
                .Replace("{productFeature}", req.ProductFeature.ToString().ToLowerInvariant());

            string apiResult = ApiSvc.WebRequestPutJson(url, req);

            var res = JsonConvert.DeserializeObject<ActivateResult>(apiResult);
            return res;
        }
    }
}
