﻿using Corp.Billing.Shared.Domain.Products;
using System;
using System.Linq;

namespace Corp.Billing.Shared.Subscription.Product
{
    public class ProductDataAttribute: Attribute
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public ProductFeatureType ProductFeature { get; set; }
        public bool IsRenewable { get; set; }
        public short NumberOfUnits { get; set; }
        public short ImpulseMinutes { get; set; }
        public ProductAttributeTypeEnum[] ProductAttributes { get; set; }

        public ProductDataAttribute(
            int productID, 
            string productName,
            ProductFeatureType productFeature,
            bool isRenewable,
            short numberOfUnits,
            short impulseMinutes,
            ProductAttributeTypeEnum[] productAttributes)
        {
            ProductID = productID;
            ProductName = productName;
            ProductFeature = productFeature;
            IsRenewable = isRenewable;
            NumberOfUnits = numberOfUnits;
            ImpulseMinutes = impulseMinutes;
            ProductAttributes = productAttributes;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            ProductDataAttribute other = obj as ProductDataAttribute;
            if (other == null
                || other.ProductID != this.ProductID
                || other.ProductName != this.ProductName
                || other.ProductFeature != this.ProductFeature
                || other.IsRenewable != this.IsRenewable
                || other.NumberOfUnits != this.NumberOfUnits
                || other.ImpulseMinutes != this.ImpulseMinutes)
                return false;

            var sum1 = GetBitMask(this.ProductAttributes ?? new ProductAttributeTypeEnum[] { });
            var sum2 = GetBitMask(other.ProductAttributes ?? new ProductAttributeTypeEnum[] { });

            return sum1 == sum2;
        }

        public override int GetHashCode() => base.GetHashCode();
        public override bool IsDefaultAttribute() => this.ProductID == 0;

        private static long GetBitMask(ProductAttributeTypeEnum[] items)
        {
            var sum = items
                        .Where(x => (int)x > 0)
                        .Select(x => (int)x)
                        .Distinct()
                        .Sum(x => Math.Pow(2, x - 1));
            return (long)sum;
        }

    }
}
