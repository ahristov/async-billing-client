﻿
// Do not modify, this is generated from T4 template

using System;

namespace Corp.Billing.Shared.Subscription.Product
{
    [Serializable]
    public enum ProductAttributeTypeEnum
    {
        None = 0,
        LoyaltyPricing = 1,
        MonthGuaranteeProgram = 2,
        CsaFreeAddOnExistingSubs = 3,
        OneTimeEventServiceFeeDoNotDisplayOnRateCard = 4,
        UpsellProductDoNotDisplayOnRateCard = 5,
        Installments = 6,
        AppleIosInAppPurchase = 7,
        MonthlyInstallmentsCollectedByPaymentProcessor = 8,
        RushhourBoost = 9,
        BoostWithRff = 10,
    }
}

