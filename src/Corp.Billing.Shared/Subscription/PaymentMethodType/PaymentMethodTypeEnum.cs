﻿
// Do not modify, this is generated from T4 template

using System;

namespace Corp.Billing.Shared.Subscription.PaymentMethodType
{
    [Serializable]
    public enum PaymentMethodTypeEnum
    {
        /// <summary>
        /// Not Applicable - BaseCode: N, DtlCode: NA
        /// </summary>
        [PaymentMethodTypeData(0, false, "NA", "N", true, PaymentMethodBaseTypeEnum.NotApplicable)]
        NotApplicable = 0,

        /// <summary>
        /// American Express - BaseCode: C, DtlCode: AX
        /// </summary>
        [PaymentMethodTypeData(1, true, "AX", "C", true, PaymentMethodBaseTypeEnum.Card)]
        AmericanExpress = 1,

        /// <summary>
        /// Diners Club - BaseCode: C, DtlCode: DC
        /// </summary>
        [PaymentMethodTypeData(1, true, "DC", "C", true, PaymentMethodBaseTypeEnum.Card)]
        DinersClub = 2,

        /// <summary>
        /// Discover - BaseCode: C, DtlCode: DI
        /// </summary>
        [PaymentMethodTypeData(1, true, "DI", "C", true, PaymentMethodBaseTypeEnum.Card)]
        Discover = 3,

        /// <summary>
        /// JCB (Japanese Credit) - BaseCode: C, DtlCode: JC
        /// </summary>
        [PaymentMethodTypeData(1, true, "JC", "C", true, PaymentMethodBaseTypeEnum.Card)]
        JcbJapaneseCredit = 4,

        /// <summary>
        /// Master Card - BaseCode: C, DtlCode: MC
        /// </summary>
        [PaymentMethodTypeData(1, true, "MC", "C", true, PaymentMethodBaseTypeEnum.Card)]
        MasterCard = 5,

        /// <summary>
        /// Visa - BaseCode: C, DtlCode: VI
        /// </summary>
        [PaymentMethodTypeData(1, true, "VI", "C", true, PaymentMethodBaseTypeEnum.Card)]
        Visa = 6,

        /// <summary>
        /// Online Check - BaseCode: O, DtlCode: OC
        /// </summary>
        [PaymentMethodTypeData(2, false, "OC", "O", false, PaymentMethodBaseTypeEnum.OnlineCheck)]
        OnlineCheck = 7,

        /// <summary>
        /// Check - BaseCode: P, DtlCode: CK
        /// </summary>
        [PaymentMethodTypeData(0, false, "CK", "P", true, PaymentMethodBaseTypeEnum.Check)]
        Check = 8,

        /// <summary>
        /// Cash - BaseCode: S, DtlCode: CA
        /// </summary>
        [PaymentMethodTypeData(0, false, "CA", "S", true, PaymentMethodBaseTypeEnum.Cash)]
        Cash = 9,

        /// <summary>
        /// Money Order - BaseCode: M, DtlCode: MO
        /// </summary>
        [PaymentMethodTypeData(0, false, "MO", "M", false, PaymentMethodBaseTypeEnum.MoneyOrder)]
        MoneyOrder = 12,

        /// <summary>
        /// E-Check - BaseCode: A, DtlCode: EC
        /// </summary>
        [PaymentMethodTypeData(2, true, "EC", "A", false, PaymentMethodBaseTypeEnum.ECheck)]
        ECheck = 15,

        /// <summary>
        /// Gift Subscription Redemption - BaseCode: G, DtlCode: GS
        /// </summary>
        [PaymentMethodTypeData(0, false, "GS", "G", true, PaymentMethodBaseTypeEnum.Gift)]
        GiftSubscriptionRedemption = 16,

        /// <summary>
        /// Switch/Maestro - BaseCode: C, DtlCode: SM
        /// </summary>
        [PaymentMethodTypeData(1, true, "SM", "C", false, PaymentMethodBaseTypeEnum.Card)]
        SwitchMaestro = 17,

        /// <summary>
        /// PayPal - BaseCode: L, DtlCode: PP
        /// </summary>
        [PaymentMethodTypeData(3, true, "PP", "L", true, PaymentMethodBaseTypeEnum.PayPal)]
        Paypal = 18,

        /// <summary>
        /// Zeus Conversion Without Payment Information - BaseCode: C, DtlCode: ZC
        /// </summary>
        [PaymentMethodTypeData(1, true, "ZC", "C", false, PaymentMethodBaseTypeEnum.Card)]
        ZeusConversionWithoutPaymentInformation = 19,

        /// <summary>
        /// Danal/Teledit - BaseCode: N, DtlCode: DA
        /// </summary>
        [PaymentMethodTypeData(0, false, "DA", "N", false, PaymentMethodBaseTypeEnum.NotApplicable)]
        DanalTeledit = 20,

        /// <summary>
        /// GC Bank Transfer - BaseCode: T, DtlCode: GC
        /// </summary>
        [PaymentMethodTypeData(4, false, "GC", "T", false, PaymentMethodBaseTypeEnum.Offline)]
        GcBankTransfer = 21,

        /// <summary>
        /// Apple IAP - BaseCode: E, DtlCode: AP
        /// </summary>
        [PaymentMethodTypeData(0, false, "AP", "E", true, PaymentMethodBaseTypeEnum.AppleIAP)]
        AppleIap = 22,

        /// <summary>
        /// GC - konbini - BaseCode: T, DtlCode: KB
        /// </summary>
        [PaymentMethodTypeData(4, false, "KB", "T", false, PaymentMethodBaseTypeEnum.Offline)]
        GcKonbini = 23,

        /// <summary>
        /// Ello - BaseCode: C, DtlCode: EL
        /// </summary>
        [PaymentMethodTypeData(1, true, "EL", "C", true, PaymentMethodBaseTypeEnum.Card)]
        Ello = 24,

        /// <summary>
        /// Hipercard - BaseCode: C, DtlCode: HC
        /// </summary>
        [PaymentMethodTypeData(1, false, "HC", "C", false, PaymentMethodBaseTypeEnum.Card)]
        Hipercard = 25,

        /// <summary>
        /// Boleto - BaseCode: T, DtlCode: BO
        /// </summary>
        [PaymentMethodTypeData(4, false, "BO", "T", true, PaymentMethodBaseTypeEnum.Boleto)]
        Boleto = 26,

        /// <summary>
        /// Debito - BaseCode: T, DtlCode: DB
        /// </summary>
        [PaymentMethodTypeData(5, true, "DB", "T", true, PaymentMethodBaseTypeEnum.DirectDebit)]
        Debito = 27,

        /// <summary>
        /// DineroMail - BaseCode: T, DtlCode: DM
        /// </summary>
        [PaymentMethodTypeData(4, false, "DM", "T", true, PaymentMethodBaseTypeEnum.DineroMail)]
        Dineromail = 28,

        /// <summary>
        /// Amazon - BaseCode: Z, DtlCode: AZ
        /// </summary>
        [PaymentMethodTypeData(6, true, "AZ", "Z", true, PaymentMethodBaseTypeEnum.PayWithAmazon)]
        Amazon = 29,

        /// <summary>
        /// Apple Pay - BaseCode: X, DtlCode: PL
        /// </summary>
        [PaymentMethodTypeData(7, true, "PL", "X", true, PaymentMethodBaseTypeEnum.ApplePay)]
        ApplePay = 30,

        /// <summary>
        /// Naranja  - BaseCode: C, DtlCode: NR
        /// </summary>
        [PaymentMethodTypeData(1, true, "NR", "C", true, PaymentMethodBaseTypeEnum.Card)]
        Naranja = 31,

        /// <summary>
        /// Shopping  - BaseCode: C, DtlCode: SP
        /// </summary>
        [PaymentMethodTypeData(1, true, "SP", "C", true, PaymentMethodBaseTypeEnum.Card)]
        Shopping = 32,

        /// <summary>
        /// Cabal  - BaseCode: C, DtlCode: CB
        /// </summary>
        [PaymentMethodTypeData(1, true, "CB", "C", true, PaymentMethodBaseTypeEnum.Card)]
        Cabal = 33,

        /// <summary>
        /// ArgenCard  - BaseCode: C, DtlCode: AC
        /// </summary>
        [PaymentMethodTypeData(1, true, "AC", "C", true, PaymentMethodBaseTypeEnum.Card)]
        Argencard = 34,

        /// <summary>
        /// Cencosud  - BaseCode: C, DtlCode: CS
        /// </summary>
        [PaymentMethodTypeData(1, true, "CS", "C", true, PaymentMethodBaseTypeEnum.Card)]
        Cencosud = 35,

        /// <summary>
        /// BlackHawk Gift Card - BaseCode: C, DtlCode: BH
        /// </summary>
        [PaymentMethodTypeData(1, false, "BH", "C", true, PaymentMethodBaseTypeEnum.Card)]
        BlackhawkGiftCard = 36,

    }
}


