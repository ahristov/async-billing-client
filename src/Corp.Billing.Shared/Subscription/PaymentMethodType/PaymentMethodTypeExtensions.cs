﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Corp.Billing.Shared.Subscription.PaymentMethodType
{
    public static class PaymentMethodTypeExtensions
    {
        public static IEnumerable<PaymentMethodTypeEnum> GetAllPaymentMethodTypes()
            => Enum.GetValues(typeof(PaymentMethodTypeEnum)).Cast<PaymentMethodTypeEnum>();

        public static PaymentMethodTypeDataAttribute GetPaymentMethodTypeData(
            this PaymentMethodTypeEnum paymentMethodTypeEnum)
        {
            FieldInfo fi = paymentMethodTypeEnum.GetType().GetField(paymentMethodTypeEnum.ToString());
            PaymentMethodTypeDataAttribute[] attributes =
                (PaymentMethodTypeDataAttribute[])fi.GetCustomAttributes(typeof(PaymentMethodTypeDataAttribute), false);
            return (attributes != null && attributes.Length > 0)
                ? attributes[0]
                : null;
        }

        public static PaymentMethodBaseTypeEnum GetPaymentMethodBaseType(this PaymentMethodTypeEnum paymentMethodTypeEnum)
        {
            var data = GetPaymentMethodTypeData(paymentMethodTypeEnum);
            return data?.BasePaymentType ?? PaymentMethodBaseTypeEnum.NotApplicable;
        }

        public static PaymentMethodTypeEnum GetPaymentTypeFromDtlCode(this string dtlCode)
        {
            if (string.IsNullOrWhiteSpace(dtlCode))
                return PaymentMethodTypeEnum.NotApplicable;

            foreach (var paymentMethodTypeEnum in GetAllPaymentMethodTypes())
            {
                var data = GetPaymentMethodTypeData(paymentMethodTypeEnum);
                if (data != null && dtlCode.Equals(data.DtlCode, StringComparison.InvariantCultureIgnoreCase))
                    return paymentMethodTypeEnum;
            }

            return PaymentMethodTypeEnum.NotApplicable;
        }
    }
}
