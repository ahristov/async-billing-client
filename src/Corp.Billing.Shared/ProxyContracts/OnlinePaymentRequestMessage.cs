﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class OnlinePaymentRequestMessage : OrderPaymentRequestMessage
    {
        public OnlinePaymentRequestMessage() { }
        public OnlinePaymentRequestMessage(Dictionary<string, object> from) : base(from) { }

    }
}
