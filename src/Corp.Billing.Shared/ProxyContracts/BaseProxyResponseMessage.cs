﻿using System;

namespace Corp.Billing.Shared.ProxyContracts
{
    public abstract class BaseProxyResponseMessage : BaseProxyMessage
    {
        protected BaseProxyResponseMessage()
        {
            ResponseErrors = new ResponseErrorsCollection();
        }

        public int ReturnValue
        {
            get { return this.Get<int>("ReturnValue"); }
            set { this["ReturnValue"] = value; }
        }

        public int OrderResult
        {
            get { return this.Get<int>("OrderResult"); }
            set { this["OrderResult"] = value; }
        }

        /// <summary>
        /// To be used for insufficient funds
        /// </summary>
        public bool CapturedBadAccountData
        {
            get { return this.Get<bool>("CapturedBadAccountData"); }
            set { this["CapturedBadAccountData"] = value; }
        }

        public ResponseStatusType ResponseStatus
        {
            get { return GetEnum<ResponseStatusType>("ResponseStatus"); }
            set { this["ResponseStatus"] = ToEnum<ResponseStatusType>(value + ""); }
        }


        public ResponseErrorsCollection ResponseErrors
        {
            get
            {
                var val = this["ResponseErrors"];

                try
                {
                    if (val is ResponseErrorsCollection)
                    {
                        return val as ResponseErrorsCollection;
                    }

                    if (val is Newtonsoft.Json.Linq.JArray)
                    {
                        return (val as Newtonsoft.Json.Linq.JArray).ToObject<ResponseErrorsCollection>();
                    }

                } catch { }

                return new ResponseErrorsCollection();
                
            }
            set { this["ResponseErrors"] = value; }
        }
    }
}
