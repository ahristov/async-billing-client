﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public class CreditCardWithExternalWalletPaymentRequestMessage : CreditCardWithCardinalParamsPaymentRequestMessage
    {
        public string CurrencyCode
        {
            get { return Get<string>("CurrencyCode"); }
            set { this["CurrencyCode"] = value; }
        }

        public string WalletId
        {
            get { return Get<string>("WalletId"); }
            set { this["WalletId"] = value; }
        }

    }
}
