﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.ProxyContracts
{
    public enum OrderType
    {
        BaseProduct,
        AddOn,
        Unit
    }

    public class PayPalPaymentRequestMessage : OnlinePaymentRequestMessage
    {
        public PayPalPaymentRequestMessage() : base()
        {
            CommandType = PaymentProxyCommandType.PayPalOrderProcessing;
        }

        public PayPalPaymentRequestMessage(Dictionary<string, object> from) : base(from)
        {
            CommandType = PaymentProxyCommandType.PayPalOrderProcessing;
        }

        public OrderType OrderType
        {
            get { return Get<OrderType>("OrderType"); }
            set { this["OrderType"] = value; }
        }

        /// <summary>
        /// Either provide the token or billing agreement id
        /// </summary>
        public string Token
        {
            get { return Get<string>("Token"); }
            set { this["Token"] = value; }
        }

        /// <summary>
        /// Either provide the billing agreement id or the token
        /// </summary>
        public string BillingAgreementId
        {
            get { return Get<string>("BillingAgreementId"); }
            set { this["BillingAgreementId"] = value; }
        }

        public decimal TotalAmount
        {
            get { return Get<decimal>("TotalAmount"); }
            set { this["TotalAmount"] = value; }
        }
    }
}
