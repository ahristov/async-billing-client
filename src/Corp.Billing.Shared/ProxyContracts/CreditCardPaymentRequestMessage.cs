﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class CreditCardPaymentRequestMessage : OnlinePaymentRequestMessage
    {
        public CreditCardPaymentRequestMessage() : base()
        {
            CommandType = PaymentProxyCommandType.CreditCardOrderProcessing;
        }

        public CreditCardPaymentRequestMessage(Dictionary<string, object> from) : base(from)
        {
            CommandType = PaymentProxyCommandType.CreditCardOrderProcessing;
        }

        public string CardNumberClear
        {
            get { return Get<string>("CardNumberClear"); }
            set { this["CardNumberClear"] = value; }
        }

        public string Cvv
        {
            get { return Get<string>("Cvv"); }
            set { this["Cvv"] = value; }
        }

        public byte CcMonth
        {
            get { return Get<byte>("CCMonth"); }
            set { this["CCMonth"] = value; }
        }

        public short CcYear
        {
            get { return Get<short>("CCYear"); }
            set { this["CCYear"] = value; }
        }

        public bool CheckCc
        {
            get { return Get<bool>("CheckCC"); }
            set { this["CheckCC"] = value; }
        }

        public bool ExpiredCcDtCheck
        {
            get { return Get<bool>("ExpiredCCDtCheck"); }
            set { this["ExpiredCCDtCheck"] = value; }
        }

        public bool CaptureDetailedCreditCardErrorMessage
        {
            get { return Get<bool>("CaptureDetailedCreditCardErrorMessage"); }
            set { this["CaptureDetailedCreditCardErrorMessage"] = value; }
        }


        // Cardinal values.

        public int? CardinalLogId
        {
            get { return Get<int?>("CardinalLogId", (v) => v > 0); }
            set { this["CardinalLogId"] = value; }
        }

        public string ECI
        {
            get { return Get<string>("ECI"); }
            set { this["ECI"] = value; }
        }

        public string CAVV
        {
            get { return Get<string>("CAVV"); }
            set { this["CAVV"] = value; }
        }

        public string XID
        {
            get { return Get<string>("XID"); }
            set { this["XID"] = value; }
        }

    }
}
