﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public enum PaymentProxyCommandType
    {
        Undefined = 0,
        CreditCardOrderProcessing = 1,
        PayPalOrderProcessing = 2,
        GetPayPalToken = 3,
        DineroMailOrderProcessing = 4,
        BoletoOrderProcessing = 5,
        DebitOrderProcessing = 6,
        DineroMailGetAuthorizeUrl = 7,
        BoletoGetAuthorizeUrl = 8,
        PayPalBillingAgreement = 9,
        VisaCheckoutOrderProcessing = 10,
        CashOrderProcessing = 11,
        AMWSOrderProcessing = 12,
        MasterPassOrderProcessing = 13,
        ApplePayOrderProcessing = 14,

        CreditCardOrderProcessingV2 = 101,
        PayPalOrderProcessingV2 = 102,
        DineroMailOrderProcessingV2 = 104,
        BoletoOrderProcessingV2 = 105,
        DebitOrderProcessingV2 = 106,
        VisaCheckoutOrderProcessingV2 = 110,
        AMWSOrderProcessingV2 = 112,
        MasterPassOrderProcessingV2 = 113,
        ApplePayOrderProcessingV2 = 114,
        CashOrderProcessingV2 = 115,
        GiftCardOrderProcessingV2 = 116,
    }
}
