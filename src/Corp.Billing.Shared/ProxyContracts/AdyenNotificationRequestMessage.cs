﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ProxyContracts
{
    

    public class AdyenNotificationRequestMessage
    {

        public class AdditionalDataType
        {
            public string AuthCode { get; set; }
            public string CardSummary { get; set; }
            public string ExpiryDate { get; set; }
        }

        public enum EventCodes { AUTHORISATION, CANCELLATION, REFUND, CANCEL_OR_REFUND, CAPTURE, CAPTURE_FAILED, REFUND_FAILED, REFUNDED_REVERSED, REQUEST_FOR_INFORMATION, ADVICE_OF_DEBIT, CHARGEBACK, CHARGEBACK_REVERSED, NOTIFICATION_OF_CHARGEBACK, NOTIFICATION_OF_FRAUD, MANUAL_REVIEW_ACCEPT, MANUAL_REVIEW_REJECT, RECURRING_CONTRACT, SUBMIT_RECURRING, DEACTIVATE_RECURRING, PAYOUT_EXPIRE, PAYOUT_DECLINE, PAYOUT_THIRDPARTY, REFUND_WITH_DATA, AUTHORISE_REFERRAL, EXPIRE, FRAUD_ONLY, FUND_TRANSFER, HANDLED_EXTERNALLY, OFFER_CLOSED, ORDER_OPENED, ORDER_CLOSED, PENDING, PROCESS_RETRY, REPORT_AVAILABLE }

        public AdditionalDataType AdditionalData  { get; set; }
        public string PspReference { get; set; }
        public EventCodes EventCode { get; set; }
        public DateTime EventDate { get; set; }
        public string MerchantAccountCode { get; set; }
        public string Operations { get; set; }
        public string MerchantReference { get; set; }
        public string OriginalReference { get; set; }
        public string PaymentMethod { get; set; }
        public string Reason { get; set; }
        public Boolean Success { get; set; }

        public string Currency { get; set; }
        public int Value { get; set; }
        
        public Boolean Live { get; set; }

    }
}
