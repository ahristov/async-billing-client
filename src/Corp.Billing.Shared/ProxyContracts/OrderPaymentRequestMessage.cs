﻿using Corp.Billing.Shared.Domain.Installlments;
using Corp.Billing.Shared.Domain.Subscription;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Corp.Billing.Shared.ProxyContracts
{
	public class OrderPaymentRequestMessage : PaymentProxyRequestMessage
    {
        public OrderPaymentRequestMessage()
        {
            OrderDetailTable = new List<OrderDetailTable>();
        }
        public OrderPaymentRequestMessage(Dictionary<string, object> from) : base(from) { }

        public short? CountryCode
        {
            get { return  Get<short?>("CountryCode"); }
            set { this["CountryCode"] = value; }
        }

        public byte? StateCode
        {
            get { return Get<byte?>("StateCode"); }
            set { this["StateCode"] = value; }
        }

        public string OrderList
        {
            get { return Get<string>("OrderList"); }
            set { this["OrderList"] = value;  }
        }


        public int? OtherUserId
        {
            get { return Get<int?>("OtherUserId") ; }
            set { this["OtherUserId"] = value; }
        }

        public int? EventId
        {
            get { return Get<int?>("EventId"); }
            set { this["EventId"] = value; }
        }

        public decimal? EventTotalPriceIncludingGuests
        {
            get { return Get<decimal?>("EventTotalPriceIncludingGuests"); }
            set { this["EventTotalPriceIncludingGuests"] = value; }
        }

        public int SubBrandId
        {
            get { return Get<int>("SubBrandId"); }
            set { this["SubBrandId"] = value; }
        }

        public string PromoId
        {
            get { return Get<string>("PromoId"); }
            set { this["PromoId"] = value; }
        }

        // Secondary revenue share
        public int StId
        {
            get { return Get<int>("STID"); }
            set { this["STID"] = value; }
        }

        public short SubscribeTextId
        {
            get { return Get<short>("SubscribeTextID"); }
            set { this["SubscribeTextID"] = value; }
        }


        public short AccountOrderModId
        {
            get { return Get<short>("AccountOrderModID"); }
            set { this["AccountOrderModID"] = value; }
        }

        public DateTime OldUserStatusDate
        {
            get { return Get<DateTime>("OldUserStatusDate"); }
            set { this["OldUserStatusDate"] = value; }
        }

        public short GeoStateTaxId
        {
            get { return Get<short>("GeoStateTaxID"); }
            set { this["GeoStateTaxID"] = value; }
        }


        public string FName
        {
            get { return Get<string>("FirstName") ?? string.Empty; }
            set { this["FirstName"] = value; }
        }

        public string LName
        {
            get { return Get<string>("LastName") ?? string.Empty; }
            set { this["LastName"] = value; }
        }

        public string FullName
        {
            get { return Get<string>("FullName") ?? string.Empty; }
            set { this["FullName"] = value; }
        }

        public string Address1
        {
            get { return Get<string>("Address1") ?? string.Empty; }
            set { this["Address1"] = value; }
        }

        public string Address2
        {
            get { return Get<string>("Address2") ?? string.Empty; }
            set { this["Address2"] = value; }
        }

        public string CityName
        {
            get { return Get<string>("CityName") ?? string.Empty; }
            set { this["CityName"] = value; }
        }

        public string PostalCode
        {
            get { return Get<string>("PostalCode") ?? string.Empty; }
            set { this["PostalCode"] = value; }
        }


        public string PhoneNumberClear
        {
            get { return Get<string>("PhoneNumberClear"); }
            set { this["PhoneNumberClear"] = value; }
        }


        public byte? ClientIp1
        {
            get { return Get<byte?>("ClientIp1"); }
            set { this["ClientIp1"] = value; }
        }

        public byte? ClientIp2
        {
            get { return Get<byte?>("ClientIp2"); }
            set { this["ClientIp2"] = value; }
        }

        public byte? ClientIp3
        {
            get { return Get<byte?>("ClientIp3"); }
            set { this["ClientIp3"] = value; }
        }

        public byte? ClientIp4
        {
            get { return Get<byte?>("ClientIp4"); }
            set { this["ClientIp4"] = value; }
        }

        // Session ID
        public Guid? SId
        {
            get { return Get<Guid?>("SID"); }
            set { this["SID"] = value.ToString(); }
        }
        public Guid? SessionId
        {
            get { return SId; }
            set { SId = value; }
        }


        //CSA use
        //used to determine transaction source.  3=cash order CSA, 4=cc order CSA
        public byte? TrxSourceId
        {
            get { return Get<byte?>("TrxSourceId"); }
            set { this["TrxSourceId"] = value; }
        }

        public int? ReferenceTrxId
        {
            get { return this.Get<int?>("ReferenceTrxId"); }
            set { this["ReferenceTrxId"] = value; }
        }


        //CSA used for various credit scenarios
        public short? CreditCode
        {
            get { return Get<short?>("CreditCode");  }
            set { this["CreditCode"] = value; }
        }

        //CSA use
        public int? EmployeeId
        {
            get { return Get<int?>("EmployeeId"); }
            set { this["EmployeeId"] = value; }
        }

        //CSA use denoting a free subscription given to a user
        public bool? IsComplimentary
        {
            get { return Get<bool?>("IsComplimentary"); }
            set { this["IsComplimentary"] = value; }
        }

        //CSA use
        public string Comment
        {
            get { return Get<string>("Comment"); }
            set { this["Comment"] = value; }
        }


        public int? PaymentMethodToken
        {
            get { return Get<int?>("PaymentMethodToken"); }
            set { this["PaymentMethodToken"] = value; }
        }

        public string SecuredPaymentToken
        {
            get { return Get<string>("SecuredPaymentToken"); }
            set { this["SecuredPaymentToken"] = value; }
        }

        public bool? IsInstallments
        {
            get { return Get<bool?>("IsInstallments"); }
            set { this["IsInstallments"] = value; }
        }

        public int? NumberOfInstallments
        {
            get { return Get<int?>("NumberOfInstallments"); }
            set { this["NumberOfInstallments"] = value; }
        }


        public int? InstallmentFrequency
        {
            get { return Get<int?>("InstallmentFrequency"); }
            set { this["InstallmentFrequency"] = value; }
        }







        public int? RateCardKey
        {
            get { return Get<int?>("RateCardKey"); }
            set { this["RateCardKey"] = value; }
        }

        public int? RateCardOverrideKey
        {
            get { return Get<int?>("RateCardOverrideKey"); }
            set { this["RateCardOverrideKey"] = value; }
        }


        public bool IsUpsell
        {
            get { return Get<bool>("IsUpsell"); }
            set { this["IsUpsell"] = value; }
        }

        public bool IsMembershipUpgrade
        {
            get { return Get<bool>("IsMembershipUpgrade"); }
            set { this["IsMembershipUpgrade"] = value; }
        }

        public bool IsSaveOffer
        {
            get { return Get<bool>("IsSaveOffer"); }
            set { this["IsSaveOffer"] = value; }
        }

		public string MachineId
		{
			get { return Get<string>("MachineId"); }
			set { this["MachineId"] = value; }
		}

		public string EventCorrelationId
		{
			get { return Get<string>("EventCorrelationId"); }
			set { this["EventCorrelationId"] = value; }
		}
		
		public List<OrderDetailTable> OrderDetailTable
        {
            get
            {
                try
                {
                    List<OrderDetailTable> result = (this["OrderDetailTable"] as List<OrderDetailTable>)
                        ?? Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderDetailTable>>(this["OrderDetailTable"].ToString());
                    return result;
                }
                catch
                {
                    return new List<OrderDetailTable>();
                }

            }
            set
            {
                this["OrderDetailTable"] = Newtonsoft.Json.JsonConvert.SerializeObject(value);
            }
        }

        public List<InstallmentDetailTable> InstallmentTable
        {
            get
            {
                try
                {
                    List<InstallmentDetailTable> result = (this["InstallmentTable"] as List<InstallmentDetailTable>)
                        ?? Newtonsoft.Json.JsonConvert.DeserializeObject<List<InstallmentDetailTable>>(this["InstallmentTable"].ToString());
                    return result;
                }
                catch
                {
                    return new List<InstallmentDetailTable>();
                }

            }
            set
            {
                this["InstallmentTable"] = Newtonsoft.Json.JsonConvert.SerializeObject(value);
            }
        }



        public bool SeparateServiceFeeTransaction
        {
            get { return Get<bool>("SeparateServiceFeeTransaction"); }
            set { this["SeparateServiceFeeTransaction"] = value; }
        }


        public string UserAgent
        {
            get { return Get<string>("UserAgent"); }
            set { this["UserAgent"] = value; }
        }

        public string BrowserHttpAcceptHeader
        {
            get { return Get<string>("BrowserHttpAcceptHeader"); }
            set { this["BrowserHttpAcceptHeader"] = value; }
        }


        public bool RunFraudCheck
        {
            get { return Get<bool>("RunFraudCheck"); }
            set { this["RunFraudCheck"] = value; }
        }

        public Guid? RateCardRefId
        {
            get { return Get<Guid?>("RateCardRefId"); }
            set { this["RateCardRefId"] = value.ToString(); }
        }


        public List<List<OrderDetailTable>> OrderTransactions
        {
            get
            {
                var orderTransactions = new List<List<OrderDetailTable>>();


                if (SeparateServiceFeeTransaction
                    && OrderDetailTable != null
                    && OrderDetailTable.Any(item => item.IsServiceFee)
                    && !OrderDetailTable.Any(item => item.IsFreeTrial))
                {
                    orderTransactions.Add(new List<OrderDetailTable>(OrderDetailTable.Where(item => !item.IsServiceFee)));
                    orderTransactions.Add(new List<OrderDetailTable>(OrderDetailTable.Where(item => item.IsServiceFee)));
                }
                else
                {
                    orderTransactions.Add(OrderDetailTable);
                }

                return orderTransactions;
            }
        }


        public string GetIPAddressAsString()
        {
            return String.Format("{0}.{1}.{2}.{3}",  ClientIp1, ClientIp2, ClientIp3, ClientIp4);
        }

    }
}
