﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.ProxyContracts
{
    public class DineroMailOfflinePaymentRequestMessage: OfflinePaymentRequestMessage
    {

        public string MerchantReference
        {
            get { return Get<String>("MerchantReference"); }
            set { this["MerchantReference"] = value; }
        }

        public string SkinCode
        {
            get { return Get<String>("SkinCode"); }
            set { this["SkinCode"] = value; }
        }

        public string MerchantSig
        {
            get { return Get<String>("MerchantSig"); }
            set { this["MerchantSig"] = value; }
        }

        public DineroMailAuthResult AuthResult
        {
            get { return (DineroMailAuthResult)Get<int>("AuthResult"); }
            set { this["AuthResult"] = value; }
        }

        public string ShopperLocale
        {
            get { return Get<String>("ShopperLocale");  }
            set { this["ShopperLocale"] = value;  }
        }

        public string PaymentMethod
        {
            get { return Get<String>("PaymentMethod"); }
            set { this["PaymentMethod"] = value;  }
        }

        public string PspReference
        {
            get { return Get<String>("PspReference"); }
            set { this["PspReference"] = value; }
        }

        public enum DineroMailAuthResult
        {
            AUTHORISED, REFUSED, CANCELLED, PENDING, ERROR
        }
    }
}
