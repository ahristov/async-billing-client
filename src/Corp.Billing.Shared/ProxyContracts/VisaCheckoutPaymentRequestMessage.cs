﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public class VisaCheckoutPaymentRequestMessage : CreditCardWithExternalWalletPaymentRequestMessage
    {
        public VisaCheckoutPaymentRequestMessage()
        {
            CommandType = PaymentProxyCommandType.VisaCheckoutOrderProcessing;
        }

        public string VCOCallId
        {
            get { return Get<string>("VCOCallId"); }
            set { this["VCOCallId"] = value; }
        }

        public string VCOEncKey
        {
            get { return Get<string>("VCOEncKey"); }
            set { this["VCOEncKey"] = value; }
        }

        public string VCOEncPaymentData
        {
            get { return Get<string>("VCOEncPaymentData"); }
            set { this["VCOEncPaymentData"] = value; }
        }

        // Visa checkout amount (it is the actual order amount)
        public decimal VCOAmount
        {
            get { return Get<decimal>("VCOAmount"); }
            set { this["VCOAmount"] = value; }
        }

    }
}
