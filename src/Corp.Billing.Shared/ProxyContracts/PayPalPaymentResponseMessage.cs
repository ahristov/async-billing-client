﻿namespace Corp.Billing.Shared.ProxyContracts
{
    public class PayPalPaymentResponseMessage : OnlinePaymentResponseMessage
    {
        public bool IsStackedSub
        {
            get { return Get<bool>("IsStackedSub"); }
            set { this["IsStackedSub"] = value; }
        }
    }
}
