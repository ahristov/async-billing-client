﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Corp.Billing.Shared.Facilities
{
    public static class ExceptionExtentions
    {
        public static Exception AddData(this Exception exception, object info)
        {
            return SetData(exception, info);
        }

        public static Exception WithData(this Exception exception, string name, object value)
        {
            return SetData(exception, name, value);
        }

        private static Exception SetData(this Exception exception, object info)
        {
            Dictionary<string, object> data = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            if (info is IEnumerable<KeyValuePair<string, object>>)
            {
                foreach (var item in (info as IEnumerable<KeyValuePair<string, object>>))
                {
                    if (item.Value == null)
                    {
                        SetData(exception, item.Key, null);
                        continue;
                    }

                    string value = item.Value.ToString();

                    if (value == item.Value.GetType().FullName)
                    {
                        value = Newtonsoft.Json.JsonConvert.SerializeObject(item.Value);
                    }

                    SetData(exception, item.Key, value);
                }
            }
            else
            {
                foreach (PropertyInfo pi in info.GetType().GetProperties())
                {
                    try
                    {
                        var value = pi.GetValue(info);

                        if (value == null)
                        {
                            SetData(exception, pi.Name, null);
                            continue;
                        }

                        if (value is Newtonsoft.Json.Linq.JObject || value.ToString() == value.GetType().FullName)
                        {
                            value = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                        }

                        SetData(exception, pi.Name, value);
                    }
                    catch { }
                }
            }

            return exception;
        }

        private static Exception SetData(this Exception exception, string name, object value)
        {
            string fullName = string.Format("ExceptionManager.{0}.{1}", exception.GetType().Name, name);

            exception.Data[fullName] = value;

            return exception;
        }

    }

}
