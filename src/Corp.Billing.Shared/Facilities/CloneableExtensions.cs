﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Facilities
{
    public static class CloneableExtensions
    {
        public static T CloneTyped<T>(this T obj)
            where T: ICloneable
        {
            if (obj == null)
                return default(T);

            return (T)obj.Clone();
        }

        public static IEnumerable<T> CloneTypedCollection<T>(this IEnumerable<T> c)
            where T : ICloneable
        {
            if (c != null)
            {
                foreach (var item in c)
                    yield return item.CloneTyped();
            }
        }

        public static string CloneString(this string s)
        {
            return (s == null)
                ? s
                : new System.Text.StringBuilder(s).ToString();
        }
    }
}
