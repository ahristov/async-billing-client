﻿using System;

namespace Corp.Billing.Shared.Domain.Profile
{
    public class GetUserProfileLiteRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    public class GetUserProfileLiteResult
    {
        public string Handle { get; set; }
        public string Email { get; set; }
        public byte PrimaryPhotoId { get; set; }
        public string PrimaryPhotoApprovalState { get; set; }
        public int PhotoCount { get; set; }
        public bool IsProfileOn { get; set; }
        public byte Gender { get; set; }
        public string Headline { get; set; }
        public Guid PhotoImageId { get; set; }
        public byte GenderGenderSeek { get; set; }
        public short CountryCode { get; set; }
        public byte StateCode { get; set; }
        public DateTime SignUpDate { get; set; }
        public DateTime Birthday { get; set; }
        public short UrlCode { get; set; }
        public int BrandId { get; set; }
        public int BillingId { get; set; }
        public Guid TestingGuid { get; set; }
        public short SiteCode { get; set; }
        public DateTime PrevLoginDate { get; set; }
        public DateTime PrevSubPaidThroughDate { get; set; }
        public byte LowConvertingChannel { get; set; }
        public bool HasPhoto { get; set; }
        public bool HasPrimaryPhotoPending { get; set; }
        public int ReturnValue { get; set; }

    }

}
