﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Care
{
    public class PromoDisplayRequest
    {
        public int UserID { get; set; }
        public bool SkipRuleChecks { get; set; }
    }
    public class PromoDisplayResult
    {
        public int ReturnValue { get; set; }
        public int UserID { get;set;}
        public short StateCode { get; set; }
        public short CountryCode { get; set; }
        public int BrandID {get;set;}
        public short Age { get; set; }
        public List<PromoDisplayItem> PromoList{get;set;}

    }

    public class PromoDisplayItem
    {
        public string PromoID { get; set; }
        public string PromoName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PromoDesc { get; set; }
        public string OfferType { get; set; }
        public string FreeTrialProdDtlID {get; set;}
        public List<BaseFeature> BaseFeatureList {get; set;}
    }

    public class BaseFeature{
        public short ProdDtlID {get;set;}
        public byte Months { get; set; }
        public decimal InitialAmt {get;set;}
        public decimal RenewalAmt {get;set;}
        public short InitialDays{get;set;}
        public short RenewalDays {get;set;}
        public short FreeDays {get;set;}
        public decimal DiscountAmt {get;set;}
        public decimal DiscountPrct {get;set;}
        public string ISOCurrencyCode {get;set;}
        public bool IsBundledBit {get;set;}
        public bool ShowAddOnPage {get;set;}
    }

    public class UserPromoIDResult
    {
        public int ReturnValue { get; set; }
        public string PromoID { get; set; }
        public int BrandID { get; set; }
        public short URLCode { get; set; }
        public string DefaultBrandPromoID { get; set; }

    }
    public class PromoDisplayAddOnsResult
    {
        public int ReturnValue { get; set; }
        public int UserID { get; set; }
        public short StateCode { get; set; }
        public short CountryCode { get; set; }
        public int BrandID { get; set; }
        public short Age { get; set; }
        public DateTime OldUserStatusDt { get; set; }
        public int AcctOrderModID { get; set; }
        public int GeoStateTaxID { get; set; }
        public List<PromoAddOnDisplayItem> PromoAddOnList { get; set; }
    }

    public class PromoAddOnDisplayItem
    {
        public string PromoID { get; set; }
        public string PromoName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PromoDesc { get; set; }
        public string OfferType { get; set; }
        public List<AddonItem> AddOnItem { get; set; }
    }
    public class AddonItem
    {
        public short ProdDtlID { get; set; }
        public byte Months { get; set; }
        public DateTime BeginDt { get; set; }
        public decimal InitialAmt { get; set; }
        public decimal RenewalAmt { get; set; }
        public short InitialDays { get; set; }
        public short RenewalDays { get; set; }
        public string Descr { get; set; }
        public string OrderString { get; set; }
        public decimal InitialTax { get; set; }
        public decimal RenewalTax { get; set; }
        public string ISOCurrencyCode { get; set; }
        public bool BundlePreOrder { get; set; }
        public byte ProdFeatureID { get; set; }
    }

}



