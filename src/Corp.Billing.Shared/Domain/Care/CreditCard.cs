﻿using System.Collections.Generic;


namespace Corp.Billing.Shared.Domain.Care
{

    public class CSACreditCardListRequest
    {
        public int UserID { get; set; }
        public string CreditCardType { get; set; }
    }

    public class CSACreditCardListResult
    {
        public int ReturnValue { get; set; }
        public List<CreditCardItem> CreditCardList { get; set; }
    }


    public class CreditCardItem
    {
        public int Id {get;set;}
        public string Name {get;set;}
    }
}
