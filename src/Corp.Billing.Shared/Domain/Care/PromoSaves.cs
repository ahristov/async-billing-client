﻿using System;
using System.Collections.Generic;


namespace Corp.Billing.Shared.Domain.Care
{

    public class PromoSavesRequest
    {
        public bool Save { get; set; }
    }

    public class PromoSavesResult
    {
        public int ReturnValue { get; set; }
        public List<PromoItem> PromoSavesList { get; set; }

    }


    public class PromoItem
    {
        public string PromoID { get; set; }
        public string PromoName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PromoDesc { get; set; }
        public string PromoType { get; set; }
    }
}
