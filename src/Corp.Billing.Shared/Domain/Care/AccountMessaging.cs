﻿namespace Corp.Billing.Shared.Domain.Care
{
    public class AccountNoteRequest
    {
        public short ActivityId { get; set; }
        public int UserId { get; set; }
        public string Comment { get; set; }
        public int EmployeeId { get; set; }
        public string CustomData { get; set; }
    }

    public class AccountNoteAddResult
    {
        public int ReturnValue { get; set; }
    }

    public enum ActivityType : short
    {
        GeneralBillingNote = 5,
        GeneralAccountNote = 18,
        ReactivationAutomaticRenewal = 62,
        AppleIAPIssues = 315,
    }
}
