﻿using System;
using System.Collections.Generic;


namespace Corp.Billing.Shared.Domain.Care
{

    public class CSACreateOrderRequest
    {
        public int UserID { get; set; }
        public short AcctOrderModID { get; set; }
        public DateTime OldUserStatusDt { get; set; }
        public short GeoStateTaxID { get; set; }
        public bool CheckCC { get; set; }
        public string PromoID { get; set; }
        public string OrderList { get; set; }
        public int STID { get; set; }
        public int AcctOrderID { get; set; }
        public int TrxID { get; set; }
        public bool IsComplimentary { get; set; }
        public int EmployeeID { get; set; }
        public byte TrxSrcID { get; set; }
        public string Comment { get; set; }
        public short CRCode { get; set; }
        public string DtlCode { get; set; }
        public byte AcctPymtMethTypeID { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string FullName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public byte PState { get; set; }
        public short PCountry { get; set; }
        public string PostalCode { get; set; }
        public byte CCMonth { get; set; }
        public short CCYear { get; set; }
        public string CheckNum { get; set; }
        public string DriverLic { get; set; }
        public string DLStateAbv { get; set; }
        public string DLCountryAbv { get; set; }
        public string AcctNumClear { get; set; }
        public string CardStartDT { get; set; }
        public string CardIssueNum{ get; set; }
    }

    public class CSACreateOrderResult
    {
        public int ReturnValue { get; set; }
        public int ProcReturn { get; set; }
        public int AccountOrderID { get; set; }
        public int TrxID { get; set; }
        public short SubscribeTextID { get; set; }
        public string ErrorMessage {get; set;}
       
    }

    public class CSAOrderDetailRequest
    {
        public int UserId { get; set; }
        public string ProdDtlIDList { get; set; }
        public string PromoID { get; set; }
        public short URLCode{ get; set; }
        public byte? State { get; set; }
        public short? Country { get; set; }
    }

    public class CSAOrderDetailResponse
    {
        public int ReturnValue { get; set; }
        public List<CSAOrderDetailResult> OrderDetail { get; set; }
    }

    public class CSAOrderDetailResult
    {
        public int ReturnValue { get; set; }
        public string ISOCurrencyCode { get; set; }
        public string OrderString { get; set; }
        public string Description { get; set; }
        public decimal RenewalTax { get; set; }
        public decimal InitialTax { get; set; }
        public byte Months { get; set; }
        public bool IsFreeTrial { get; set; }
        public short RenewalDays { get; set; }
        public decimal RenewalPrice { get; set; }
        public decimal SalesPrice { get; set; }
        public short InitialDays { get; set; }
        public DateTime BeginDate { get; set; }
        public string ProdDtlId { get; set; }
        public bool BundlePreOrdered { get; set; }
        public bool DelayCapture { get; set; }
        public DateTime? CancelDate { get; set; }
        public byte? ProdFeatureID { get; set; }
        public string ProdId { get; set; }
        public string PromoId { get; set; }
    }
}
