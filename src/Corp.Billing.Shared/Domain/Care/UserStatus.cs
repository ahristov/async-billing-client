﻿namespace Corp.Billing.Shared.Domain.Care
{
    public class CSAUserStatusRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    public class CSAUserStatusResult
    {
        public int ReturnValue { get; set; }
        public bool CanPurchaseBase { get; set; }
        public bool CanPurchaseAddOn { get; set; }
        public byte IneligibilityReason { get; set; }
        public int SubUserState { get; set; }
        public byte URLCode { get; set; }
        public int BrandID { get; set; }
        public byte Age { get; set; }
        public bool CanPurchaseGuarantee { get; set; }
        public string ISOCurrencyCode { get; set; }
        public string FullName { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public byte StateCode { get; set; }
        public string PostalCode { get; set; }
        public short CountryCode { get; set; }
        public byte AcctPymtMethTypeID { get; set; }
        public string AcctNum { get; set; }
        public byte CCMonth { get; set; }
        public short CCYear { get; set; }
        public string BaseCode { get; set; }
    }



    public class CSABillingSearchResult
    {
        public int ReturnValue { get; set; }
        public string PmtMethodType { get; set; }
        public string PmtType { get; set; }
        public string Email { get; set; }
        public string LastEmailSentDate { get; set; }
        public string Handle { get; set; }
        public string Password { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string City { get; set; }
        public int State { get; set; }
        public int Country { get; set; }
        public string PostalCode { get; set; }
        public string LoginDate { get; set; }
        public string CheckNum { get; set; }
        public string RoutingNum { get; set; }
        public string AcctNum { get; set; }
        public string CCExpMonth { get; set; }
        public string CCExpYear { get; set; }
        public string RenewAmt { get; set; }
        public string PromoName { get; set; }
        public short ResignCode { get; set; }
        public string ResignReason { get; set; }
        public string ResignDate { get; set; }
        public string ResignComment { get; set; }
        public string OriginalAmt { get; set; }
        public short OriginalTerm { get; set; }
        public string SubscribeDate { get; set; }
        public string CCType { get; set; }
        public short PState { get; set; }
        public short PCountry { get; set; }
        public string CCZip { get; set; }
        public string CCFName { get; set; }
        public string CCLName { get; set; }
        public string CCAddress1 { get; set; }
        public string CCAddress2 { get; set; }
        public string CCCity { get; set; }
        public int CCState { get; set; }
        public int CCCountry { get; set; }
        public string DriverLic { get; set; }
        public int OrderNumber { get; set; }
        public short StateTextID { get; set; }
        public string StateText { get; set; }
        public string PymtMethCreateDate { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public string CancelType { get; set; }
        public int AcctPymtMethID { get; set; }
        public int AcctPymtMethTypeID { get; set; }
        public string FullName { get; set; }
        public int URLCode { get; set; }
        public bool HasBillingData { get; set; }
        public string Domain { get; set; }
        public byte BillingState { get; set; }
        public bool LoyaltyPrice { get; set; }
        public string ISOCurrencyCode { get; set; }
        public string MobilePhone { get; set; }
        public string MobileCarrier { get; set; }
        public string MobileCarrierPhone { get; set; }
        public string SubscriptionIP { get; set; }
        public bool WillRenew { get; set; }

    }
}
