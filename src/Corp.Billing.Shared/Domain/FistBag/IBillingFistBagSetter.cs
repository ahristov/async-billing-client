﻿namespace Corp.Billing.Shared.Domain.FistBag
{
    public interface IBillingFistBagSetter
    {
        SetBillingFistBagResult SetFistBagData(SetBillingFistBagRequest request);
    }
}
