﻿using System;

namespace Corp.Billing.Shared.Domain.FistBag
{
    [Serializable]
    public class PriceTargetingData
    {
        public int Version { get; set; }
        public double Score { get; set; }
        public bool Calculated { get; set; }
        public bool NeverSubscribed { get; set; }
        public DateTime? MTime { get; set; }
    }
}
