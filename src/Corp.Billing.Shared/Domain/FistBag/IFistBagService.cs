﻿namespace Corp.Billing.Shared.Domain.FistBag
{
    public interface IFistBagService
    {
        BillingFistBagData GetFistBagData(int userId);
        void SetFistBagData(int userId, BillingFistBagData fistBagData);
    }
}
