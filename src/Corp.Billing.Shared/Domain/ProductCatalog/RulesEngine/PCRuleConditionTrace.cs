﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine
{
    public class PCRuleConditionTrace
    {
        public string RuleType { get; set; }
        public string RuleName { get; set; }
        public bool RuleNegation { get; set; }
        public string RuleValue { get; set; }
        public string DataValue { get; set; }
        public bool Result { get; set; }

        public string GetDbgString()
        {
            return
                string.Format(
                    "PC: RuleType:{0,-30}, RuleName:{1,-50}, RuleValue:{2,-5}{3,-50}, DataValue:{4,-20}, Result:{5,-20}",
                    TrimMax(RuleType, 30),
                    TrimMax(RuleName, 50),
                    RuleNegation ? "!" : "",
                    TrimMax(RuleValue, 50),
                    TrimMax(DataValue, 20),
                    Result, 20);
        }

        private string TrimMax(string s, int max)
        {
            var res = (s ?? string.Empty).Trim();
            return (res.Length <= max)
                ? res
                : res.Substring(0, max);
        }
    }
}
