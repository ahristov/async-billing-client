﻿using Corp.Billing.Shared.ApiClient.Base;
using Corp.Billing.Shared.Domain.Affiliate;
using Corp.Billing.Shared.Domain.FistBag;
using Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride;
using Corp.Billing.Shared.Domain.ProductCatalog.RateCards;
using Corp.Billing.Shared.Domain.RateCardV3.Data;
using Corp.Billing.Shared.Domain.RulesEngine;
using Corp.Billing.Shared.Domain.Subscription;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine
{
    public interface IPCRulesData : IHasTraceParams
    {
        // Context data

        int UserId { get; }
        bool IsLoggedIn { get; }
        string UserAgent { get; }
        bool IsMobileUserAgent { get; }
        string AppVersion { get; }
        byte? PlatformId { get; }
        byte? RegistrationPlatformId { get; }
        bool RegistrationIsMobile { get; }
        string SessionPromoId { get; }
        bool PriceUpParam { get; }
        bool SkipOnApplyOnlyDiscountsParam { get; }
        byte? ProdFeatureId { get; }
        PriceOverrideUpsellTypeEnum PriceOverrideUpsellTypeParam { get; }
        RequestedRateCardType RequestedRateCardType { get; }
        CatalogType CatalogTypeParam { get; }
        CatalogSource CatalogSourceParam { get; }


        // IUserBillingDataGetter data

        DateTime SignUpDate { get; }
        int Age { get; }
        string Handle { get; }
        short SiteCode { get; }
        short CountryCode { get; }
        short StateCode { get; }
        short UrlCode { get; }

        byte GenderCode { get; }
        byte GenderGenderSeekCode { get; }
        bool IsProfileOn { get; }

        int UserBrandId { get; }
        int BillingId { get; }
        bool HasDefaultPaymentMethod { get; }

        Guid? TestingGuid { get; }
        bool HasAnyPhoto { get; }
        bool HasPhoto { get; }
        DateTime PreviousLoginDate { get; }
        DateTime PrevSubPaidThroughDate { get; }
        DateTime PrevSubEndDateForBaseFeature { get; }
        DateTime MaxBaseEndDt { get; }
        DateTime MaxBaseEndDtFTC { get; }
        double BaseDaysRemaining { get; }
        double BaseFTCDaysRemaining { get; }
        bool IsIAPSubscriber { get; }
        SubscriptionPackageTypeEnum SubscriptionPackageType { get; }
        IList<int> LastProdFeatureList { get; }
        long CurrentPrvFeatures { get; }
        bool HasPendingPrimaryPhoto { get; }
        int LowConvertingChannel { get; }
        string ChannelName { get; }
        string BrandNonBrandRollUp { get; }

        int UserPriceGroupId { get; }
        int CBSACode { get; }
        string NextISOCurrencyCode { get; }
        string LastISOCurrencyCode { get; }

        GetUserBillingDataResult.SubscriberStateType SubscriberState { get; }
        UserSubStatusResult.UserSubStatusType UserSubStatus { get;  }

        DateTime? FirstSubDate { get; }



        // IUserSubscriptionStatusGetter data

        bool IsSubscriber { get; }
        bool NeverSubscribed { get; }
        bool IsFreeTrialUser { get; }
        bool IsExpiredFreeTrialUser { get;  }
        bool IsCharterMember { get; }
        bool IsPastSubscriber { get; }
        bool IsResignedCurentSub { get;  }
        bool CanPurchaseBase { get; }

        byte LastSubMonths { get; }
        bool LastSubBundled { get; }
        DateTime? LastFTBeginDt { get; }
        DateTime? CurrentSubscriptionBeginDt { get; }
        bool IsFailedRenewal { get; }

        // ICBSAGroupGetter data
        int CBSAGroup { get; }

        // IUserPricingScoreDataGetter data
        float GetUserPricingScore();

        // IBrandInfoGetter data
        BrandInfoResult BrandInfo { get; }

        // IUserPromoInfoGetter data
        string PromoIdLastApplied { get; }

        // FistBag
        PriceTargetingData PriceTargetingData { get; }
        bool HasSubmitedPhotoPriorTheFirstRateCardView { get; }

        bool IsInPromoUsersList(string promoId);
        bool IsInDiscountScheduleListByUser(string promoId);
        bool IsInDiscountScheduleListByPromo(string promoId);

        /// <summary>
        /// Returns the persisted platform ID data if it exists, otherwise persists the current data and returns that instead.
        /// </summary>
        /// <returns></returns>
        PersistedPlatformIdDataResult GetOrSetPersistedPlatformIdData();
    }
}
