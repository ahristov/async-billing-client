﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine
{
    public static class PCRulesRequestExtensions
    {
        public static void CopyTo(this PCRulesRequest source, PCRulesRequest target)
        {
            target.TraceLevel = source.TraceLevel;
            target.TraceVerbosity = source.TraceVerbosity;

            target.UserId = source.UserId;
            target.PlatformId = source.PlatformId;
            target.SessionId = source.SessionId;
            target.SiteCode = source.SiteCode;
            target.UserAgent = source.UserAgent;
            target.AppVersion = source.AppVersion;

            target.PriceUpParam = source.PriceUpParam;
            target.SkipOnApplyOnlyDiscountsParam = source.SkipOnApplyOnlyDiscountsParam;
            target.ProdFeatureId = source.ProdFeatureId;

            target.CatalogTypeParam = source.CatalogTypeParam;
            target.CatalogSourceParam = source.CatalogSourceParam;

            target.PromoId = source.PromoId;
        }

        public static T InitFrom<T>(this T target, PCRulesRequest source)
            where T : PCRulesRequest
        {
            source.CopyTo(target);
            return target;
        }

        public static PCRulesRequest CreateFrom(this PCRulesRequest source)
        {
            var target = new PCRulesRequest();
            source.CopyTo(target);
            return target;
        }

    }
}
