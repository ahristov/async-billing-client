﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public class PriceOverrideResultV2 : PriceOverrideRulesResultV2, IPriceOverrideResult
    {
        public PriceOverrideRuleCollection PriceOverrides { get; set; }

        public PriceOverrideResultV2()
        {
            PriceOverrides = new PriceOverrideRuleCollection();
        }

        public static PriceOverrideResultV2 InitFrom(PriceOverrideRulesResultV2 copyFrom)
        {
            var res = new PriceOverrideResultV2();

            if (copyFrom != null)
            {
                copyFrom.CopyTo(res);

                res.RateCardOverrideKey = copyFrom.RateCardOverrideKey;
                res.UpsellType = copyFrom.UpsellType;
            }

            return res;
        }
    }
}
