﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public interface IPriceOverrideRulesEvaluatorV2
    {
        PriceOverrideRulesResultV2 GetPriceOverrideRulesResultV2(PCRulesRequest request);
    }
}
