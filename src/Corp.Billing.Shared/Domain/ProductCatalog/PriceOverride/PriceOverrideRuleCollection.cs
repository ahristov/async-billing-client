﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public class PriceOverrideRuleCollection
    {
        public List<PriceOverrideRuleValue> Data { get; set; }

        public PriceOverrideRuleCollection()
        {
            Data = new List<PriceOverrideRuleValue>();
        }
    }
}
