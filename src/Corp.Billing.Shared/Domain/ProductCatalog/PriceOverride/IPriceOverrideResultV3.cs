﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public interface IPriceOverrideResultV3 : IPriceOverrideResult
    {
        IList<int> RateCardOverrideKeys { get; }
    }
}
