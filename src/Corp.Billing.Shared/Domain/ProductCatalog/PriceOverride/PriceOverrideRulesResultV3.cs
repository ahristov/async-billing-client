﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using System.Collections.Generic;
using System.Linq;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public class PriceOverrideRulesResultV3 : PCRulesResult
    {
        public IList<int> RateCardOverrideKeys { get; set; }
        public PriceOverrideUpsellTypeEnum UpsellType { get; set; }

        public int? RateCardOverrideKey
        {
            get
            {
                if (RateCardOverrideKeys == null
                    || RateCardOverrideKeys.Count == 0)
                    return null;

                return RateCardOverrideKeys.LastOrDefault();
            }
        }

        public PriceOverrideRulesResultV3()
        {
            RateCardOverrideKeys = new List<int>();
        }
    }
}
