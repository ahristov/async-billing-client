﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride
{
    public static class PriceOverrideRulesResultV3Extensions
    {
        public static void CopyTo(this PriceOverrideRulesResultV3 source, PriceOverrideRulesResultV3 target)
        {
            PCRulesResultExtensions.CopyTo(source, target);

            target.RuleId = source.RuleId;
            target.RuleResults = source.RuleResults;
            target.RuleConditionTraces.AddRange(source.RuleConditionTraces.ToArray());
        }

        public static T InitFrom<T>(this T target, PriceOverrideRulesResultV3 source)
            where T : PriceOverrideRulesResultV3
        {
            source.CopyTo(target);
            return target;
        }

        public static PriceOverrideRulesResultV3 CreateFrom(this PriceOverrideRulesResultV3 source)
        {
            var target = new PriceOverrideRulesResultV3();
            source.CopyTo(target);
            return target;
        }
    }
}
