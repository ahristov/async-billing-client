﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.RateCards
{
    public class RateCardXmlDataRequest : IApiRequestWithQueryString
    {
        public int RateCardKey { get; set; }
    }

    public class RateCardXmlDataResult
    {
        public string RateCard { get; set; }
    }
}
