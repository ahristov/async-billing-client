﻿namespace Corp.Billing.Shared.Domain.ProductCatalog.RateCards
{
    public enum RequestedRateCardType
    {
        Undefined,
        BaseRateCard,
        AddOnsRateCard,
        PowerUpsRateCard,
        UnitProductsRateCard,
        AddOnsBundleRateCard,
    }
}
