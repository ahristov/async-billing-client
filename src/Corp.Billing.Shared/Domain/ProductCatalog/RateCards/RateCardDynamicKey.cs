﻿using System;

namespace Corp.Billing.Shared.Domain.ProductCatalog.RateCards
{
    public class RateCardDynamicKeyRequest : IApiRequestWithQueryString
    {
        public int RootRateCardKey { get; set; }
        public string CsvAppliedOverrideKeys { get; set; }
    }

    [Serializable]
    public class RateCardDynamicKeyResult
    {
        public int RateCardKey { get; set; }
    }
}
