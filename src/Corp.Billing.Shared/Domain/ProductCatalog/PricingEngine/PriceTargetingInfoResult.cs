﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PricingEngine
{
    public class PriceTargetingInfoResult : PCRulesResult
    {
        public string PriceTargetingPromoId { get; set; }
    }

}
