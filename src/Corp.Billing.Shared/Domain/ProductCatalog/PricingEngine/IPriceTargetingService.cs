﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;

namespace Corp.Billing.Shared.Domain.ProductCatalog.PricingEngine
{
    public interface IPriceTargetingService
    {
        PriceTargetingInfoResult GetPriceTargetingInfo(PCRulesRequest request);
        PriceTargetingInfoV2Result GetPriceTargetingInfoV2(PCRulesRequest request);
    }
}
