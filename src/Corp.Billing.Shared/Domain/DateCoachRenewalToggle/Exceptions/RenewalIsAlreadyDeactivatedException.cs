﻿using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;

namespace Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Exceptions
{
    public class RenewalIsAlreadyDeactivatedException : BaseDateCoachRenewalToggleException
    {
        public RenewalIsAlreadyDeactivatedException(DateCoachRenewalToggleRequest request)
            : base(request)
        { }

        public override int ErrorCode => (int)ExceptionErrorCodes.RenewalIsAlreadyDeactivated;

        public override string ErrorMessage => "Date coach is already deactivated";
    }
}
