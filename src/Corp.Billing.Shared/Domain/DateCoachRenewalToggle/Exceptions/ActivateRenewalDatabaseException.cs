﻿using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;

namespace Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Exceptions
{
    public class ActivateRenewalDatabaseException : BaseDateCoachRenewalToggleException
    {
        public ActivateRenewalDatabaseException(DateCoachRenewalToggleRequest request)
            : base(request)
        { }

        public override int ErrorCode => (int)ExceptionErrorCodes.DatabaseErrorActivatingRenewal;

        public override string ErrorMessage => "Database error activating renewal";
    }
}
