﻿using System;

namespace Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data
{
    [Serializable]
    public class DateCoachRenewalToggleRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
        public DateCoachRenewalToggleRequestType ToggleType { get; set; }
    }
}
