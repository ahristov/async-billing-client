﻿namespace Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data
{
    public enum DateCoachRenewalToggleRequestType : byte
    {
        Toggle=0,
        Deactivate=1,
        Activate=2,
    }
}
