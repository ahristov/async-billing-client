﻿using Corp.Billing.Shared.Domain.DateCoachStatus.Data;
using System;
using System.Net;

namespace Corp.Billing.Shared.Domain.DateCoachStatus.Exceptions
{
    public abstract class BaseDateCoachStatusException : Exception
    {
        public int UserId { get; private set; }

        public BaseDateCoachStatusException(DateCoachStatusRequest request)
        {
            UserId = request.UserId;
        }

        public abstract int ErrorCode { get; }
        public abstract string ErrorMessage { get; }

        public virtual HttpStatusCode HttpStatusCode { get { return (HttpStatusCode)422; } }
    }
}
