﻿using Corp.Billing.Shared.Domain.DateCoachStatus.Data;
using System.Net;

namespace Corp.Billing.Shared.Domain.DateCoachStatus.Exceptions
{
    public class InvalidUserIdException : BaseDateCoachStatusException
    {
        public InvalidUserIdException(DateCoachStatusRequest request)
            : base(request)
        { }

        public override int ErrorCode => (int)ExceptionErrorCodes.InvalidUserId;

        public override string ErrorMessage => "Invalid user ID";

        public override HttpStatusCode HttpStatusCode => HttpStatusCode.BadRequest;
    }
}
