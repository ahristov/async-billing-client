﻿using Corp.Billing.Shared.Domain.DateCoachStatus.Data;
using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.DateCoachStatus
{
    public interface IDateCoachUserSessionInfoDataGetter : IResetableCache<int>
    {
        DateCoachUserSessionInfoDataResult GetDateCoachUserSessionInfo(DateCoachUserSessionInfoDataRequest request);
    }
}
