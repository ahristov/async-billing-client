﻿using System;

namespace Corp.Billing.Shared.Domain.LegalTexts
{
    [Serializable]
    public class LegalTextsResponse : ILegalTextsResponse
    {
        public string LegalDisclaimerTermsKey { get; set; }
        public string LegalDisclaimerTerms { get; set; }
    }
}
