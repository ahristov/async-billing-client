﻿namespace Corp.Billing.Shared.Domain.LegalTexts
{
    public enum LegalTextPlacementType
    {
        Undefined,
        Payment,
        UpdateMOP,
        Confirmation,
        ConfirmationInstl,
        MemberUpgrade,
        SaveOffer3for1,
        SaveOfferRenewalDiscount,
        RenewalDiscountConfirmaton,
    }
}
