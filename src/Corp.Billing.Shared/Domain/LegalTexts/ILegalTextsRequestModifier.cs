﻿namespace Corp.Billing.Shared.Domain.LegalTexts
{
    public interface ILegalTextsRequestModifier
    {
        void Modify(LegalTextsRequest req);
    }
}
