﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Location
{
    public interface ICBSAGroupGetter
    {
        /// <summary>
        /// Retrieves the mappings of CBSA code -> CBSA group
        /// </summary>
        /// <returns></returns>
        IDictionary<int, int> GetCBSAGroupMappings();
    }
}
