﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.Domain.Chargebacks
{
    public class CreateChargebackFileRequest
    {
        public ChargebackProviderType Provider { get; set; }
        public string Filename { get; set; }
    }
}
