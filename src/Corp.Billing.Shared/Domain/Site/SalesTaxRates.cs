﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Site
{
    public class SalesTaxRates
    {
        // GetSalesTax: API Request ////////////////////////////////

        public class GetSalesTaxRatesRequest : IApiRequestWithQueryString
        {
            public short SiteCode { get; set; }
            public short CountryCode { get; set; }
            public int StateCode { get; set; }
            public string PostalCode { get; set; }
            public byte? ProductFeatureId { get; set; }
        }

        public class GetSalesTaxRatesByUserRequest : IApiRequestWithQueryString
        {
            public int UserId { get; set; }
            public byte? ProductFeatureId { get; set; }
        }


        // GetSalesTax: API Response ////////////////////////////////

        public class GetSalesTaxRatesResponse
        {
            public byte luGeoTaxTypeID { get; set; }
            public int GeoTaxID { get; set; }
            public decimal TaxRate { get; set; }
            public byte ProductFeatureId { get; set; }
            public object Clone()
            {
                return this.MemberwiseClone();
            }
        }


        // GetSalesTax: Data Result ////////////////////////////////

        public class GetSalesTaxRatesDataResult
        {
            public int ReturnValue { get; set; }
            public IList<GetSalesTaxRatesStateTaxItem> TaxesPerState { get; set; }
            public IList<GetSalesTaxRatesZipCodeTaxItem> TaxesPerZipCode { get; set; }

            public GetSalesTaxRatesDataResult()
            {
                TaxesPerState = new List<GetSalesTaxRatesStateTaxItem>();
                TaxesPerZipCode = new List<GetSalesTaxRatesZipCodeTaxItem>();
            }
        }


        public class GetSalesTaxRatesStateTaxItem : GetSalesTaxRatesResponse
        {
            // Fields to query
            public short SiteCode { get; set; }
            public short CountryCode { get; set; }
            public int StateCode { get; set; }
            public bool ZipCodeTaxing { get; set; }

            // Fields for the result are inherited
        }


        public class GetSalesTaxRatesZipCodeTaxItem : GetSalesTaxRatesResponse
        {
            // Fields to query
            public short CountryCode { get; set; }
            public int StateCode { get; set; }
            public string PostalCode { get; set; }

            // Fields for the result are inherited
        }
    }
}
