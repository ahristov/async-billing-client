﻿namespace Corp.Billing.Shared.Domain.Site
{
    public interface ISiteFeaturesGetter
    {
        SiteFeatures.GetSiteFeaturesResult GetSiteFeatures();
    }
}
