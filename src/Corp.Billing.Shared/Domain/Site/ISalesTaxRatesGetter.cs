﻿namespace Corp.Billing.Shared.Domain.Site
{
    public interface ISalesTaxRatesGetter
    {
        SalesTaxRates.GetSalesTaxRatesDataResult GetSalesTaxRatesV2();
        SalesTaxRates.GetSalesTaxRatesDataResult GetSalesTaxRatesV3();
    }
}
