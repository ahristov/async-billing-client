﻿namespace Corp.Billing.Shared.Domain.Site
{
    public interface ISitePaymentMatrixGetter
    {
        SitePaymentMatrix.GetSitePaymentMatrixResult GetSitePaymentMatrix();
    }
}
