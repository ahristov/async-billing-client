﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Corp.Billing.Shared.Domain.Site
{
    public static class SiteMatrixExtensions
    {
        public static IList<SitePaymentMatrix.GetSitePaymentMatrixItem> Filter(
            this IList<SitePaymentMatrix.GetSitePaymentMatrixItem> items,
            SitePaymentMatrix.GetSitePaymentMatrixRequest request)
        {
            var matrix = items.Where(r =>
                    (request.IncudeNotActive || r.Active.GetValueOrDefault()) &&
                    (request.IncludeNotApplicable || r.PaymentServiceCategoryId.GetValueOrDefault() != 0) &&
                    (request.IncludeInternal || r.PaymentProcessorId.GetValueOrDefault() != 1)
                ).ToList();

            List<SitePaymentMatrix.GetSitePaymentMatrixItem> result = new List<SitePaymentMatrix.GetSitePaymentMatrixItem>();

            var predicates = new List<Func<SitePaymentMatrix.GetSitePaymentMatrixItem, bool>>()
            {
                r =>
                    (r.SiteCode == request.SiteCode) &&
                    (r.UrlCode == request.UrlCode) &&
                    (request.CountryCode.HasValue && r.CountryCode.HasValue && r.CountryCode.Value == request.CountryCode.Value) && 
                    (!string.IsNullOrWhiteSpace(request.ISOCurrencyCode) && !string.IsNullOrWhiteSpace(r.PresentationCurrencyCode) && r.PresentationCurrencyCode == request.ISOCurrencyCode),
                r =>
                    (r.SiteCode == request.SiteCode) &&
                    (r.UrlCode == request.UrlCode) &&
                    (!r.CountryCode.HasValue) &&
                    (!string.IsNullOrWhiteSpace(request.ISOCurrencyCode) && !string.IsNullOrWhiteSpace(r.PresentationCurrencyCode) && r.PresentationCurrencyCode == request.ISOCurrencyCode),
                r =>
                    (r.SiteCode == request.SiteCode) &&
                    (r.UrlCode == request.UrlCode) &&
                    (request.CountryCode.HasValue && r.CountryCode.HasValue && r.CountryCode.Value == request.CountryCode.Value),
                r =>
                    (r.SiteCode == request.SiteCode) &&
                    (!r.UrlCode.HasValue) &&
                    (request.CountryCode.HasValue && r.CountryCode.HasValue && r.CountryCode.Value == request.CountryCode.Value) &&
                    (!string.IsNullOrWhiteSpace(request.ISOCurrencyCode) && !string.IsNullOrWhiteSpace(r.PresentationCurrencyCode) && r.PresentationCurrencyCode == request.ISOCurrencyCode),
                r =>
                    (r.SiteCode == request.SiteCode) &&
                    (r.UrlCode == request.UrlCode) &&
                    (!r.CountryCode.HasValue),
                r =>
                    (r.SiteCode == request.SiteCode) &&
                    (!r.UrlCode.HasValue) &&
                    (!r.CountryCode.HasValue),
            };

            foreach (var predicate in predicates)
            {
                result = matrix.Where(predicate).ToList();
                if (result.Count > 0)
                    break;
            }


            return result;
            
        }

    }
}
