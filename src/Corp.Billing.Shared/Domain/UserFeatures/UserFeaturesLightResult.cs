﻿using System;

namespace Corp.Billing.Shared.Domain.UserFeatures
{
    [Serializable]
    public class UserFeaturesLightResult
    {
        public Int64 FeaturesMask { get; set; }
    }
}
