﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.UserFeatures
{
    public interface IUserFeaturesLightGetter : IResetableCache<int>
    {
        UserFeaturesLightResult GetUserFeaturesLight(UserFeaturesLightRequest request);
    }
}
