﻿using Corp.Billing.Shared.Domain.SubscriptionBenefits.Services;

namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Exceptions
{
    public class UserDoesNotExistsException : BaseSubscriptionBenefitsException
    {
        public UserDoesNotExistsException(BenefitsStatusRequest req) 
            : base(req)
        { }

        public override int ErrorCode => (int)BenefitFeatureExceptionErrorCode.UserDoesNotExists;

        public override string ErrorMessage => $"User {Request?.UserId} does not exist";
    }
}
