﻿namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Data
{
    public enum BenefitProvisionStateEnum : byte
    {
        None = 0,
        Used = 1,
        Active = 2,
        PendingActivation = 3,
    }
}
