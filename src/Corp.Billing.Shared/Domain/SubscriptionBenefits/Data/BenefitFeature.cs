﻿using System;

namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Data
{
    [Serializable]
    public class BenefitFeature
    {
        public int FeatureType { get; set; }
        public string FeatureTypeName => ((BenefitFeatureTypeEnum)this.FeatureType).ToString();

        public byte PurchaseType { get; set; }
        public string PurchaseTypeName => ((BenefitPurchaseTypeEnum)this.PurchaseType).ToString();

        public byte ProvisionState { get; set; }
        public string ProvisionStateName => ((BenefitProvisionStateEnum)this.ProvisionState).ToString();

        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }

        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpirationDate { get; set; }

        /// <summary>
        /// For backward compatibility: this is same as "IsNewGrant".
        /// </summary>
        [Obsolete]
        public bool IsNewBenefit
        {
            get { return IsNewGrant;  }
            set { IsNewGrant = value; }
        }

        /// <summary>
        /// Whether or not this is a "new" grant.
        /// </summary>
        public bool IsNewGrant { get; set; }

        /// <summary>
        /// Whether or not this is a "new" launched feature.
        /// </summary>
        public bool IsNewFeature { get; set; }
        public DateTime FeatureLaunchDate { get; set; }

    }
}
