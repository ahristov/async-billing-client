﻿using Corp.Billing.Shared.Domain.Subscription;
using Corp.Billing.Shared.Domain.SubscriptionBenefits.Data;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Services
{
    [Serializable]
    public class BenefitStatusResponse : BenefitsCountNewResponse, ICurrentPackageResult
    {
        public bool IsCurrentSubscriber { get; set; }
        public bool IsFreeTrial { get; set; }
        public byte? CurrentPackageMonths { get; set; }
        public bool IsCurrentPackageBundle { get; set; }
        public bool IsGuaranteeSubscriber { get; set; }
        public DateTime? CurrentPackageBeginDate { get; set; }
        public byte SubscriptionPackageType { get; set; }
        public string SubscriptionPackageTypeName => ((SubscriptionPackageTypeEnum)this.SubscriptionPackageType).ToString();
        public bool IsCurrentPackageAppleIAP { get; set; }
        public bool IsValueAddSubscription { get; set; }
        public bool IsEligibleForDiscountedEvents { get; set;  }
        public bool IsEligibleForHappyHour { get; set; }

        public ICollection<BenefitFeature> Features { get; set; }

        public BenefitStatusResponse()
        {
            Features = new List<BenefitFeature>();
        }
    }
}
