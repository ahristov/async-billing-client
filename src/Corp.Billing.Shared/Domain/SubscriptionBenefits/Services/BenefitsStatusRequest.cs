﻿using Corp.Billing.Shared.Domain.SubscriptionBenefits.Data;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Services
{
    [Serializable]
    public class BenefitsStatusRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
        public DateTime? LastBenefitsViewTime { get; set; }
    }
}
