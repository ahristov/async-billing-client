﻿using System;

namespace Corp.Billing.Shared.Domain.SubscriptionBenefits.Services
{
    [Serializable]
    public class BenefitsCountNewResponse
    {
        public int CountOfNewBenefits { get; set; }

        public static BenefitsCountNewResponse CreateFrom(BenefitsCountNewResponse other)
        {
            return new BenefitsCountNewResponse
            {
                CountOfNewBenefits = other.CountOfNewBenefits,
            };
        }
    }
}
