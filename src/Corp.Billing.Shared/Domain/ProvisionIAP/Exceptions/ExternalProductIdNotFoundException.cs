﻿using Corp.Billing.Shared.Domain.ProvisionIAP.Services;

namespace Corp.Billing.Shared.Domain.ProvisionIAP.Exceptions
{
    public class ExternalProductIdNotFoundException : BaseProvisionIAPException
    {
        private readonly string _externalProductId;

        public ExternalProductIdNotFoundException() : base() { }

        public ExternalProductIdNotFoundException(ProvisionIAPRequest req, string externalProductId)
            : base(req)
        {
            _externalProductId = externalProductId;
        }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.ExternalProductIdNotFound; } }
        public override string ErrorMessage { get { return "External Product Id Not Found"; } }
        public override string ExternalProductId { get { return _externalProductId; } }
    }
}
