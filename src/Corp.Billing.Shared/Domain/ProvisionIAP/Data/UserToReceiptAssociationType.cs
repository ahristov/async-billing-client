﻿namespace Corp.Billing.Shared.Domain.ProvisionIAP.Data
{
    public enum UserToReceiptAssociationType
    {
        NotAssociatedWithAnyUser = 0,
        AssociatedWithCorrectUser = 10,
        AssociatedWithAnotherUser = 20,
        NoInAppPurchases = 30
    }
}
