﻿namespace Corp.Billing.Shared.Domain.Pricing
{
    public interface IUserPricingScoreGetter
    {
        GetUserScoreDataResult GetUserPricingScore(GetUserScoreDataRequest request);
    }

    public interface IUserPricingScoreGetterNoHttpCache : IUserPricingScoreGetter { }
}
