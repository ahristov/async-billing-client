﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface IUserStatusDateGetter : IResetableCache<int>
    {
        UserStatusDateResult bilGetUserStatusDtv0(UserStatusDateRequest request);
    }
}
