﻿namespace Corp.Billing.Shared.Domain.Subscription
{
	public enum SubscriptionsSummaryStateEnum: byte
    {
		NoPrevious = 0,
		FirstTime = 1,
		Renewed = 2,
		Expired = 3
	}
}
