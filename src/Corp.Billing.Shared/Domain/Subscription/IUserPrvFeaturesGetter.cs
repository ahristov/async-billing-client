﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface IUserPrvFeaturesGetter : IResetableCache<int>
    {
        UserFeatureResult prvFeaturesv1(UserFeatureRequest request);
    }
}
