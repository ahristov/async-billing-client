﻿using System.Reflection;

namespace Corp.Billing.Shared.Domain.Subscription.Reactivate.Data
{
    public static class CancelSourceTypeExtensions
    {
        public static string GetComment(this CancelSourceType sourceType)
        {
            FieldInfo fi = sourceType.GetType().GetField(sourceType.ToString());

            ReactivationCommentAttribute[] attributes =
                (ReactivationCommentAttribute[])fi.GetCustomAttributes(typeof(ReactivationCommentAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Comment;
            else
                return ReactivationCommentAttribute.COMMENT_GENERIC;
        }
    }
}
