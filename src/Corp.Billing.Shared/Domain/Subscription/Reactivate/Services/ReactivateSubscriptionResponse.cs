﻿namespace Corp.Billing.Shared.Domain.Subscription.Reactivate.Services
{
    public class ReactivateSubscriptionResponse
    {
        public bool Success { get; set; }
    }
}
