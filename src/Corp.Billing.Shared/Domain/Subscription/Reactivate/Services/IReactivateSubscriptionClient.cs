﻿namespace Corp.Billing.Shared.Domain.Subscription.Reactivate.Services
{
    public interface IReactivateSubscriptionClient
    {
        ReactivateSubscriptionResponse ReactivateSubscription(ReactivateSubscriptionRequest req);
    }
}
