﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface IUserBillingData
    {
        GetUserBillingDataResult GetUserBillingData(GetUserBillingDataRequest request);
		GetCurrentPackageResult GetCurrentPackage(GetCurrentPackageRequest request);
	}
}
