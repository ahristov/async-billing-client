﻿namespace Corp.Billing.Shared.Domain.Subscription.Extentions
{
    public static class UserBillingDataExtentions
    {
        public static SubscriptionPackageTypeEnum GetCurrentSubscriberPackageType(
            this GetUserBillingDataResult billingData,
            bool isFreeTrial)
        {
            if (billingData == null)
                return SubscriptionPackageTypeEnum.NonSubscriber;

            bool isCurrentSubscriber = billingData.SubscriberState == Shared.Domain.Subscription.GetUserBillingDataResult.SubscriberStateType.CurrentSubscriber;
            bool isCurrentPackageBundle = billingData.IsCurrentPackageBundle.GetValueOrDefault();
            byte packageMonths = billingData.CurrentPackageMonths.GetValueOrDefault();
            bool isCurrentPackageValueAdd = billingData.IsCurrentPackageValueAdd;
            int? prodBundleId = billingData.ProdBundleId;

            if (isFreeTrial == true 
                && packageMonths <= 0
                && prodBundleId.HasValue == false)
            {
                packageMonths = billingData.FTPkgMonths.GetValueOrDefault();
                prodBundleId = billingData.FTPkgBundleID;
            }

            var res = SubscriptionPackageTypeHelpers.GetCurrentSubscriptionPackageType(
                isCurrentSubscriber || isFreeTrial, 
                isCurrentPackageBundle, 
                packageMonths, 
                isCurrentPackageValueAdd, 
                prodBundleId);

            return res;
        }

        public static bool IsCurrentElitePackageSubscriber(this GetUserBillingDataResult billingData)
        {
            var currentPackageType = GetCurrentSubscriberPackageType(billingData, false);
            return currentPackageType == SubscriptionPackageTypeEnum.ElitePackage;
        }

    }
}
