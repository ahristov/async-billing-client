﻿using UserSubStatusType = Corp.Billing.Shared.Domain.Subscription.UserSubStatusResult.UserSubStatusType;

namespace Corp.Billing.Shared.Domain.Subscription.Extentions
{
    public static class UserSubStatusExtentions
    {

        public static bool IsNeverSubscribed(this UserSubStatusType subUserState)
        {
            return subUserState == UserSubStatusType.NeverSubscribed;
        }

        public static bool IsFreeTrialUser(this UserSubStatusType subUserState)
        {
            return subUserState == UserSubStatusType.FreeTrial
                || subUserState == UserSubStatusType.ResignedFreeTrial;
        }

        public static bool IsExpiredFreeTrialUser(this UserSubStatusType subUserState)
        {
            return subUserState == UserSubStatusType.ExpiredFreeTrial
                || subUserState == UserSubStatusType.ExpiredFreeTrialOther;
        }

        public static bool IsPendingOfflineSubscrber(this UserSubStatusType subUserState)
        {
            return subUserState == UserSubStatusType.PendingSubscription;
        }

        public static bool IsCharterMember(this UserSubStatusType subUserState)
        {
            return subUserState == UserSubStatusType.CharterMember;
        }
        public static bool IsIAPSubscriber(this UserSubStatusType subUserState)
        {
            return subUserState == UserSubStatusType.CurrentAipSub;
        }

        public static bool IsPastSubscriber(this UserSubStatusType subUserState)
        {
            return subUserState == UserSubStatusType.ExpiredSubscription
                    || subUserState == UserSubStatusType.ResignedPastSub
                    || subUserState == UserSubStatusType.ExpiredCompAccount
                    || subUserState == UserSubStatusType.ExpiredFreeTrial;
        }

        public static bool IsFailedRenewal(this UserSubStatusType subUserState)
        {
            return subUserState == UserSubStatusType.PastSubscriber1
                || subUserState == UserSubStatusType.PastSubscriber2;
        }

        public static bool IsResignedCurentSub(this UserSubStatusType subUserState)
        {
            return subUserState == UserSubStatusType.ResignedCurrentSub;
        }

        public static bool IsCompUser(this UserSubStatusType subUserState)
        {
            return subUserState == UserSubStatusType.CompAccount;
        }

        public static bool IsInECheckReviewPeriod(this UserSubStatusType subUserState)
        {
            return subUserState == UserSubStatusType.ECheckReviewPeriod;
        }
    }
}
