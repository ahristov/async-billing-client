﻿using System;

namespace Corp.Billing.Shared.Domain.Subscription
{
    [Serializable]
    public class ValueAddedFeaturesTable
    {
        public byte ProdFeatureId { get; set; }
        public bool ProvisionWithBase { get; set; }
        public bool RecurringFeature { get; set; }
        public byte? FrequencyQuantity { get; set; }
        public short? FrequencyDays { get; set; }
    }
}
