﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface IUserSubscriptionStatusGetter : IResetableCache<int>
    {
        UserSubStatusResult SubUserStatusv3(UserSubStatusRequest request);
    }

    public interface IUserSubscriptionStatusGetterNoHttpCache : IUserSubscriptionStatusGetter {}
}
