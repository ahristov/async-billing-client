﻿using System;

namespace Corp.Billing.Shared.Domain.Subscription
{
    public interface ICurrentPackageResult
    {
        bool IsCurrentSubscriber { get; }
        bool IsFreeTrial { get; }
        byte? CurrentPackageMonths { get; }
        bool IsCurrentPackageBundle { get; }
        bool IsGuaranteeSubscriber { get; }
        bool IsCurrentPackageAppleIAP { get; }
        bool IsEligibleForDiscountedEvents { get; set; }
        bool IsEligibleForHappyHour { get; set; }
        DateTime? CurrentPackageBeginDate { get; set; }
    }
}
