﻿namespace Corp.Billing.Shared.Domain.RateCardCommon.RateCardCommon
{
    public interface IBaseProduct
    {
        byte Months { get; }
        bool IsBundledBit { get; }
    }
}
