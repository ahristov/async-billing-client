﻿using System;

namespace Corp.Billing.Shared.Domain
{
    public class IVRRefundRequest
    {
        public string AcctNum { get; set; }
        public string PostalCode { get; set; }
        public int TrxID { get; set; }
        public bool RefundLastTrx { get; set; }
        public int? UserID { get; set; }
    }

    public class IVRRefundResult
    {
        public int ReturnValue { get; set; }
        public int UserID { get; set; }
        public int TrxID { get; set; }
        public DateTime TrxStatusDt { get; set; }
        public decimal Amount { get; set; }
    }
        
}
