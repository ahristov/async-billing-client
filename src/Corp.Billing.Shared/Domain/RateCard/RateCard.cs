﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using Corp.Billing.Shared.Domain.RateCardCommon.RateCardCommon;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.RateCard
{
    public class RateCardBaseRequest : PCRulesRequest, IApiRequestWithQueryString
    {
        public short? PageTemplateID {get; set;}
        public bool SkipRuleChecks { get; set; }
        public bool ReturnUpsell { get; set; }
        public bool Return12Month { get; set; }
        public string DefaultPromoID { get; set; }
        private byte? _monthstoCompare;
        public byte MonthstoCompare
        {
            get
            {
                if (_monthstoCompare.HasValue && _monthstoCompare.Value > 0)
                    return _monthstoCompare.GetValueOrDefault();
                return 1;

            }
            set { _monthstoCompare = value; }
        }

        public RateCardBaseRequest()
        {
            PromoId = Corp.Billing.Shared.Domain.Consts.DefaultPromoId;
            DefaultPromoID = Corp.Billing.Shared.Domain.Consts.DefaultPromoId;
        }
    }

    public class RateCardBaseResult
    {
        public int ReturnValue { get; set; }
        public short? FreeTrialProdDtlId { get; set; }
        public short? FreeTrialDays { get; set; }

        public IList<RateCardBaseItem> Items { get; set; }

        public RateCardBaseResult()
        {
            Items = new List<RateCardBaseItem>();
        }

        public static RateCardBaseResult RateCardBaseNotEligible()
        {
            return new RateCardBaseResult
            {
                ReturnValue = 50000
            };
        }
    }

    public class RateCardBaseItem : IBaseProduct
    {
        public short ProdID { get; set; }
        public short ProdDtlID { get; set; }
        public decimal InitialAmt { get; set; }
        public short DiscountAmt { get; set; }
        public decimal DiscountPrct { get; set; }
        public short InitialDays { get; set; }
        public short FreeDays { get; set; }
        public short RenewalDays { get; set; }
        public decimal RenewalAmt { get; set; }
        public byte Months { get; set; }
        public decimal InitialTax { get; set; }
        public decimal RenewalTax { get; set; }
        public long ProdAttributeMask { get; set; } //?
        public bool IsBundledBit { get; set; }
        public string ISOCurrencyCode { get; set; }
        public bool ShowAddOnPage { get; set; } //?
        public long BundleMask { get; set; } //?
        public decimal PercentSavings { get; set; }
        public decimal PreDiscountAmt { get; set; }
        public decimal OneMonthPriceCompNoDiscount { get; set; }
        public string ProcessorData { get; set; } // V2: This is used for IAP
    }

    public class RateCardAddOnRequest : PCRulesRequest, IApiRequestWithQueryString
    {

        public string ProdDtlIDList { get; set; }
        public short URLCode { get; set; }
        public byte StateCode { get; set; }
        public short CountryCode { get; set; }
        public byte ExcludeProdFeature { get; set; }
        /// <summary>
        /// 1 = Return Individual add-ons only,
        /// 2 = Return add-on bundles only, 
        /// 3 = Return both individual add-ons and add-on upsell bundles
        /// </summary>
        public byte SkipAddonBundle {get; set;}
        /// <summary>
        /// Set to `true` only for debugging purposes.
        /// </summary>
        public bool SkipSubRules { get; set; }
        public string DefaultPromoID { get; set; }
    }

    public class RateCardAddOnResult
    {
        public int ReturnValue { get; set; }
        public short AccountOrderModID { get; set; }
        public DateTime OldUserStatusDate { get; set; }
        public short GeoStateTaxID {get; set;}
        public List<RateCardAddOnItem> RateCardItems { get; set; } 
  
    }

    public class RateCardAddOnItem
    {
        public int ReturnValue { get; set; }
        public short ProdId { get; set; }
        public short ProdDtlId { get; set; }
        public DateTime BeginDate { get; set; }
        public short InitialDays { get; set; }
        public decimal SalesPriceWithoutDiscount { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal RenewalPrice { get; set; }
        public short RenewalDays { get; set; }
        public bool DelayCapture { get; set; }
        public bool IsFreeTrial { get; set; }
        public byte Months { get; set; }
        public decimal InitialTax { get; set; }
        public decimal RenewalTax { get; set; }
        public string Description { get; set; }
        public string OrderString { get; set; }
        public byte? ProdFeatureID { get; set; }
        public string ISOCurrencyCode { get; set; }
        public bool BundlePreOrdered { get; set; }
        public string Text1 { get; set; }
    }


    public class UnitRateCardRequest : PCRulesRequest, IApiRequestWithQueryString
    {
        public int? ProdDtlId { get; set; }
        public int? EventId { get; set; }
    }

    public class UnitRateCardResult
    {
        public short AcctOrderModId { get; set; }
        public DateTime? OldUserStatusDt { get; set; }
        public int ReturnValue { get; set; }

        public IList<UnitRateCardRowItem> Items { get; set; }

        public UnitRateCardResult()
        {
            Items = new List<UnitRateCardRowItem>();
        }
    }

    public class UnitRateCardRowItem
    {
        public int ProdDtlId { get; set; }
        public byte ProdFeatureId { get; set; }
        public short Units { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal TaxAmt { get; set; }
        public string IsoCurrencyCode { get; set; }
        public string OrderString { get; set; }
        public short ImpulseMinutes { get; set; }				
        public string Data { get; set; }
    }



}
