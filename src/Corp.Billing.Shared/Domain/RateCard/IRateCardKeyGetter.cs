﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.RateCard.Services
{
    public interface IRateCardKeyGetter : IResetableCache<int>
    {
        int? GetRateCardKey(PCRulesRequest apiRequest);
    }
}
