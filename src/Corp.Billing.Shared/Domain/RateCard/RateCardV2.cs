﻿using Corp.Billing.Shared.Domain.Installlments;
using Corp.Billing.Shared.Domain.ProductCatalog.PriceOverride;
using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using Corp.Billing.Shared.Domain.RateCardCommon.RateCardCommon;
using Corp.Billing.Shared.Subscription.Product;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.RateCard
{
    // Base Classes, Enumerations ////////////////////////////////////////////

    public static class RateCardV2Extensions
    {
        public static byte NormalizeMonthValue(this byte? months)
        {
            if (months.HasValue && months.Value > 0)
                return months.GetValueOrDefault();
            return 1;
        }
    }

    public enum BundleType : byte
    {
        None = 0,
        Premium = 1,
        Upsell = 2,
        AddOn = 3,
        Elite = 4,
    }


    public abstract class RateCardV2RequesBase : PCRulesRequest
    {
        /// <summary>
        /// If set: will use this to get the rate card data.
        /// If not set: will run the rules engine to get rate card key on it's own
        /// </summary>
        public int? RateCardKey { get; set; }

        /// <summary>
        /// If set: will use this to get the overrides data, but only and only if `UseRateCardOverrideKeyFromRequest` is set to `trie`.
        /// If not set: will run the rules engine ot get the ooerrides key on it's own.
        /// </summary>
        public int? RateCardOverrideKey { get; set; }

        /// <summary>
        /// Only when this is `true`, the value (if passed) of `RateCardOverrideKey` will be respected.
        /// </summary>
        public bool UseRateCardOverrideKeyFromRequest { get; set; }

        /// <summary>
        /// If true optimize the cache, do not invalidate down the call.
        /// </summary>
        public bool CombinedRateCardFlow { get; set; }

    }


    public abstract class RateCardV2ResultBase
    {
        public Guid RateCardRefId { get; set; }
        public int? RateCardKey { get; set; }

        public int? RateCardOverrideKey { get; set; }
        public PriceOverrideUpsellTypeEnum RateCardOverrideUpsellType { get; set; }

        public string PricingRuleId { get; set; }
        public decimal TaxRate { get; set; }

        public int ReturnValue { get; set; }

        public void SetRateCardOverrideInfo(PriceOverrideRulesResultV2 priceOverrideResult)
        {
            RateCardOverrideKey = (priceOverrideResult == null) ? null : priceOverrideResult.RateCardOverrideKey;
            RateCardOverrideUpsellType = (priceOverrideResult == null) ? PriceOverrideUpsellTypeEnum.None :  priceOverrideResult.UpsellType;
        }

        public RateCardV2ResultBase()
        {
            RateCardRefId = Guid.NewGuid();
        }
    }


    [Serializable]
    public abstract class RateCardV2ItemBase
    {
        // Product identifiers
        public int ProdId { get; set; }
        public int ProdDetailId { get; set; }

        // Length of product
        public byte Months { get; set; }
        public short FreeDays { get; set; }

        public short InitialDays { get; set; }
        public short RenewalDays { get; set; }

        

        // Price information
        public decimal InitialAmt { get; set; }
        public decimal PreDiscountInitialAmt { get; set; }

        public decimal RenewalAmt { get; set; }
        public decimal PreDiscountRenewalAmt { get; set; }

        public int? RateCardOverrideKey { get; set; }
        public bool InheritRenewalPrice { get; set; }

        public decimal SalesPriceWithoutDiscount { get; set; }
        public decimal SalesPriceWithDiscount => SalesPriceWithoutDiscount * (1 - DiscountPrct) - DiscountAmt;

        public decimal? MonthlySalesPriceWithoutDiscount
        {
            get
            {
                try
                {
                    return Months > 0
                        ? Math.Round(SalesPriceWithoutDiscount / Months, 2, MidpointRounding.AwayFromZero)
                        : default(decimal?);
                }
                catch
                {
                    return default(decimal?);
                }
            }
        }

        public decimal? MonthlySalesPriceWithDiscount
        {
            get
            {
                try
                {
                    return Months > 0
                        ? decimal.Truncate(100 * SalesPriceWithDiscount / Months) / 100
                        : default(decimal?);
                }
                catch
                {
                    return default(decimal?);
                }
            }
        }

        // Discount information
        public decimal DiscountAmt { get; set; }
        public decimal DiscountPrct { get; set; }


        // Tax information
        public decimal TaxRate { get; set; }
        public decimal InitialTax { get; set; }
        public decimal RenewalTax { get; set; }


        // Currency
        public string ISOCurrencyCode { get; set; }


        // Bundle data
        public BundleType BundleType { get; set; }
        public int BundleId { get; set; }

        private long _bundleMask;

        public long BundleMask
        {
            get { return _bundleMask < 1 ? 1 : _bundleMask; }
            set { _bundleMask = value; }
        }

        public bool IsGuarantee { get; set; }


        // External payments
        public string ExternalProductId { get; set; }
        public string PriceTier { get; set; }

        public IDictionary<short, IList<InstallmentDetailTable>> Installments { get; set; }

        public RateCardV2ItemBase()
        {
            Installments = new Dictionary<short, IList<InstallmentDetailTable>>();
        }
    }


    // Base RateCard //////////////////////////////////////////////////////////

    [Serializable]
    public class RateCardV2BaseRequest : RateCardV2RequesBase
    {
        public bool SkipRuleChecks { get; set; }
        public bool ReturnUpsell { get; set; }
        public bool Return12Month { get; set; }


        private byte? _monthsToCompare;
        public byte MonthsToCompare
        {
            get { return _monthsToCompare.NormalizeMonthValue(); }
            set { _monthsToCompare = value; }
        }

        public RateCardV2BaseRequest()
        {
            Return12Month = true;
            ReturnUpsell = false;
            MonthsToCompare = 1;
        }

    }

    [Serializable]
    public class RateCardV2BaseResult : RateCardV2ResultBase
    {
        /// <summary>
        /// Is the applied promo a marked up promo.
        /// </summary>
        public bool HasMarkedUpPrices { get; set; }
        public short? FreeTrialProdDtlId { get; set; }
        public short? FreeTrialDays { get; set; }

        public IList<RateCardV2BaseItem> Items { get; set; }

        public List<PCRuleConditionTrace> RuleConditionTraces { get; private set; }

        public RateCardV2BaseResult()
        {
            Items = new List<RateCardV2BaseItem>();
            RuleConditionTraces = new List<PCRuleConditionTrace>();
        }

        public static RateCardBaseResult RateCardBaseNotEligible()
        {
            return new RateCardBaseResult
            {
                ReturnValue = 50000
            };
        }
    }


    [Serializable]
    public class RateCardV2BaseItem : RateCardV2ItemBase, IBaseProduct, ICloneable
    {
        public decimal NMonthPriceCompNoDiscount { get; set; }
        public decimal PercentSavings { get; set; }

        // Bundle information
        public bool IsBundledBit { get { return BundleType != BundleType.None; } }

        public bool ShowAddOnPage { get { return !IsBundledBit; } }

        public RateCardV2BaseItem Clone()
        {
            return this.MemberwiseClone() as RateCardV2BaseItem;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }


    // PowerUps RateCard //////////////////////////////////////////////////////////

    [Serializable]
    public class RateCardV2PowerUpsRequest : RateCardV2RequesBase
    {
    }

    [Serializable]
    public class RateCardV2PowerUpsResult : RateCardV2AddOnsResult
    {
        public RateCardV2PowerUpsResult()
        {
        }

        public RateCardV2PowerUpsResult(RateCardV2AddOnsResult addOnsResult, int rateCardKey, string pricingRuleId)
            : this()
        {

            Items = new List<RateCardV2AddOnsItem>(addOnsResult.Items);

            FreeTrialDays = addOnsResult.FreeTrialDays;
            FreeTrialProdDtlId = addOnsResult.FreeTrialProdDtlId;
            TaxRate = addOnsResult.TaxRate;
            ReturnValue = addOnsResult.ReturnValue;
            RateCardOverrideKey = addOnsResult.RateCardOverrideKey;
        }
    }


    // AddOns RateCard //////////////////////////////////////////////////////////




    [Serializable]
    public class RateCardV2AddOnsRequest : RateCardV2RequesBase
    {
        public int PrimaryProdId { get; set; }
        public BundleType BundleType { get; set; }

        /// <summary>
        /// Only used for powerups rate cards.
        /// </summary>
        public byte? Months { get; set; }

        public long? BundleMask { get; set; }

        public bool IncludeServiceFeeFeature { get; set; }

        public short? RenewalDays { get; set; }
    }

    [Serializable]
    public class RateCardV2AddOnsResult : RateCardV2ResultBase
    {
        public short? FreeTrialProdDtlId { get; set; }
        public short? FreeTrialDays { get; set; }


        public IList<RateCardV2AddOnsItem> Items { get; set; }

        public List<PCRuleConditionTrace> RuleConditionTraces { get; private set; }

        public RateCardV2AddOnsResult()
        {
            Items = new List<RateCardV2AddOnsItem>();
            RuleConditionTraces = new List<PCRuleConditionTrace>();
        }
    }

    [Serializable]
    public class RateCardV2AddOnsItem : RateCardV2ItemBase
    {
        public DateTime BeginDt { get; set; }
        public bool BundlePreOrdered { get; set; }

        public byte LuProdFeatureId { get; set; }

        public bool DelayCapture { get; set; }
        public bool IsFreeTrial { get; set; }

        public RateCardV2AddOnsItem()
        {
            BeginDt = DateTime.Now.Date;
        }


        public bool IsBaseFeature
        {
            get
            {
                return LuProdFeatureId == (byte)ProductDefinitions.ProductFeatureIds.EmailCommunication;
            }
        }

        public bool IsServiceFee
        {
            get
            {
                return LuProdFeatureId == (byte)ProductDefinitions.ProductFeatureIds.ServiceFeeFeatureID;
            }
        }

        public bool IsMatchPhone
        {
            get
            {
                return LuProdFeatureId == (byte)ProductDefinitions.ProductFeatureIds.MatchPhone;
            }
        }

    }



    // Unit Products RateCard //////////////////////////////////////////////////////////

    [Serializable]
    public class RateCardV2UnitProductsRequest : RateCardV2RequesBase
    {
        public int? OtherUserId { get; set; }
        public bool IncludeRushHourBoost { get; set; }
    }

    [Serializable]
    public class RateCardV2UnitProductsResult : RateCardV2ResultBase
    {
        public IList<RateCardV2UnitProductsItem> Items { get; set; }

        public List<PCRuleConditionTrace> RuleConditionTraces { get; private set; }

        public RateCardV2UnitProductsResult()
        {
            Items = new List<RateCardV2UnitProductsItem>();
            RuleConditionTraces = new List<PCRuleConditionTrace>();
        }

        public byte? OtherUserGenderGenderSeek { get; set; }
    }

    [Serializable]
    public class RateCardV2UnitProductsItem
    {
        public int ProdDetailId { get; set; }
        public byte ProdFeatureId { get; set; }
        public bool IsRushHour { get; set; }
        public bool IsBoostWithRff { get; set; }
        public short Units { get; set; }

        public decimal SalesPrice { get; set; }
        public decimal TaxAmt { get; set; }

        public string IsoCurrencyCode { get; set; }
        public short ImpulseMinutes { get; set; }


        // External payments
        public string ExternalProductId { get; set; }
        public string PriceTier { get; set; }


        // Discount information
        public decimal DiscountAmt { get; set; }
        public decimal DiscountPrct { get; set; }

        public decimal PreDiscountSalesPrice { get; set; }

        // Renewal information
        public short RenewalDays { get; set; }
        public bool IsRenewing => RenewalDays > 0;

        public decimal RenewalAmt { get; set; }
        public decimal PreDiscountRenewalAmt { get; set; }
        public decimal RenewalTaxAmt { get; set; }

        public byte? PrctSavingsToSingleUnit { get; set; }
    }



    // AddOn Bundles ///////////////////////////////////////////////////////////////

    [Serializable]
    public class RateCardV2AddOnsBundleRequest : RateCardV2RequesBase
    {
        //public byte Months { get; set; }
        
    }




    // Feature compare prices //////////////////////////////////////////////////////

    [Serializable]
    public class RateCardV2FeatureCompareRequest : RateCardV2RequesBase
    {
        private byte? _monthsSelected;
        public byte MonthsSelected
        {
            get { return _monthsSelected.NormalizeMonthValue(); }
            set { _monthsSelected = value; }
        }


        private byte? _monthsToCompare;
        public byte MonthsToCompare
        {
            get { return _monthsToCompare.NormalizeMonthValue(); }
            set { _monthsToCompare = value; }
        }
    }


    [Serializable]
    public class RateCardV2FeatureCompareResult : RateCardV2ResultBase
    {
        public IList<RateCardV2FeatureCompareItem> Items { get; set; }
        public List<PCRuleConditionTrace> RuleConditionTraces { get; private set; }

        public RateCardV2FeatureCompareResult()
        {
            Items = new List<RateCardV2FeatureCompareItem>();
            RuleConditionTraces = new List<PCRuleConditionTrace>();
        }
    }

    [Serializable]
    public class RateCardV2FeatureCompareItem
    {
        public byte luProdFeatureId { get; set; }


        public decimal PreDiscountInitialAmt { get; set; }
        public decimal InitialAmt { get; set; }
        public decimal DiscountAmt { get; set; }
        public decimal InitialTax { get; set; }


        public decimal TotalPreDiscountInitialAmt { get; set; }
        public decimal TotalInitialAmt { get; set; }
        public decimal TotalDiscountAmt { get; set; }
        public decimal TotalInitialTax { get; set; }

    }


}
