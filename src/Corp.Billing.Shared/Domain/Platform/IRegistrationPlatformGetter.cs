﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Platform
{
    public interface IRegistrationPlatformGetter : IResetableCache<int>
    {
        RegistrationPlatformResult GetRegistrationPlatform(int userId);
    }

    public interface IRegistrationPlatformGetterNoHttpCache : IRegistrationPlatformGetter { }
}
