﻿using System;

namespace Corp.Billing.Shared.Domain.Installlments
{
    [Serializable]
    public class InstallmentData
    {
        public short ProvisionDays { get; set; }
        public decimal InitialInstallmentAmount { get; set; }
        public decimal InitialInstallmentAmountWithTax { get; set; }
        public decimal InitialInstallmentAmountPriorDiscount { get; set; }
        public decimal InitialInstallmentAmountWithTaxPriorDiscount { get; set; }
        public decimal RenewalInstallmentAmount { get; set; }
        public decimal RenewalInstallmentAmountWithTax { get; set; }
        public DateTime InstallmentBillDt { get; set; }
        public DateTime BeginDt { get; set; }
        public DateTime EndDt { get; set; }
    }
}
