﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Corp.Billing.Shared.Domain.Installlments
{
    public static class InstallmentTypeExtensions
    {
        public static IEnumerable<InstallmentTypeEnum> GetAllPaymentMethodTypes()
            => Enum.GetValues(typeof(InstallmentTypeEnum)).Cast<InstallmentTypeEnum>();

        public static InstallmentTypeDataAttribute GetInstallmentTypeData(
            this InstallmentTypeEnum installmentType)
        {
            FieldInfo fi = installmentType.GetType().GetField(installmentType.ToString());
            if (fi == null)
                return null;

            InstallmentTypeDataAttribute[] attributes =
                (InstallmentTypeDataAttribute[])fi.GetCustomAttributes(typeof(InstallmentTypeDataAttribute), false);
            return (attributes != null && attributes.Length > 0)
                ? attributes[0]
                : null;
        }
    }
}
