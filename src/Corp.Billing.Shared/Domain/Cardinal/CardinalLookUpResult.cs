﻿using System;

namespace Corp.Billing.Shared.Domain.Cardinal
{
    [Serializable]
    public class CardinalLookUpResult
    {
        public string Cavv { get; set; }
        public string EciFlag { get; set; }
        public string Enrolled { get; set; }
        public string XId { get; set; }
        public int? CardinalLogId { get; set; }
    }
}
