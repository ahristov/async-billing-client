﻿namespace Corp.Billing.Shared.Domain.Cardinal
{
    public class CardinalUserData
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
    }
}
