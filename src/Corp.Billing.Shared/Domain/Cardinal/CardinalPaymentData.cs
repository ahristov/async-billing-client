﻿namespace Corp.Billing.Shared.Domain.Cardinal
{
    public class CardinalPaymentData
    {
        public int? PaymentMethodToken { get; set; }
        public string PlainCreditCardNumber { get; set; }
        public short CardExpMonth { get; set; }
        public short CardExpYear { get; set; }

        public int Amount { get; set; }
        public int CurrencyCode { get; set; }

        public string Recurring { get; set; }
        public int RecurringFrequency { get; set; }
        public string RecurringEnd { get; set; }
    }
}
