﻿using System;

namespace Corp.Billing.Shared.Domain.Orders
{
    public class SubscribeRequestBase
    {
        private DateTime _oldUserStatusDt;
        static DateTime _minserStatusDt = new DateTime(1753, 1, 1);

        /* These parameters come FROM subRateCardAddOn OUTPUT parameters */
        public short AcctOrderModID { get; set; }

        public DateTime OldUserStatusDt
        {
            get { return (_oldUserStatusDt < _minserStatusDt) ? _minserStatusDt : _oldUserStatusDt; }
            set { _oldUserStatusDt = value; }

        }
        public short GeoStateTaxID { get; set; }


        /* IF CheckCC = 1 we'll check FOR blocked, expired, usage within 14 days */
        public bool CheckCC { get; set; }

        /* required data */
        public int UserID { get; set; }
        public string PromoID { get; set; }
        public byte? TrxSrcID { get; set; }
        public string OrderList { get; set; }
        public int STID { get; set; }
        public int SubBrandID { get; set; }

        /* data optional to everyone */
        public int? AcctOrderID { get; set; }
        public int? TrxID { get; set; }

        /* CSA data */
        public bool IsComplimentary { get; set; }
        public int EmployeeID { get; set; }

        /* CSA AND back-END processes */
        public string Comment { get; set; }
        public short? CRCode { get; set; }


        /* refunds */
        public int? AcctPymtMethID { get; set; }
        public bool IsRefund { get; set; }

        /* site-only data */
        public Guid? SID { get; set; }
        public byte? ClientIP1 { get; set; }
        public byte? ClientIP2 { get; set; }
        public byte? ClientIP3 { get; set; }
        public byte? ClientIP4 { get; set; }
        public short? SubscribeTextID { get; set; }



        /* payment method data */
        public string DtlCode { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string FullName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityName { get; set; }
        public byte? PState { get; set; } //From the Order Form
        public short? PCountry { get; set; } //From the Order Form
        public string PostalCode { get; set; }

        public byte? CCMonth { get; set; }
        public short? CCYear { get; set; }
        public string CheckNum { get; set; }

        /*sub order rules override*/
        public bool OverrideSubOrderRules { get; set; }


        /* debug */
        public DateTime? Now { get; set; }
        public byte? ForceCompAddOn { get; set; }


        /* Optional input parameter for Event purchases*/
        public int? EventID { get; set; }
        public string PhoneNumberClear { get; set; }
        public bool? ExpiredCCDtCheck { get; set; }

        /* Optional input parameter for MatchMe double-billing check.  This is a required parameter for MatchMe (luProdFeatureID = 30) */
        public int? OtherUserID { get; set; }

        public int? CardinalLogID { get; set; }
        public int PaymentMethodToken { get; set; }
        public int? luTrxProcessorID { get; set; }
        public bool VisaCheckout { get; set; }
        public bool MasterPass { get; set; }

        public SubscribeRequestBase()
        { }

        public SubscribeRequestBase(SubscribeRequestBase from)
        {
            AcctOrderModID = from.AcctOrderModID;
            OldUserStatusDt = from.OldUserStatusDt;
            GeoStateTaxID = from.GeoStateTaxID;
            CheckCC = from.CheckCC;
            UserID = from.UserID;
            PromoID = from.PromoID;
            TrxSrcID = from.TrxSrcID;
            OrderList = from.OrderList;
            STID = from.STID;
            SubBrandID = from.SubBrandID;
            AcctOrderID = from.AcctOrderID;
            TrxID = from.TrxID;
            IsComplimentary = from.IsComplimentary;
            EmployeeID = from.EmployeeID;
            Comment = from.Comment;
            CRCode = from.CRCode;
            AcctPymtMethID = from.AcctPymtMethID;
            IsRefund = from.IsRefund;
            SID = from.SID;
            ClientIP1 = from.ClientIP1;
            ClientIP2 = from.ClientIP2;
            ClientIP3 = from.ClientIP3;
            ClientIP4 = from.ClientIP4;
            SubscribeTextID = from.SubscribeTextID;
            DtlCode = from.DtlCode;
            FName = from.FName;
            LName = from.LName;
            FullName = from.FullName;
            Address1 = from.Address1;
            Address2 = from.Address2;
            CityName = from.CityName;
            PState = from.PState;
            PCountry = from.PCountry;
            PostalCode = from.PostalCode;
            CCMonth = from.CCMonth;
            CCYear = from.CCYear;
            CheckNum = from.CheckNum;
            OverrideSubOrderRules = from.OverrideSubOrderRules;
            Now = from.Now;
            ForceCompAddOn = from.ForceCompAddOn;
            EventID = from.EventID;
            PhoneNumberClear = from.PhoneNumberClear;
            ExpiredCCDtCheck = from.ExpiredCCDtCheck;
            OtherUserID = from.OtherUserID;
            CardinalLogID = from.CardinalLogID;
            PaymentMethodToken = from.PaymentMethodToken;
            luTrxProcessorID = from.luTrxProcessorID;
            VisaCheckout = from.VisaCheckout;
            MasterPass = from.MasterPass;
        }
    }

    public class SubscribeRequestPriorV4 : SubscribeRequestBase
    {
        public string DriverLic { get; set; }
        public string DLStateAbv { get; set; }
        public string DLCountryAbv { get; set; }
        public string AcctNumClear { get; set; }
        public string CardStartDT { get; set; }
        public string CardIssueNum { get; set; }
        public string XID { get; set; }
        public string UCAF { get; set; }
        public string CAVV { get; set; }
        public string ECI { get; set; }
        public int? VerificationReasonCode { get; set; }

        public SubscribeRequestPriorV4()
            : base()
        { }

        public SubscribeRequestPriorV4(SubscribeRequestPriorV4 from)
            : base(from)
        {
            DriverLic = from.DriverLic;
            DLStateAbv = from.DLStateAbv;
            DLCountryAbv = from.DLCountryAbv;
            AcctNumClear = from.AcctNumClear;
            CardStartDT = from.CardStartDT;
            CardIssueNum = from.CardIssueNum;
            XID = from.XID;
            UCAF = from.UCAF;
            CAVV = from.CAVV;
            ECI = from.ECI;
            VerificationReasonCode = from.VerificationReasonCode;
        }

    }




    public class SubscribeRequest : SubscribeRequestPriorV4
    {
        public SubscribeRequest() 
            : base()
        { }

        public SubscribeRequest(SubscribeRequest from)
            : base(from)
        { }
    }

    public class SubscribeResult
    {
        public int ReturnValue { get; set; }
        public int AccountOrderID { get; set; }
        public int TrxID { get; set; }
    }

    public class SubscribeRequestV3 : SubscribeRequestPriorV4
    {
        public SubscribeRequestV3()
            : base()
        { }

        public SubscribeRequestV3(SubscribeRequestPriorV4 from)
            : base(from)
        { }
    }

    public class SubscribeResultV3 : SubscribeResult
    {
        public int AccountPaymentMethodID { get; set; }
        public int? luTrxProcessorID { get; set; }
        public string ISOCurrencyCode { get; set; }
        public byte luTrxCategoryID { get; set; }
        public byte luTrxTypeID { get; set; }
    }



    public class SubscribeRequestV4 : SubscribeRequestBase
    {
        public string BankIdentification { get; set; }
        public string AccountNumberLastFour { get; set; }
        public string DebitoBranch { get; set; }

        public SubscribeRequestV4()
            : base()
        { }

        public SubscribeRequestV4(SubscribeRequestBase from)
            : base(from)
        { }
    }

    public class SubscribeResultV4 : SubscribeResultV3
    {

    }



}
