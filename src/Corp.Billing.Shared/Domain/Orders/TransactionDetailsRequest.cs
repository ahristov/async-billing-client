﻿using System;

namespace Corp.Billing.Shared.Domain.Orders
{
    [Serializable]
    public class TransactionDetailsRequest: IApiRequestWithQueryString
    {
        public int UserId { get; set; }
        public int TransactionId { get; set; }
        public Guid? SessionId { get; set; }
    }
}
