﻿namespace Corp.Billing.Shared.Domain.Discount
{
    public static class RenewalUpsellDefaults
    {
        public const int DefaultDaysBeforeRenewal = 10;
    }
}
