﻿using System;

namespace Corp.Billing.Shared.Domain.Discount
{
    [Serializable]
    public class RenewalDiscountStatusRequest
    {
        public int UserId { get; set; }
        public RenewalDiscountOfferType OfferType { get; set; }
        public decimal? PercentDiscount { get; set; }
    }
}
