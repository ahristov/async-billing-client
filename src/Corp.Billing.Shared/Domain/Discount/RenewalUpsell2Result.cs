﻿using System;

namespace Corp.Billing.Shared.Domain.Discount
{
    [Serializable]
    public class RenewalUpsell2Result
    {
        public RenewalUpsellResponseCodeType ResponseCode { get; set; }
    }
}
