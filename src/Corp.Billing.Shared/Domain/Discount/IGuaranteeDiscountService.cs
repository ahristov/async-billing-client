﻿namespace Corp.Billing.Shared.Domain.Discount
{
    public interface IGuaranteeDiscountService
    {
        // Guarantee

        RedeemGuaranteeResult RedeemGuarantee(RedeemGuaranteeRequest request);
    }
}
