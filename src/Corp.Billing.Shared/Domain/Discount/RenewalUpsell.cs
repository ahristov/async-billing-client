﻿using Corp.Billing.Shared.Domain.Subscription;
using System;

namespace Corp.Billing.Shared.Domain.Discount
{

    public class GetRenewalUpsellRequest : IApiRequestWithQueryString
    {
        private int _daysBeforeRenewal;

        public int UserId { get; set; }

        public int DaysBeforeRenewal
        {
            get
            {
                return (_daysBeforeRenewal <= 0) 
                    ? RenewalUpsellDefaults.DefaultDaysBeforeRenewal
                    : _daysBeforeRenewal;
            }
            set { _daysBeforeRenewal = value; }
        }

    }

    [Serializable]
    public class GetRenewalUpsellResult
    {
        public GetRenewalUpsellResult()
        { }

        public GetRenewalUpsellResult(GetUserBillingDataResult billingData)
        {
            BillingData = billingData;
        }

        public GetUserBillingDataResult BillingData { get; set; }

        public bool IsEligibleForOffer { get; set; }

        public bool UserWillRenewAtMultiMonth { get; set; }

        public bool PackageIncludesReplyForFree { get; set; }

        public decimal? OfferFullPrice { get; set; }
        public decimal? OfferMonthlyPrice { get; set; }
        public int OfferPercentageDiscount { get; set; }

    }



    public class ApplyRenewalUpsellRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }

        private int _daysBeforeRenewal;
        public int DaysBeforeRenewal
        {
            get
            {
                return (_daysBeforeRenewal <= 0)
                    ? RenewalUpsellDefaults.DefaultDaysBeforeRenewal
                    : _daysBeforeRenewal;
            }
            set { _daysBeforeRenewal = value; }
        }

        public Guid? Sid { get; set; }

        public byte? ClientIp1 { get; set; }
        public byte? ClientIp2 { get; set; }
        public byte? ClientIp3 { get; set; }
        public byte? ClientIp4 { get; set; }

    }

    [Serializable]
    public class ApplyRenewalUpsellResult
    {
        public RenewalUpsellResponseCodeType ResponseCode { get; set; }

    }


}
