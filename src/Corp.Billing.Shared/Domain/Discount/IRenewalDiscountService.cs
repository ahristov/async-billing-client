﻿namespace Corp.Billing.Shared.Domain.Discount
{
    public interface IRenewalDiscountService
    {
        // Renewal

        RenewalDiscountResult GetRenewalDiscountMessaging(RenewalDiscountRequest request);
        RenewalDiscountResultRNT GetRenewalDiscountMessagingRnt(RenewalDiscountRequestRNT request);
        RedeemRenewalDiscountResult RedeemRenewalDiscount(RedeemRenewalDiscountRequest request);
        RedeemRenewalDiscountResultRNT RedeemRenewalDiscountRnt(RedeemRenewalDiscountRequestRNT request);

    }
}
