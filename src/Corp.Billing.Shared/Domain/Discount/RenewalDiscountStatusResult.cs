﻿using System;

namespace Corp.Billing.Shared.Domain.Discount
{
    [Serializable]
    public class RenewalDiscountStatusResult
    {
        // Database data

        public RenewalDiscountStateType RenewalDiscountState { get; set; }

        public decimal? OriginalRenewalAmount { get; set; }
        public decimal? DiscountRenewalAmount { get; set; }
        public decimal? DiscountPercentApplied { get; set; }
        public decimal? NextRenewalAmount { get; set; }
        public DateTime? NextRenewalDate { get; set; }

        public short RenewalCount { get; set; }

        public byte DbErrorStateCode { get; set; }
        public string DbErrorMessage { get; set; }


        // Other

        public byte PackageMonths { get; set; }
        public string ISOCurrencyCode { get; set; }
        public RenewalAmounts DiscountAmounts { get; set; }

        public RenewalDiscountStatusResult()
        {
            DiscountAmounts = new RenewalAmounts();
        }
    }
}
