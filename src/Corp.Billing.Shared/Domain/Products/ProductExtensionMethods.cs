﻿using System.Reflection;

namespace Corp.Billing.Shared.Domain.Products
{
    public static class ProductExtensionMethods
    {
        public static int? GetFeatureProductId(this ProductFeatureType productFeature)
        {
            FieldInfo fi = productFeature.GetType().GetField(productFeature.ToString());

            FeatureProductIdAttribute[] attributes =
                (FeatureProductIdAttribute[])fi.GetCustomAttributes(typeof(FeatureProductIdAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].ProdId;
            }

            return null;
        }
    }
}
