﻿using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.Products
{
    public interface ITopSpotStatusGetter : IResetableCache<int>
    {
        TopSpotStatusResult GetTopSpotStatus(TopSpotStatusRequest request);
    }
}
