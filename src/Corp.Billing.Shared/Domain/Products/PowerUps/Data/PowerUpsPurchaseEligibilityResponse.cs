﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Products.PowerUps.Data
{

    public class PowerUpsPurchaseEligibilityResponse
    {
        public bool CanPayWithOneClick { get; set; }
        public IList<PowerUpFeature> Features { get; set; }

        public PowerUpsPurchaseEligibilityResponse()
        {
            Features = new List<PowerUpFeature>();
        }
    }
}
