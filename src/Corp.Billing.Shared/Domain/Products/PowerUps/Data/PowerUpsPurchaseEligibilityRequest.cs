﻿using System;

namespace Corp.Billing.Shared.Domain.Products.PowerUps.Data
{

    public class PowerUpsPurchaseEligibilityRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }
}
