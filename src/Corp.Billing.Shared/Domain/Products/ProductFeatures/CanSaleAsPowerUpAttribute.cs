﻿using System;

namespace Corp.Billing.Shared.Domain.Products.ProductFeatures
{
    [AttributeUsage(AttributeTargets.All)]
    public class CanSaleAsPowerUpAttribute : Attribute
    {
        public override bool Equals(object obj) => obj == this || obj is CanSaleAsPowerUpAttribute;
        public override int GetHashCode()=> base.GetHashCode();
        public override bool IsDefaultAttribute() => true;
    }
}
