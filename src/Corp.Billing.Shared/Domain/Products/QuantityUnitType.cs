﻿namespace Corp.Billing.Shared.Domain.Products
{
    public enum QuantityUnitType
    {
        Undefined,
        Subscription,
        Unit,
    }
}
