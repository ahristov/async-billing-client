﻿using System;
using Corp.Billing.Shared.Domain.Products.PowerUps.Data;

namespace Corp.Billing.Shared.Domain.Products.IneligibilityReasons
{
    [AttributeUsage(AttributeTargets.All)]
    public class CorrespondingPowerUpIneligibilityAttribute : Attribute
    {
        public PowerUpPurchaseIneligibilityType PurchaseIneligibility { get; private set; }

        public CorrespondingPowerUpIneligibilityAttribute(PowerUpPurchaseIneligibilityType purchaseIneligibility)
        {
            PurchaseIneligibility = purchaseIneligibility;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            CorrespondingPowerUpIneligibilityAttribute other = obj as CorrespondingPowerUpIneligibilityAttribute;

            return (other != null) && other.PurchaseIneligibility == this.PurchaseIneligibility;
        }
        public override int GetHashCode() => base.GetHashCode();
        public override bool IsDefaultAttribute() => PurchaseIneligibility == PowerUpPurchaseIneligibilityType.Eligible;
    }
}
