﻿using System;

namespace Corp.Billing.Shared.Domain.Products
{
    [Serializable]
    public class QuantityData: ICloneable
    {
        public decimal Amount { get; set; }
        public int QuantityUnit { get; set; }
        public string QuantityUnitName => ((QuantityUnitType)QuantityUnit).ToString();

        public QuantityData()
        { }

        public QuantityData(Quantity quantity)
        {
            Amount = quantity.Amount;
            QuantityUnit = (int)quantity.Unit;
        }

        public QuantityData Clone()
        {
            return this.MemberwiseClone() as QuantityData;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
