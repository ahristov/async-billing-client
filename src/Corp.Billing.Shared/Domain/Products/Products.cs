﻿using Corp.Billing.Shared.Subscription.Product;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.Products
{
    
    // MatchMe

    public class MatchMe30DayEligibilityCheckRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
        public int OtherUserId { get; set; }
    }

    public class MatchMe30DayEligibilityCheckResult
    {
        public byte AllowPurchase { get; set; }
        public DateTime? LastPurchaseDate { get; set; }
        public int ReturnValue { get; set; }
    }

    public class PutMeInUserListRequest
    {
        public int UserId { get; set; }
        public int OtherUserId { get; set; }
        public int TransactionId { get; set; }
    }

    public class PutMeInUserListResult
    {
        public int ReturnValue { get; set; }
    }



    // TopSpot

    public class TopSpotStatusRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    [Serializable]
    public class TopSpotStatusResult
    {
        public int ReturnValue { get; set; }
        public int UnusedCount { get; set; }
        public int FreeCount { get; set; }
        public DateTime? BeginDt { get; set; }
        public DateTime? EndDt { get; set; }
        public DateTime? EffectiveDt { get; set; }
        public DateTime? ExpirationDt { get; set; }
        public int ImpulseFeatureLogId { get; set; }
        public bool? IsComplimentary { get; set; }
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets if the user has 1-click MOP.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the user has 1-click MOP, false otherwise</c>.
        /// </value>
        public virtual bool CanPayWithOneClickPaymentMethod { get; set; }

        public bool IsInBoostRFFAbTest { get; set; }
        public IList<TotalsByProductType> TotalsByProductType { get; set; }

        public TopSpotStatusResult()
        {
            TotalsByProductType = new List<TotalsByProductType>();
        }
    }

    [Serializable]
    public class TotalsByProductType
    {
        public ProductAttributeTypeEnum ProductType { get; set; }
        public int PaidCount { get; set; }
        public int FreeCount { get; set; }
        public int UnusedPaidCount { get; set; }
        public int UnusedFreeCount { get; set; }
    }

    [Serializable]
    public class TotalsByProductID
    {
        public int? ProdID { get; set; }
        public int PaidCount { get; set; }
        public int FreeCount { get; set; }
        public int UnusedPaidCount { get; set; }
        public int UnusedFreeCount { get; set; }
    }

    public class ProvisionAndActivateTopSpotRequest
    {
        public int UserId { get; set; }
        public short Minutes { get; set; }
        public bool Activate { get; set; }
    }

    [Serializable]
    public class ProvisionAndActivateTopSpotResult
    {
        public int ImpulseFeatureLogId { get; set; }
    }

    public class ActivateUnclaimedImpulseFeatureRequest
    {
        public int UserId { get; set; }
        public byte luProductFeatureID { get; set; }
        public int? OtherUserId { get; set; }
    }

    public class ActivateUnclaimedImpulseFeatureResult
    {
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public short ImpulseMinutes { get; set; }
        public int ReturnValue { get; set; }
    }



    // ProfilePro

    public class CSAUserPCSStatusRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
    }

    public class CSAUserPCSStatusResult
    {
        public int ReturnValue { get; set; }
        public int UserId { get; set; }
        public bool CanPurchasePCS { get; set; }
        public int DefaultAcctPymtMethID { get; set; }
        public bool CanOneClickPurchasePCS { get; set; }

        public IList<CSAUserPCSStatusItem> Items { get; set; }

        public CSAUserPCSStatusResult()
        {
            Items = new List<CSAUserPCSStatusItem>();
        }
    }

    public class CSAUserPCSStatusItem
    {
        public int AcctDtlID { get; set; }

        public decimal PurchaseAmt { get; set; }

        public DateTime PurchaseDt { get; set; }

        public DateTime? FulfillmentDt { get; set; }
    }



    // Unit products

    public class UnitUserStatusRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
        public bool AllowPayPal { get; set; }
    }

    public class UnitUserStatusResult
    {
        public bool CanOneClickPurchase { get; set; }
        public int? DefaultAccountPaymentMethodId { get; set; }
        public int ReturnValue { get; set; }
    }



    public class UnitDisplayStatusRequest : IApiRequestWithQueryString
    {
        public int UserId { get; set; }
        public byte? ProdFeatureId { get; set; }
        public int? EventId { get; set; }
    }

    public class UnitDisplayStatusResult
    {
        public int ReturnValue { get; set; }
        public IList<UnitDisplayStatusItem> Items;

        public UnitDisplayStatusResult()
        {
            Items = new List<UnitDisplayStatusItem>();
        }
    }

    public class UnitDisplayStatusItem
    {
        public int TrxID { get; set; }
        public int AcctDtlID { get; set; }
        public short ProdDtlID { get; set; }
        public byte luProdFeatureID { get; set; }
        public string Descr { get; set; }
        public DateTime PurchaseDate { get; set; }
        public decimal SalesPrice { get; set; }
        public short UnitsPurchased { get; set; }
        public int IsRenewable { get; set; }
        public int EventID { get; set; }
        public string AcctNum { get; set; }
        public string DtlCode { get; set; }
        public bool IsRefunded { get; set; }
        public bool IsPending { get; set; }
    }


}
