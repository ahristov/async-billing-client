﻿using System;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures
{
    /// <summary>
    /// Corresponding to "TotalFeatureCounts" 2nd recordset.
    /// </summary>
    [Serializable]
    public class TotalFeatureCountsRow
    {
        public int LuProdFeatureID { get; set; }
        public int TotalFreeCount { get; set; }
        public int TotalPaidCount { get; set; }
    }
}
