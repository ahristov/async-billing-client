﻿using System.Net;
using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Exceptions
{
    public class ActivateUnClaimedImpulseFeatureDbException : BaseImpulseFeaturesException
    {
        public ActivateUnClaimedImpulseFeatureDbException(ActivateRequest req, int? activateDbReturnValue)
            : base(req, activateDbReturnValue)
        { }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.DbErrorActivatingUnClaimedImpulseFeature; } }
        public override HttpStatusCode HttpStatusCode { get { return HttpStatusCode.InternalServerError; }}

        public override string ErrorMessage { get { return string.Format("Error activating impulse feature {0} for user {1}: DB Return={2}", ProductFeature, UserId, ActivateDbReturnValue); } }
    }
}
