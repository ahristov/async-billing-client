﻿using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;
using System;
using System.Net;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Exceptions
{
    public abstract class BaseImpulseFeaturesException : Exception
    {
        public int UserId { get; private set; }
        public ImpulseFeatureType ProductFeature { get; private set; }
        public int? OtherUserId { get; private set; }
        public int? ActivateDbReturnValue { get; private set; }


        public BaseImpulseFeaturesException(GetStatusRequest req)
        {
            UserId = req.UserId;
            ProductFeature = req.ProductFeature;
        }

        public BaseImpulseFeaturesException(ActivateRequest req)
        {
            UserId = req.UserId;
            ProductFeature = req.ProductFeature;
            OtherUserId = req.OtherUserId;
        }

        public BaseImpulseFeaturesException(ActivateRequest req, int? activateDbReturnValue)
            : this(req)
        {
            ActivateDbReturnValue = activateDbReturnValue;
        }


        public abstract int ErrorCode { get; }
        public abstract string ErrorMessage { get; }

        public virtual HttpStatusCode HttpStatusCode { get { return (HttpStatusCode)422; } }
    }
}
