﻿using System;

namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures
{
    /// <summary>
    /// Corresponding to "FeatureLevelDetails" 3rd recordset.
    /// </summary>
    [Serializable]
    public class ImpulseFeatureDailyRow
    {
        public DateTime FeatureUsageDate { get; set; }
        public int ActivatedUnitCount { get; set; }
        public int UnitsInReserve { get; set; }
        public bool IsComplimentary { get; set; }
    }
}
