﻿namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data
{
    public class ActivateRequest
    {
        public int UserId { get; set; }
        public ImpulseFeatureType ProductFeature { get; set; }
        public int? OtherUserId { get; set; }
    }
}
