﻿namespace Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data
{
    public enum ImpulseFeatureType : byte
    {
        Undefined = 0,
        DateCoach = 4,
        TopSpot = 26,
        Undercover = 27,
        SuperLikes = 39,
    }
}
