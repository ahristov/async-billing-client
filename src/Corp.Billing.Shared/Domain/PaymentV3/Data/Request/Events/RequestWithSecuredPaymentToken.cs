﻿using System;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request.Events
{
    [Serializable]
    public class RequestWithSecuredPaymentToken : PaymentRequest, IHasSecuredPaymentToken
    {
        public string SecuredPaymentToken
        {
            get { return base.PaymentToken;  }
            set { base.PaymentToken = value; }
        }

        public new RequestWithSecuredPaymentToken InitFrom(PaymentRequest x)
        {
            base.InitFrom(x);
            return this;
        }
    }
}
