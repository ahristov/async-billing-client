﻿using System;
using System.Linq;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request.Events
{
    [Serializable]
    public class RequestWithDefaultPaymentMethod : PaymentRequest
    {
        public RequestWithPaymentMethodToken ConvertToMopWithToken(int paymentMethodToken)
        {
            RequestWithPaymentMethodToken res = new RequestWithPaymentMethodToken
            {
                ClientIp1 = ClientIp1,
                ClientIp2 = ClientIp2,
                ClientIp3 = ClientIp3,
                ClientIp4 = ClientIp4,
                CountryCode = CountryCode,
                EventId = EventId,
                EventTotalPriceIncludingGuestsAndTax = EventTotalPriceIncludingGuestsAndTax,
                FirstName = FirstName,
                LastName = LastName,
                OrderItems = OrderItems.Select(x => x.Clone()).ToArray(),
                PlatformId = PlatformId,
                PostalCode = PostalCode,
                PromoId = PromoId,
                SessionId = SessionId,
                SiteCode = SiteCode,
                UrlCode = UrlCode,
                UserId = UserId,
                PaymentMethodToken = paymentMethodToken,
            };

            return res;
        }

        public new RequestWithDefaultPaymentMethod InitFrom(PaymentRequest x)
        {
            base.InitFrom(x);
            return this;
        }
    }
}
