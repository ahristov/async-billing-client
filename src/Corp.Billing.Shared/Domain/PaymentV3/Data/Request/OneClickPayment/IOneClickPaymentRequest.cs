﻿using Corp.Billing.Shared.Domain.PaymentV3.Data.Request.OrderItems;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request.OneClickPayment
{
    public interface IOneClickPaymentRequest : IPaymentRequestBase, ISessionInformation
    {
        ICollection<OrderItem> OrderItems { get; set; }
    }
}
