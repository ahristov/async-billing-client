﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Request
{
    public interface IEventInformation1
    {
        int EventId { get; set; }
        decimal EventTotalPriceIncludingGuestsAndTax { get; set; }
    }
}
