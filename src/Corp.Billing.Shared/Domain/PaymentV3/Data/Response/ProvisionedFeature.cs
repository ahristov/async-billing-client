﻿using Corp.Billing.Shared.Domain.Products;
using System;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Response
{
    public class ProvisionedFeature
    {
        public ProductFeatureType FeatureType { get; set; }
        public Duration Duration { get; set; }
        public Quantity Quantity { get; set; }

        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }

        public ProvisionedFeature()
        {
            Duration = new Duration();
            Quantity = new Quantity();
        }
    }
}
