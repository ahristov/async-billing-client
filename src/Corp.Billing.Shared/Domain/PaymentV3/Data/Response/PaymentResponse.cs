﻿using System;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Response
{
    [Serializable]
    public class PaymentResponse
    {
        public int PaymentResult { get; set; }
        public string PaymentResultName => ((PaymentResultTypeEnum)this.PaymentResult).ToString();

        public int TrxId { get; set; }
        public decimal TransactionAmount { get; set; }
        public string TransactionISOCurrencyCode { get; set; }
        public string XmlMatchEventTicketsPurchased { get; set; }
        public decimal RefundAmount { get; set; }
        public PerformedActionType PerformedAction { get; set; }

    }
}
