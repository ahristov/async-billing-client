﻿using System;
using System.Collections.Generic;
using Corp.Billing.Shared.Domain.Orders;
using Corp.Billing.Shared.Subscription.Payment;

namespace Corp.Billing.Shared.Domain.PaymentV3.Data.Response
{
    public class OrderPaymentResponse : PaymentResponse
    {
        public IList<ProvisionedFeature> AddedFeatures { get; set; }

        public decimal? RenewalPaymentTotalAmount { get; set; }
        public decimal TaxRate { get; set; }
        public byte Months { get; set; }
        public short FreeTrialDays { get; set; }
        public PaymentDefinitions.PurchasePaymentTypes PaymentMethodType { get; set; }
        public PaymentDefinitions.CreditCardTypes PaymentMethodCardType { get; set; }
        public string PaymentMethodLastFourDigits { get; set; }
        public int RateCardKey { get; set; }
        public Guid? RateCardRefId { get; set; }
        public string AppliedPromoId { get; set; }
        public bool DiscountWasAppliedToInitialPayment { get; set; }
        public bool DiscountAppliesToRenewalPayment { get; set; }

        public TransactionDetailsResult TransactionDetails { get; set; }

        public OrderPaymentResponse()
            : base()
        {
            AddedFeatures = new List<ProvisionedFeature>();
            AppliedPromoId = Consts.DefaultPromoId;
        }
    }
}
