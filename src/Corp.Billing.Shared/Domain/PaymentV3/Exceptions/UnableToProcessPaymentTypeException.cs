﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public class NotOneClickMethodOfPaymentException : ExceptionBase
    {
        public override int ErrorCode => (int)ExceptionErrorCodesEnum.UnableToProcessMopTypeAsOneClick;
        public override string ErrorMessage => ErrorMessages.CANNOT_PROCESS_MOP_AS_ONECLICK;
    }
}
