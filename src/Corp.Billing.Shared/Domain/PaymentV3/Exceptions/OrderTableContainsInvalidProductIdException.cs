﻿namespace Corp.Billing.Shared.Domain.PaymentV3.Exceptions
{
    public class OrderTableContainsInvalidProductIdException : ExceptionBase
    {
        public override int ErrorCode => (int)ExceptionErrorCodesEnum.OrderTableContainsInvalidProductId;
        public override string ErrorMessage => ErrorMessages.ORDER_TABLE_CONTAINS_INVALID_PRODUCT_ID;
    }
}
