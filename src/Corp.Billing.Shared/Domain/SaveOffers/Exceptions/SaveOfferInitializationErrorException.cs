﻿using Corp.Billing.Shared.Domain.SaveOffers.Services;

namespace Corp.Billing.Shared.Domain.SaveOffers.Exceptions
{
    public class SaveOfferInitializationErrorException : BaseSaveOfferException
    {
        public SaveOfferInitializationErrorException() : base() { }
        public SaveOfferInitializationErrorException(BaseSaveOfferRequest request) : base(request) { }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.SaveOfferInitializationError; } }
        public override string ErrorMessage { get { return string.Format("SaveOffers: Initializarion error"); } }
    }
}
