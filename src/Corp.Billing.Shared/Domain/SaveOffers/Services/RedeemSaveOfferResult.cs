﻿using Corp.Billing.Shared.Domain.LegalTexts;
using Corp.Billing.Shared.Domain.SaveOffers.Data;
using System;

namespace Corp.Billing.Shared.Domain.SaveOffers.Services
{
    [Serializable]
    public class RedeemSaveOfferResult : ILegalTextsResponse
    {
        public bool Success { get; set; }
        public RejectionReasonType RejectionReason { get; set; }

        public string LegalDisclaimerTermsKey { get; set; }
        public string LegalDisclaimerTerms { get; set; }
    }
}
