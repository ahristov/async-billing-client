﻿using Corp.Billing.Shared.Domain.Discount;
using Corp.Billing.Shared.Domain.LegalTexts;
using Corp.Billing.Shared.Domain.RateCard;
using Corp.Billing.Shared.Domain.SaveOffers.Data;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.SaveOffers.Services
{
    [Serializable]
    public class GetSaveOfferEligibilityResult
    {
        public bool IsEligible { get; set; }
        public SaveOfferType EligibleSaveOfferType { get; set; }
        public List<Tuple<RejectionReasonType, string>> RejectionReasons { get; set; }

        // Save offer details

        public DateTime? MaxBaseFeatureEndDate { get; set; }
        public byte? LastSubscriptionMonths { get; set; }

        public decimal InitialPrice { get; set; }
        public decimal InitialTax { get; set; }
        public decimal InitialCost { get { return InitialPrice + InitialTax; } }

        public decimal RenewalPrice { get; set; }
        public decimal RenewalTax { get; set; }
        public decimal RenewalCost { get { return RenewalPrice + RenewalTax; } }

        public string ISOCurrencyCode { get; set; }

        /// <summary>
        /// For stack sub offers holds the base product
        /// </summary>
        public RateCardV2BaseItem StackSubBaseProduct { get; set; }

        /// <summary>
        /// For stack sub holds the discount promo Id
        /// </summary>
        public string StackSubPromoId { get; set; }

        /// <summary>
        /// For renewal discount offers holds renewal discount info
        /// </summary>
        public RenewalDiscountResultBase RenewalDiscountInfo { get; set; }


        /// <summary>
        /// If this is set, the user has accepted renewal discount with that percentage and it is not applied yet (will be applied on next renewal).
        /// </summary>
        public decimal? AcceptedNotAppliedYetRenewalDiscountPercentage { get; set; }


        //

        public bool HasRejections { get { return RejectionReasons != null && RejectionReasons.Count > 0; } }

        //

        public GetSaveOfferEligibilityResult()
        {
            RejectionReasons = new List<Tuple<RejectionReasonType, string>>();
        }

        //

        public void AddRejectionReason(RejectionReasonType reason, Type source)
        {
            RejectionReasons.Add(new Tuple<RejectionReasonType, string>(reason, source.Name));
        }

        public void AddRejectionReasons(List<Tuple<RejectionReasonType, string>> reasons)
        {
            RejectionReasons.AddRange(reasons);
        }

    }
}
