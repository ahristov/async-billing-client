﻿using Corp.Billing.Shared.Domain.RateCardV3.Services;
using Corp.Billing.Shared.Facilities;
using System.Net;

namespace Corp.Billing.Shared.Domain.RateCardV3.Exceptions
{
    public class UnableToResolveRateCardKeyException : BaseRateCardexception
    {
        public UnableToResolveRateCardKeyException(GetRateCardRequest req, string details)
            : base(req)
        {
            this.AddData(new
            {
                userId = req.UserId,
                catalogType = req.CatalogTypeParam,
                catalogSource = req.CatalogSourceParam,
                platformId = req.PlatformId,
                sessionId = req.SessionId,
                rateCardKey = req.RateCardKey,
                dbgString = DbgString(req),
                details = details,
            });
        }

        public override HttpStatusCode HttpStatusCode { get { return HttpStatusCode.InternalServerError; } }

        public override int ErrorCode { get { return (int)ExceptionErrorCodes.UnableToResolveRateCardKey; } }
        public override string ErrorMessage => ErrorMessages.UNABLE_TO_RESOLVE_RATECARD_KEY;
    }
}
