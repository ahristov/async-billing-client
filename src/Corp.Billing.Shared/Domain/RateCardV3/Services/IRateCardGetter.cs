﻿namespace Corp.Billing.Shared.Domain.RateCardV3.Services
{
    public interface IRateCardGetter
    {
        GetRateCardResponse GetRateCard(GetRateCardRequest req);
        GetExtendedRateCardResponse GetExtendedRateCard(GetRateCardRequest req);
    }
}
