﻿using Corp.Billing.Shared.Domain.Promos;
using Corp.Billing.Shared.Domain.RateCard;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.RateCardV3.Services
{
    [Serializable]
    public abstract class GetProdTokensToSellableResponseBase
    {
        public int RateCardKey { get; set; }
        public Guid? RateCardRefId { get; set; }
        public string PromoId { get; set; }
        public GetPromoDataResult PromoData { get; set; }
        public decimal TaxRate { get; set; }

        public GetProdTokensToSellableResponseBase()
        {
            PromoData = new GetPromoDataResult();
        }
    }
}
