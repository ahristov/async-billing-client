﻿using System;

namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    [Serializable]
    public class ProductSavingsData: ICloneable
    {
        public int SavingsType { get; set; }
        public string SavingsTypeName => ((ProductSavingsType)SavingsType).ToString();
        public decimal RegularPriceSingle { get; set; }
        public decimal RegularPriceTotal { get; set; }
        public decimal SavingsPerSingle { get; set; }
        public decimal SavingsTotal { get; set; }
        public decimal PercentageSavings { get; set; }
        public ProductSavingsData Clone()
        {
            return this.MemberwiseClone() as ProductSavingsData;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
