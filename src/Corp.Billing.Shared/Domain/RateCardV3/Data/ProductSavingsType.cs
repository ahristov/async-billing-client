﻿namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    public enum ProductSavingsType
    {
        Undefined,
        SavingsToNonDiscountedOneMonth,
        SavingsToSmallestUnits,
        SavingsToCurrentPackage,
    }
}
