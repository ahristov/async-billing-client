﻿using Corp.Billing.Shared.Domain.Products;
using System;

namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    [Serializable]
    public class ProductFeatureData: ICloneable
    {
        public byte ProductFeature { get; set; }
        public string ProductFeatureName => ((ProductFeatureType)ProductFeature).ToString();

        public ProductFeatureData()
        { }

        public ProductFeatureData(byte featureType)
        {
            ProductFeature = featureType;
        }

        public ProductFeatureData(ProductFeatureType featureType)
        {
            ProductFeature = (byte)featureType;
        }

        public ProductFeatureData Clone()
        {
            return this.MemberwiseClone() as ProductFeatureData;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
