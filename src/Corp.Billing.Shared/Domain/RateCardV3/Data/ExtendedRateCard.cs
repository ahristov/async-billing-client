﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.RateCardV3.Data
{
    public class ExtendedRateCard
    {
        public Guid RateCardRefId { get; set; }
        public IList<ExtendedProduct> Products { get; set; }
        public decimal TaxRate { get; set; }
        public bool HasMarkedUpPrices { get; set; }
        public short? FreeTrialProductId { get; set; }
        public short? FreeTrialDays { get; set; }

        public string AppliedPromoId { get; set; }
        public int? RateCardKey { get; set; }

        public byte? OtherUserGenderGenderSeek { get; set; }

        public IList<PCRuleConditionTrace> RuleConditionTraces { get; private set; }

        public ExtendedRateCard()
        {
            Products = new List<ExtendedProduct>();
            RuleConditionTraces = new List<PCRuleConditionTrace>();
        }
    }
}
