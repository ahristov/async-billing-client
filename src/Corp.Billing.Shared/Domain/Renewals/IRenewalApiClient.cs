﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.Domain.Renewals
{
    public interface IRenewalApiClient
    {
        void RenewUser(int userID);
    }
}
