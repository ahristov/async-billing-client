﻿namespace Corp.Billing.Shared.Domain.VisaCheckout
{
    public class DecryptPaymentDataResponse
    {
        public string Payload { get; set; }

        public DtoPaymentRequest PaymentRequest { get; set; }
        public DtoUserData UserData { get; set; }
        public long CreationTimeStamp { get; set; } // 1442415998798,
        public DtoPaymentInstrument PaymentInstrument { get; set; }
        public DtoRiskData RiskData { get; set; }

        public class DtoPaymentRequest
        {
            public string CurrencyCode { get; set; } // "USD",
            public double Total { get; set; } // "10.54",
            public double Subtotal { get; set; } // "1.54",
            public double Tax { get; set; } // "12.08"
        }

        public class DtoUserData
        {
            public string UserFirstName { get; set; } // "Atanas",
            public string UserLastName { get; set; } // "Hristov",
            public string UserFullName { get; set; } // "Atanas Hristov",
            public string UserName { get; set; } // "atanas.hristov@match.com",
            public string UserEmail { get; set; } // "atanas.hristov@match.com",
            public string EncUserId { get; set; } // "851GKH+tvq0+2x7dt7EZBMD+pnDX28NTuFpgAHA6xxc="
        }

        public class DtoPaymentInstrument
        {
            public string Id { get; set; } // "yBlJ0HwCXaqSCLztYkcNpd2ovaGncE4IXPbajeMoa/w=",
            public string LastFourDigits { get; set; } // "4821",
            public string BinSixDigits { get; set; } // "400552",
            public string AccountNumber { get; set; } // "4005529998884821",
            public DtoPaymentType PaymentType { get; set; }
            public DtoBillingAddress BillingAddress { get; set; }
            public string VerificationStatus { get; set; } // VERIFIED, NOT_VERIFIED, CONSUMER_OVERRIDE
            public bool Expired { get; set; }
            public string NameOnCard { get; set; }
            public string CardFirstName { get; set; }
            public string CardLastName { get; set; }
            public DtoExpirationDate ExpirationDate { get; set; }
        }

        public class DtoPaymentType
        {
            public string CardBrand { get; set; } // VISA, MASTERCARD, AMEX, DISCOVER
            public string CardType { get; set; } // CREDIT, DEBIT, CHARGE, PREPAID, DEFERRED_DEBIT, NONE
        }

        public class DtoBillingAddress
        {
            public string PersonName { get; set; } // "Atanas Hristov",
            public string FirstName { get; set; } // "Atanas",
            public string LastName { get; set; } // "Hristov",
            public string Line1 { get; set; } // "1234 Match Ave",
            public string City { get; set; } // "Frisco",
            public string StateProvinceCode { get; set; } // "TX",
            public string PostalCode { get; set; } // "75034",
            public string CountryCode { get; set; } // "US",
            public string Phone { get; set; } // "9724648888",
            public bool Default { get; set; } // false
        }

        public class DtoExpirationDate
        {
            public string Month { get; set; }
            public string Year { get; set; }
        }

        public class DtoRiskData
        {
            public string Advice { get; set; } // LOW, MEDIUM, HIGH, UNAVAILABLE
            public int Score { get; set; } //0..99, the higher the score, the higher the risk
            public string AvsResponseCode { get; set; } // Address verification system response code.
            public string CvvResponseCode { get; set; } // Card validation verification system response code.
            public long AgeOfAccount { get; set; } // Number of days since the Visa Checkout account was created
        }
    }
}
