﻿namespace Corp.Billing.Shared.Domain.VisaCheckout
{
    public interface IVisaCheckoutService
    {
        UpdatePaymentInfoResponse UpdatePaymentInfo(UpdatePaymentInfoRequest request);
        DecryptPaymentDataResponse DecryptPaymentData(DecryptPaymentDataRequest request);
    }
}
