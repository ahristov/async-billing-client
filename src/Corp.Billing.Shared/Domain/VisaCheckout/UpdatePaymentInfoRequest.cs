﻿namespace Corp.Billing.Shared.Domain.VisaCheckout
{
    public class UpdatePaymentInfoRequest
    {
        public string CallId { get; set; }
        public decimal Total { get; set; }
        public string CurrencyCode { get; set; }
    }
}
