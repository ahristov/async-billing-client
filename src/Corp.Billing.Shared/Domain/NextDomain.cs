﻿namespace Corp.Billing.Shared.Domain
{
    public class NextProcessOrderResult
    {
        public int Success { get; set; }
        public string FriendlyErrorMessage { get; set; }
        public NextOrderResult Result { get; set; }
    }

    //not sure if this is needed
    public class NextProcessOrderRequest
    {
        public int UserId { get; set; }
        public int BrandId { get; set; }
        public string PromoId { get; set; }
        public string IPAddress { get; set; }
    }

    public enum NextOrderResult 
    {
        Success,
        TokenFetchFailed,
        MatchLockGetFailed,
        OrderCreationFailed,
        MappingIntoTransactionRequestFailed,
        PaymentProviderFailed,
        UpdateStatusFailed,
        UnknownError
    }
}
