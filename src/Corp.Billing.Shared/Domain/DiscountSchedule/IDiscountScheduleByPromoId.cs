﻿using System;
using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.DiscountSchedule
{
    public interface IDiscountScheduleByPromoId
    {
        ICollection<int> GetDiscountScheduleByPromoId(string promoId);
    }
}
