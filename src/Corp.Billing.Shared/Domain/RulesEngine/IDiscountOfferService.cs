﻿namespace Corp.Billing.Shared.Domain.RulesEngine
{
    public interface IDiscountOfferService
    {
        DiscountInfoResult GetDiscountInfo(DiscountInfoRequest request);
    }
}
