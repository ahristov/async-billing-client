﻿namespace Corp.Billing.Shared.Domain.BillingOrigins.Exceptions
{
    public class BillingOriginsExistsWithDifferentDataExcepion : BilingOriginsBaseException
    {
        public BillingOriginsExistsWithDifferentDataExcepion(int userId)
            : base(userId)
        { }

        public override int ErrorCode => (int)ExceptionErrorCodes.BillingOriginsExistsWithDifferentData;

        public override string ErrorMessage => "Billing origins for this session already exist with the different values.";
    }
}
