﻿using Corp.Billing.Shared.Domain.BillingOrigins.Data;
using Corp.Billing.Shared.Facilities;

namespace Corp.Billing.Shared.Domain.BillingOrigins
{
    public interface IBillingOriginsStorage : IResetableCache<int>
    {
        BillingOriginsStoredData GetBillingOriginsStoredData(int userId);
        void SetSetBillingOriginsStoredData(int userId, BillingOriginsStoredData billingOrigins);
    }
}
