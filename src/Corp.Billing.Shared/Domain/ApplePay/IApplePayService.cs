﻿namespace Corp.Billing.Shared.Domain.ApplePay
{
    public interface IApplePayService
    {
        string ValidateMerchant(ValidateMerchantRequest request);
    }

    public class ValidateMerchantRequest
    {
        public string AppleValidateUrl { get; set; }
        public string ApplePayMerchantId { get; set; }
        public string MerchantHostName { get; set; }
    }

}
