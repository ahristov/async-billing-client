﻿using Corp.Billing.Shared.Domain.GiftCards;
using Corp.Billing.Shared.ProxyContracts;

namespace Corp.Billing.Shared.Domain.Payments
{
    public interface IGiftCardPaymentService
    {
        GiftCardProcessResult ProcessCard(GiftCardPaymentRequestMessage request);
        GiftCardEligibilityResult GetEligibility(int userId);
    }
}
