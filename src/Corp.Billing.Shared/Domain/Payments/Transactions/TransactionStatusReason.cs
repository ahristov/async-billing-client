﻿namespace Corp.Billing.Shared.Domain.Payments.Transactions
{
    /// <summary>
    /// Duplicate of Services.Common.Payment.Entities.TransactionStatusReason
    /// </summary>
    public enum TransactionStatusReason
    {
        None = 0,
        Address = 1,
        SecurityCode = 2,
        Expiration = 3,
        Validation = 4,
        Fraud = 5,
        Blocked = 6,
        NotSupported = 7,
        InsufficientFunds = 8,
        Error = 200,
        Other = 255
    }
}
