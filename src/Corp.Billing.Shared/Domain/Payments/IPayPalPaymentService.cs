﻿using Corp.Billing.Shared.ProxyContracts;

namespace Corp.Billing.Shared.Domain.Payments
{
    public interface IPayPalPaymentService
    {
        PayPalPaymentResponseMessage ProcessPayment(PayPalPaymentRequestMessage request);
        PayPalBillingAgreementTokenResponseMessage CreateBillingAgreementToken(PayPalBillingAgreementTokenRequestMessage request);
        PayPalBillingAgreementResponseMessage CreateBillingAgreement(PayPalBillingAgreementRequestMessage request);
    }
}