﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Corp.Billing.Shared.Domain.Payments
{
    public class CreateTrxRequest
    {
        public int UserId { get; set; }
        public int AccountDetailRows { get; set; }
        public bool TIP { get; set; }
        public int AccountPaymentMethodId { get; set; }
        public int TransactionProcessorId { get; set; }
        public decimal PreTaxAmount { get; set; }
        public byte TransactionTypeId { get; set; }
        public byte TransactionSourceId { get; set; }
        public byte TransactionCategoryId { get; set; }
        public int EmployeeId { get; set; }
        public string Comment { get; set; }
        public short CRCode { get; set; }
        public int OrigTransactionId { get; set; }
        public byte TransactionStatusId { get; set; }
        public byte MaxSubmitDays { get; set; }
        public DateTime SubmitOnDate { get; set; }
        public decimal TaxRate { get; set; }
        public string AccountDetailList { get; set; }
        public int CreditDays { get; set; }
        public int RefundId { get; set; }
    }

    public class CreateTrxResult
    {
        public int TransactionId { get; set; }
        public string ISOCurrencyCode { get; set; }
        public int ReturnValue { get; set; }
    }
}
