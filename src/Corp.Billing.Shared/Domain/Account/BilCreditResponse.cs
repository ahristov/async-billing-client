﻿using System;

namespace Corp.Billing.Shared.Domain.Account
{
    [Serializable]
    public class BilCreditResponse: IApiResponse
    {
        public short CompDays { get; set; }
        public int ReturnValue { get; set; }
    }
}
