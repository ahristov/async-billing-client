﻿using System.Collections.Generic;

namespace Corp.Billing.Shared.Domain.ProfileProLite
{
    public interface IProfileProRedemptionService
    {
        void Redeem(ProfileProLiteRedemption request);
        ProfileProLiteRedemptionStatus GetRedemptions(int userId);
    }
}
