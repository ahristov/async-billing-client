﻿using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.AbTesting
{
    public class AbTestingAsyncClient : BaseApiAsyncClient, IAbTestingAsyncClient
    {
        public AbTestingAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

        public async Task<bool> Evaluate(PCRulesRequest request, string testName, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.RulesEngine.EvaluateTest.Replace("{testName}", testName) + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<bool>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<EvaluateTestVariablesResult> EvaluateTestVariables(PCRulesRequest request, string testName, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.RulesEngine.EvaluateTestVariables.Replace("{testName}", testName) + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<EvaluateTestVariablesResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }
    }
}
