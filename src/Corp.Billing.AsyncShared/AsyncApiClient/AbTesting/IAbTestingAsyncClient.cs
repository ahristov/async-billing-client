﻿using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.AbTesting
{
    public interface IAbTestingAsyncClient
    {
        Task<bool> Evaluate(Shared.Domain.ProductCatalog.RulesEngine.PCRulesRequest request, string testName, CancellationToken? cancellationToken = null);
        Task<Shared.Domain.ProductCatalog.RulesEngine.EvaluateTestVariablesResult> EvaluateTestVariables(Shared.Domain.ProductCatalog.RulesEngine.PCRulesRequest request, string testName, CancellationToken? cancellationToken = null);

    }
}
