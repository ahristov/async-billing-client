﻿using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Discounting
{
    public class DiscountingApiAsyncClient : BaseApiAsyncClient, IDiscountingApiAsyncClient
    {
        public DiscountingApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

        public async Task<Shared.Domain.ProductCatalog.DiscountEngine.DiscountInfoResult> GetDiscountInfo(PCRulesRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.ProductCatalog.DiscountEngine.DiscountOffer + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<Shared.Domain.ProductCatalog.DiscountEngine.DiscountInfoResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }
    }
}
