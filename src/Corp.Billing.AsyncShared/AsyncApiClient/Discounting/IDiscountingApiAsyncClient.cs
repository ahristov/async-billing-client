﻿using Corp.Billing.Shared.Domain.ProductCatalog.RulesEngine;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Discounting
{
    public interface IDiscountingApiAsyncClient
    {
        Task<Shared.Domain.ProductCatalog.DiscountEngine.DiscountInfoResult> GetDiscountInfo(PCRulesRequest request, CancellationToken? cancellationToken = null);
    }
}
