﻿using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Orders;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Orders
{
    public class OrderApiAsyncClient : BaseApiAsyncClient, IOrderApiAsyncClient
    {
        public OrderApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

        public async Task<BillEventTicketsResult> BillEventTicketsAsync(BillEventTicketsRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.UnitProducts.BillEventTicketsPath;
            var apiResult = await ApiCommand.PostAsync<BillEventTicketsResult>(url, request, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<ImpulseBuyLastPurchaseResult> GetImpulseBuyLastPurchaseAsync(ImpulseBuyLastPurchaseRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.UnitProducts.GetImpulseBuyLastPurchase + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<ImpulseBuyLastPurchaseResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<GetOfflineTransferDataResult> GetOfflineTransferDataAsync(GetOfflineTransferDataRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.Transaction.UpdateOfflineTransactionStatusPath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<GetOfflineTransferDataResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<Shared.Domain.Orders.UpdateOfflinePaymentStatusResponse> UpdateOfflinePaymentStatusAsync(Shared.Domain.Orders.UpdateOfflinePaymentStatusRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Orders.Transaction.UpdateOfflineTransactionStatusPath;
            var apiResult = await ApiCommand.PutAsync<Shared.Domain.Orders.UpdateOfflinePaymentStatusResponse>(url, request);
            return apiResult.ResponseData;
        }
    }
}
