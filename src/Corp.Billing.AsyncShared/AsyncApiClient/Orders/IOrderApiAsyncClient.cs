﻿using Corp.Billing.Shared.Domain.Orders;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Orders
{
    /// <summary>
    /// This interface is the async version for the following synchronous interfaces:
    /// <see cref="IOrderService"/> 
    /// (<see cref="Shared.ApiClient.Orders.OrderApiClient"/>, skipping legacy functions)
    /// </summary>
    public interface IOrderApiAsyncClient
    {
        // Transaction
        Task<UpdateOfflinePaymentStatusResponse> UpdateOfflinePaymentStatusAsync(UpdateOfflinePaymentStatusRequest request, CancellationToken? cancellationToken = null);
        Task<GetOfflineTransferDataResult> GetOfflineTransferDataAsync(GetOfflineTransferDataRequest request, CancellationToken? cancellationToken = null);
        // Unit products
        Task<BillEventTicketsResult> BillEventTicketsAsync(BillEventTicketsRequest request, CancellationToken? cancellationToken = null);
        Task<ImpulseBuyLastPurchaseResult> GetImpulseBuyLastPurchaseAsync(ImpulseBuyLastPurchaseRequest request, CancellationToken? cancellationToken = null);
    }
}
