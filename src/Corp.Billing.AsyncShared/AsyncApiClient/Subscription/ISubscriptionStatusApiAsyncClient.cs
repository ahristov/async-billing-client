﻿using Corp.Billing.Shared.Domain.Promos;
using Corp.Billing.Shared.Domain.Subscription;
using Corp.Billing.Shared.Domain.UserFeatures;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Subscription
{
    /// <summary>
    /// This interface is the async version for the following synchronous interfaces:
    /// <see cref="IUserSubscriptionStatus"/>
    /// <see cref="IUserSubscriptionStatusV2"/>
    /// <see cref="Shared.ApiClient.Subscription.UserBillingDataApiClient"/>
    /// <see cref="IUserBillingData"/>
    /// <see cref="Shared.ApiClient.Subscription.IUserSubscriptionSummaryApiClient"/>
    /// </summary>
    public interface ISubscriptionStatusApiAsyncClient
    {
        // This is not needed, use GetDisplayStatus()
        // Task<List<DisplayUserStatusResult>> GetUserStatusBy(int userId, CancellationToken? cancellationToken = null);

        Task<List<DisplayUserStatusResult>> GetDisplayStatusAsync(DisplayUserStatusRequest request, CancellationToken? cancellationToken = null);
        Task<DisplayUserStatusResponse> GetDisplayStatusV2Async(DisplayUserStatusRequest request, CancellationToken? cancellationToken = null);
        Task<UserPromoInfoResult> GetUserStatusPromoInfoAsync(UserPromoInfoRequest request, CancellationToken? cancellationToken = null);
        Task<UserSubStatusResult> GetUserSubscritionStatusAsync(UserSubStatusRequest request, CancellationToken? cancellationToken = null);
        Task<UserSubStatusMessageResult> GetUserSubStatusMessageAsync(UserSubStatusMessageRequest request, CancellationToken? cancellationToken = null);
        Task<UserStatusDateResult> GetUserStatusDateAsync(UserStatusDateRequest request, CancellationToken? cancellationToken = null);
        Task<UserStatusDateResult> GetUserStatusDateV1Async(UserStatusDateRequest request, CancellationToken? cancellationToken = null);
        Task<UserFeatureResult> GetUserFeaturesAsync(UserFeatureRequest request, CancellationToken? cancellationToken = null);
        Task<UserFeaturesLightResult> GetUserFeaturesLightAsync(UserFeaturesLightRequest request, CancellationToken? cancellationToken = null);
        Task<CancelResult> CancelUserAsync(CancelRequest request, CancellationToken? cancellationToken = null);
        Task<GetUserBillingDataResult> GetUserBillingDataAsync(GetUserBillingDataRequest request, CancellationToken? cancellationToken = null);
        Task<GetCurrentPackageResult> GetCurrentPackageAsync(GetCurrentPackageRequest request, CancellationToken? cancellationToken = null);
        Task<UserSubscriptionSummaryResult> GetSubscriptionSummaryAsync(UserSubscriptionSummaryRequest request, CancellationToken? cancellationToken = null);
    }
}
