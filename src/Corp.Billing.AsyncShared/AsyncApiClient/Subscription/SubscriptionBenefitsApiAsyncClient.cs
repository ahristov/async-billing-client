﻿using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.SubscriptionBenefits.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Subscription
{
    public class SubscriptionBenefitsApiAsyncClient : BaseApiAsyncClient, ISubscriptionBenefitsApiAsyncClient
    {
        public SubscriptionBenefitsApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

        public async Task<BenefitsCountNewResponse> GetCountOfNewSubscriptionBenefitsAsync(BenefitsStatusRequest req, CancellationToken? cancellationToken = null)
        {
            return await GetCountOfNewSubscriptionBenefitsAsync(req, null, cancellationToken);
        }

        public async Task<BenefitsCountNewResponse> GetCountOfNewSubscriptionBenefitsAsync(BenefitsStatusRequest req, ICollection<int> acceptedFeatureTypes, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.SubscriptionBenefitsCountNew + "/?" + req.ToQueryString();

            string aftp = "" + GetAcceptedFeatureTypesParam(acceptedFeatureTypes);
            if (!string.IsNullOrWhiteSpace(aftp))
            {
                url = $"{url}&{aftp}";
            }

            var apiResult = await ApiCommand.GetAsync<BenefitsCountNewResponse>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<BenefitStatusResponse> GetSubscriptionBenefitsStatusAsync(BenefitsStatusRequest req, CancellationToken? cancellationToken = null)
        {
            return await GetSubscriptionBenefitsStatusAsync(req, null, cancellationToken);
        }

        public async Task<BenefitStatusResponse> GetSubscriptionBenefitsStatusAsync(BenefitsStatusRequest req, ICollection<int> acceptedFeatureTypes, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.SubscriptionBenefits + "/?" + req.ToQueryString();

            string aftp = "" + GetAcceptedFeatureTypesParam(acceptedFeatureTypes);
            if (!string.IsNullOrWhiteSpace(aftp))
            {
                url = $"{url}&{aftp}";
            }

            var apiResult = await ApiCommand.GetAsync<BenefitStatusResponse>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        private static string GetAcceptedFeatureTypesParam(ICollection<int> acceptedFeatureTypes)
        {
            if (acceptedFeatureTypes == null)
                return string.Empty;

            if (acceptedFeatureTypes.Count < 1)
                return string.Empty;

            var paramName = "acceptedFeatureTypes";
            var paramValue = JsonConvert.SerializeObject(acceptedFeatureTypes);

            var res = paramName.GetNameValueQueryParameterPairs(paramValue);
            return res;
        }

    }
}
