﻿using Corp.Billing.Shared.Domain.SubscriptionBenefits.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Subscription
{
    public interface ISubscriptionBenefitsApiAsyncClient
    {
        Task<BenefitsCountNewResponse> GetCountOfNewSubscriptionBenefitsAsync(BenefitsStatusRequest req, CancellationToken? cancellationToken = null);
        Task<BenefitsCountNewResponse> GetCountOfNewSubscriptionBenefitsAsync(BenefitsStatusRequest req, ICollection<int> acceptedFeatureTypes, CancellationToken? cancellationToken = null);
        Task<BenefitStatusResponse> GetSubscriptionBenefitsStatusAsync(BenefitsStatusRequest req, CancellationToken? cancellationToken = null);
        Task<BenefitStatusResponse> GetSubscriptionBenefitsStatusAsync(BenefitsStatusRequest req, ICollection<int> acceptedFeatureTypes, CancellationToken? cancellationToken = null);
    }
}
