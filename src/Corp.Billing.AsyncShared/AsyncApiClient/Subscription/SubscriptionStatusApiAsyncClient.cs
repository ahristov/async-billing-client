﻿using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.Promos;
using Corp.Billing.Shared.Domain.Subscription;
using Corp.Billing.Shared.Domain.UserFeatures;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Subscription
{
    public class SubscriptionStatusApiAsyncClient : BaseApiAsyncClient, ISubscriptionStatusApiAsyncClient
    {
        public SubscriptionStatusApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

        public async Task<CancelResult> CancelUserAsync(CancelRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.CancelProduct;
            var apiResult = await ApiCommand.PostAsync<CancelResult>(url, request);
            return apiResult.ResponseData;
        }

        public async Task<GetCurrentPackageResult> GetCurrentPackageAsync(GetCurrentPackageRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetCurrentPackage + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<GetCurrentPackageResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<List<DisplayUserStatusResult>> GetDisplayStatusAsync(DisplayUserStatusRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetDisplayStatusPath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<List<DisplayUserStatusResult>>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<DisplayUserStatusResponse> GetDisplayStatusV2Async(DisplayUserStatusRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetDisplayStatusPathV2 + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<DisplayUserStatusResponse>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<UserSubscriptionSummaryResult> GetSubscriptionSummaryAsync(UserSubscriptionSummaryRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetSubscriptionSummary + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<UserSubscriptionSummaryResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<GetUserBillingDataResult> GetUserBillingDataAsync(GetUserBillingDataRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserBillingData + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<GetUserBillingDataResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<UserFeatureResult> GetUserFeaturesAsync(UserFeatureRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserFeaturesPath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<UserFeatureResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<UserFeaturesLightResult> GetUserFeaturesLightAsync(UserFeaturesLightRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserFeaturesLightPath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<UserFeaturesLightResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<UserStatusDateResult> GetUserStatusDateAsync(UserStatusDateRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserStatusDatePath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<UserStatusDateResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<UserStatusDateResult> GetUserStatusDateV1Async(UserStatusDateRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserStatusDateV1 + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<UserStatusDateResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<UserPromoInfoResult> GetUserStatusPromoInfoAsync(UserPromoInfoRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserPromoInfoPath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<UserPromoInfoResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<UserSubStatusResult> GetUserSubscritionStatusAsync(UserSubStatusRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserSubStatusPath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<UserSubStatusResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<UserSubStatusMessageResult> GetUserSubStatusMessageAsync(UserSubStatusMessageRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Subscription.GetUserSubStatusMessagePath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<UserSubStatusMessageResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }
    }
}
