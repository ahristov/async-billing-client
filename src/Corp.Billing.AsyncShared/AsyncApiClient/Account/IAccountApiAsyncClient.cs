﻿using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Account
{
    public interface IAccountApiAsyncClient
    {
        Task<Shared.Domain.Account.GetPaymentMethodResult> GetAccountPaymentMethodV2(Shared.Domain.Account.GetPaymentMethodRequest request, CancellationToken? cancellationToken = null);
    }
}
