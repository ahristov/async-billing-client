﻿using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Account
{
    public class AccountApiAsyncClient : BaseApiAsyncClient, IAccountApiAsyncClient
    {
        public AccountApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

        public async Task<Shared.Domain.Account.GetPaymentMethodResult> GetAccountPaymentMethodV2(Shared.Domain.Account.GetPaymentMethodRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Account.Payment.GetAccountPaymentMethodV2Path + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<Shared.Domain.Account.GetPaymentMethodResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }
    }
}
