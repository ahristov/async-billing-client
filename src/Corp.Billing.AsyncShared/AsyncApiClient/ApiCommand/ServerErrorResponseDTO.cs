﻿using System;

namespace Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand
{
    [Serializable]
    public class ServerErrorResponseDTO
    {
        public int Code { get; set; }
        public Guid ReferenceID { get; set;}
        public string Title { get; set; }
        public string Message { get; set; }
    }
}
