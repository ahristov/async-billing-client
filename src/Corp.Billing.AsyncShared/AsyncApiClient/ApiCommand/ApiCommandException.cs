﻿using System;
using System.Net;

namespace Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand
{
    public class ApiCommandException : Exception
    {
        public ApiCommandException(
            string message,
            string title,
            int errorCode,
            HttpStatusCode httpStatusCode,
            Guid? referenceID)
            : base(message)
        {
            Title = title;
            ErrorCode = errorCode;
            HttpStatusCode = httpStatusCode;
            ReferenceID = referenceID;
        }

        public string Title { get; set; }
        public int ErrorCode { get; }
        public HttpStatusCode HttpStatusCode { get; }
        public Guid? ReferenceID { get; set; }
    }
}
