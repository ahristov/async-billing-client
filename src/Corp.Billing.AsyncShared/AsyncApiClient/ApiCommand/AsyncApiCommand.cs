﻿using Corp.Billing.Shared.ApiClient;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Threading;
using System;
using System.Linq;

namespace Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand
{
    public class AsyncApiCommand : IAsyncApiCommand
    {
        protected static readonly HttpClient Client;

        public static ExceptionLevelEnum ExceptionLevel = ExceptionLevelEnum.ClientErrors
            | ExceptionLevelEnum.ServerErrors;

        public static DebugLevelEnum DebugLevel = DebugLevelEnum.None;
        public static Func<HttpRequestMessage, object, HttpResponseMessage, object, ServerErrorResponseDTO, Task> DebugCallback = DefaultDebugCallback.DebugCallback;

        static AsyncApiCommand()
        {
            Client = new HttpClient();
            Client.DefaultRequestHeaders.Host = ApiConfiguration.BillingServiceHost;
        }

        public async Task<ApiCommandResponse<TResponseData>> GetAsync<TResponseData>(string url,
            CancellationToken? cancellationToken = null)
            => await SendAsync<TResponseData>(HttpMethod.Get, url);

        public async Task<ApiCommandResponse<TResponseData>> PostAsync<TResponseData>(string url, object data,
            CancellationToken? cancellationToken = null)
            => await SendAsync<TResponseData>(HttpMethod.Post, url, data, cancellationToken);

        public async Task<ApiCommandResponse<TResponseData>> PutAsync<TResponseData>(string url, object data,
            CancellationToken? cancellationToken = null)
            => await SendAsync<TResponseData>(HttpMethod.Put, url, data, cancellationToken);

        private async Task<ApiCommandResponse<TResponseData>> SendAsync<TResponseData>(
            HttpMethod httpMethod,
            string url,
            object requestData = null,
            CancellationToken? cancellationToken = null,
            string mediaType = "application/json")
        {
            var httpContent = requestData != null
                ? CreateHttpContent(requestData, mediaType)
                : null;

            HttpRequestMessage httpRequestMessage = new HttpRequestMessage
            {
                Content = httpContent,
                Method = httpMethod,
                RequestUri = new Uri(url),
            };

            var httpResponseMessage = await (cancellationToken != null
                ? Client.SendAsync(httpRequestMessage, cancellationToken.Value)
                : Client.SendAsync(httpRequestMessage));

            var responseStatusCode = httpResponseMessage.StatusCode;
            var responseMessage = httpResponseMessage.ReasonPhrase;
            var responseContentString = await httpResponseMessage.Content.ReadAsStringAsync();
            var responseData = JsonConvert.DeserializeObject<TResponseData>(
                responseContentString,
                ApiConfiguration.JsonSerializerSettings);
            var serverErrorResponseData = IsErrorResponseCode(responseStatusCode)
                ? JsonConvert.DeserializeObject<ServerErrorResponseDTO>(
                    responseContentString,
                    ApiConfiguration.JsonSerializerSettings)
                : null;

            var res = new ApiCommandResponse<TResponseData>
            {
                ResponseStatusCode = responseStatusCode,
                ResponseMessage = responseMessage,
                ResponseData = responseData,
            };

            if (DebugCallback != null && ShouldLogDebug(responseStatusCode))
            {
                await DebugCallback(httpRequestMessage, requestData, httpResponseMessage, responseData, serverErrorResponseData);
            }

            if (ShouldThrowException(responseStatusCode))
            {
                throw new ApiCommandException
                (
                    serverErrorResponseData?.Message ?? responseMessage,
                    serverErrorResponseData?.Title ?? responseMessage,
                    serverErrorResponseData?.Code ?? 0,
                    responseStatusCode,
                    serverErrorResponseData?.ReferenceID
                );
            }

            return res;
        }

        private HttpContent CreateHttpContent(object data, string mediaType = "application/json")
        {
            var jsonString = JsonConvert.SerializeObject(data, ApiConfiguration.JsonSerializerSettings);
            var contentBuffer = System.Text.Encoding.UTF8.GetBytes(jsonString);

            var byteContent = new ByteArrayContent(contentBuffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue(mediaType);

            return byteContent;
        }

        private bool IsErrorResponseCode(System.Net.HttpStatusCode statusCode)
        {
            var res = statusCode >= System.Net.HttpStatusCode.BadRequest
                && statusCode < (System.Net.HttpStatusCode)600;
            return res;
        }

        private bool ShouldThrowException(System.Net.HttpStatusCode statusCode)
        {
            if ((ExceptionLevel & ExceptionLevelEnum.ClientErrors) != 0
                && statusCode >= System.Net.HttpStatusCode.BadRequest
                && statusCode < System.Net.HttpStatusCode.InternalServerError)
                return true;

            if ((ExceptionLevel & ExceptionLevelEnum.ServerErrors) != 0
                && statusCode >= System.Net.HttpStatusCode.InternalServerError
                && statusCode < (System.Net.HttpStatusCode)600)
                return true;

            return false;
        }

        private bool ShouldLogDebug(System.Net.HttpStatusCode statusCode)
        {
            if (DebugCallback == null
                || DebugLevel == DebugLevelEnum.None)
                return false;

            if ((DebugLevel & DebugLevelEnum.InformationalResponses) != 0
                && statusCode >= System.Net.HttpStatusCode.Continue
                && statusCode < System.Net.HttpStatusCode.OK)
                return true;
            if ((DebugLevel & DebugLevelEnum.SuccessfulResponses) != 0
                && statusCode >= System.Net.HttpStatusCode.OK
                && statusCode < System.Net.HttpStatusCode.Ambiguous)
                return true;
            if ((DebugLevel & DebugLevelEnum.Redirects) != 0
                && statusCode >= System.Net.HttpStatusCode.Ambiguous
                && statusCode < System.Net.HttpStatusCode.BadRequest)
                return true;
            if ((DebugLevel & DebugLevelEnum.ClientErrors) != 0
                && statusCode >= System.Net.HttpStatusCode.BadRequest
                && statusCode < System.Net.HttpStatusCode.InternalServerError)
                return true;
            if ((DebugLevel & DebugLevelEnum.ServerErrors) != 0
                && statusCode >= System.Net.HttpStatusCode.InternalServerError
                && statusCode < (System.Net.HttpStatusCode)600)
                return true;

            return false;
        }

        [Flags]
        public enum ExceptionLevelEnum : short
        {
            None = 0,
            /// <summary>
            /// Client errors (400–499)
            /// </summary>
            ClientErrors = 8,
            /// <summary>
            /// Server errors (500–599)
            /// </summary>
            ServerErrors = 16,
        }

        [Flags]
        public enum DebugLevelEnum : short
        {
            None = 0,
            /// <summary>
            /// Log Informational responses (100–199)
            /// </summary>
            InformationalResponses = 1,
            /// <summary>
            /// Successful responses (200–299)
            /// </summary>
            SuccessfulResponses = 2,
            /// <summary>
            /// Log Redirects (300–399)
            /// </summary>
            Redirects = 4,
            /// <summary>
            /// Client errors (400–499)
            /// </summary>
            ClientErrors = 8,
            /// <summary>
            /// Server errors (500–599)
            /// </summary>
            ServerErrors = 16,
        }

        public class DefaultDebugCallback
        {
            public static Task DebugCallback(
                HttpRequestMessage requestMessage,
                object requestData,
                HttpResponseMessage responseMessage,
                object responseData,
                ServerErrorResponseDTO serverErrorResponseData)
            {
                Console.WriteLine($"HTTP/{requestMessage.Version} {requestMessage.Method} {requestMessage.RequestUri.AbsoluteUri}");
                PrintHeaders(requestMessage.Headers);
                Console.WriteLine();
                PrintDataAsJson(requestData);
                Console.WriteLine();

                Console.WriteLine($"{Convert.ToInt32(responseMessage?.StatusCode)} {responseMessage?.ReasonPhrase}");
                PrintHeaders(responseMessage.Headers);
                Console.WriteLine();
                PrintDataAsJson(serverErrorResponseData ?? responseData);
                Console.WriteLine();

                return Task.CompletedTask;
            }

            private static void PrintHeaders(HttpHeaders headers)
            {
                foreach (var hdr in headers)
                {
                    Console.WriteLine($"{hdr.Key}: {string.Join(",", hdr.Value)}");
                }
            }
            private static void PrintDataAsJson(object data)
            {
                if (data != null)
                {
                    var dataType = data.GetType();
                    var genericArgs = dataType.GenericTypeArguments;
                    if (genericArgs != null && genericArgs.Length > 0)
                    {
                        var typedArgs = string.Join(",", genericArgs.Select(x => x.Name));
                        Console.Write($"{dataType.Name}<{typedArgs}> = ");
                    }
                    else
                    {
                        Console.Write($"{dataType.Name} = ");
                    }

                    Console.WriteLine(JsonConvert.SerializeObject(data, ApiConfiguration.JsonSerializerSettings));
                }
            }

        }

    }
}
