﻿using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand
{
    public interface IAsyncApiCommand
    {
        Task<ApiCommandResponse<TResponseData>> GetAsync<TResponseData>(string url,
            CancellationToken? cancellationToken = null);
        Task<ApiCommandResponse<TResponseData>> PostAsync<TResponseData>(string url, object data,
            CancellationToken? cancellationToken = null);
        Task<ApiCommandResponse<TResponseData>> PutAsync<TResponseData>(string url, object data,
            CancellationToken? cancellationToken = null);

    }
}
