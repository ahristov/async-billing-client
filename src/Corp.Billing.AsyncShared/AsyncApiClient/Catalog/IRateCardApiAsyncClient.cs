﻿using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Catalog
{
    public interface IRateCardApiAsyncClient
    {
        Task<Shared.Domain.RateCardV3.Services.GetExtendedRateCardResponse> GetExtendedRateCard(Shared.Domain.RateCardV3.Services.GetRateCardRequest req, CancellationToken? cancellationToken = null);
        Task<Shared.Domain.RateCardV3.Services.GetRateCardResponse> GetRateCard(Shared.Domain.RateCardV3.Services.GetRateCardRequest req, CancellationToken? cancellationToken = null);
        Task<Shared.Domain.RateCard.RateCardV2AddOnsResult> GetLegacyAddOnsRateCard(Shared.Domain.RateCard.RateCardV2AddOnsRequest request, CancellationToken? cancellationToken = null);
    }
}
