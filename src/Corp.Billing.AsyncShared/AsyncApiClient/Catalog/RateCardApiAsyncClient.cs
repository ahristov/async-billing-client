﻿using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.RateCard;
using Corp.Billing.Shared.Domain.RateCardV3.Services;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Catalog
{
    public class RateCardApiAsyncClient : BaseApiAsyncClient, IRateCardApiAsyncClient
    {
        public RateCardApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

        public async Task<GetExtendedRateCardResponse> GetExtendedRateCard(GetRateCardRequest req, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetExtendedRateCardV3 + "/?" + req.ToQueryString();

            url = url
                .Replace("{catalogType}", req.CatalogTypeParam.ToString().ToLowerInvariant())
                .Replace("{catalogSource}", req.CatalogSourceParam.ToString().ToLowerInvariant());

            var apiResult = await ApiCommand.GetAsync<GetExtendedRateCardResponse>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<RateCardV2AddOnsResult> GetLegacyAddOnsRateCard(RateCardV2AddOnsRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetRateCardV2AddOnsPath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<RateCardV2AddOnsResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<GetRateCardResponse> GetRateCard(GetRateCardRequest req, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.RateCard.GetRateCardV3 + "/?" + req.ToQueryString();

            url = url
                .Replace("{catalogType}", req.CatalogTypeParam.ToString().ToLowerInvariant())
                .Replace("{catalogSource}", req.CatalogSourceParam.ToString().ToLowerInvariant());

            var apiResult = await ApiCommand.GetAsync<GetRateCardResponse>(url, cancellationToken);
            return apiResult.ResponseData;
        }
    }
}

