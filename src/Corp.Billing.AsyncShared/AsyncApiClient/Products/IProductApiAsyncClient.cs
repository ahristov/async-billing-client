﻿using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;
using Corp.Billing.Shared.Domain.DateCoachStatus.Data;
using Corp.Billing.Shared.Domain.Products;
using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Products
{
    /// <summary>
    /// This interface is the async version for the following synchronous interfaces:
    /// <see cref="Shared.Domain.Products.IProductApiClient"/>
    /// <see cref="Shared.Domain.Products.ImpulseFeatures"/>
    /// </summary>
    public interface IProductApiAsyncClient
    {
        // MatchMe
        Task<MatchMe30DayEligibilityCheckResult> MatchMe30DayEligibilityCheckAsync(MatchMe30DayEligibilityCheckRequest request, CancellationToken? cancellationToken = null);
        Task<PutMeInUserListResult> PutMeInUserListAsync(PutMeInUserListRequest request, CancellationToken? cancellationToken = null);
        // Boost
        Task<TopSpotStatusResult> GetTopSpotStatusAsync(TopSpotStatusRequest request, CancellationToken? cancellationToken = null);
        Task<ProvisionAndActivateTopSpotResult> ProvisionAndActivateTopSpotAsync(ProvisionAndActivateTopSpotRequest request, CancellationToken? cancellationToken = null);
        Task<ActivateUnclaimedImpulseFeatureResult> ActivateUnclaimedImpulseFeatureAsync(ActivateUnclaimedImpulseFeatureRequest request, CancellationToken? cancellationToken = null);
        // ProfilePro
        Task<CSAUserPCSStatusResult> GetUnitUserStatusPcsAsync(CSAUserPCSStatusRequest request, CancellationToken? cancellationToken = null);
        // ProfilePro Redemptions
        Task RedeemProfileProAsync(Shared.Domain.ProfileProLite.ProfileProLiteRedemption request, CancellationToken? cancellationToken = null);
        Task<Shared.Domain.ProfileProLite.ProfileProLiteRedemptionStatus> GetProfileProRedemptionsAsync(Shared.Domain.ProfileProLite.GetProfileProLiteRedemptionRequest request, CancellationToken? cancellationToken = null);
        // Unit products
        Task<UnitUserStatusResult> GetUnitUserStatusAsync(UnitUserStatusRequest request, CancellationToken? cancellationToken = null);
        Task<UnitDisplayStatusResult> GetUnitDisplayStatusAsync(UnitDisplayStatusRequest request, CancellationToken? cancellationToken = null);
        // Impulse features
        Task<GetStatusResult> GetImpulseFeaturesStatusAsync(GetStatusRequest request, CancellationToken? cancellationToken = null);
        Task<ActivateResult> ActivateImpulseFeatureAsync(ActivateRequest request, CancellationToken? cancellationToken = null);
        // Date coach
        Task<DateCoachStatusResponse> GetDateCoachStatusAsync(int userId, CancellationToken? cancellationToken = null);
        Task<DateCoachRenewalToggleResponse> ActivateDateCoachAutoRenewalAsync(int userId, CancellationToken? cancellationToken = null);
        Task<DateCoachRenewalToggleResponse> DeactivateDateCoachAutoRenewalAsync(int userId, CancellationToken? cancellationToken = null);
        Task<DateCoachRenewalToggleResponse> ToggleDateCoachAutoRenewalAsync(int userId, CancellationToken? cancellationToken = null);
    }
}
