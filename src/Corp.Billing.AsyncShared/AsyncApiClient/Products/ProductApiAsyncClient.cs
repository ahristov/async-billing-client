﻿using Corp.Billing.AsyncShared.AsyncApiClient.ApiCommand;
using Corp.Billing.Shared.ApiPaths;
using Corp.Billing.Shared.Domain;
using Corp.Billing.Shared.Domain.DateCoachRenewalToggle.Data;
using Corp.Billing.Shared.Domain.DateCoachStatus.Data;
using Corp.Billing.Shared.Domain.Products;
using Corp.Billing.Shared.Domain.Products.ImpulseFeatures.Data;
using Corp.Billing.Shared.Domain.ProfileProLite;
using System.Threading;
using System.Threading.Tasks;

namespace Corp.Billing.AsyncShared.AsyncApiClient.Products
{
    public class ProductApiAsyncClient : BaseApiAsyncClient, IProductApiAsyncClient
    {
        public ProductApiAsyncClient(IAsyncApiCommand apiCommand) : base(apiCommand) { }

        public async Task<MatchMe30DayEligibilityCheckResult> MatchMe30DayEligibilityCheckAsync(MatchMe30DayEligibilityCheckRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Products.MatchMe.GetMatchMe30DayEligibilityCheck + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<MatchMe30DayEligibilityCheckResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<PutMeInUserListResult> PutMeInUserListAsync(PutMeInUserListRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Products.MatchMe.PutMeInUserList;
            var apiResult = await ApiCommand.PostAsync<PutMeInUserListResult>(url, request, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<TopSpotStatusResult> GetTopSpotStatusAsync(TopSpotStatusRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Products.TopSpot.GetTopSpotStatusPath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<TopSpotStatusResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<ProvisionAndActivateTopSpotResult> ProvisionAndActivateTopSpotAsync(ProvisionAndActivateTopSpotRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Products.TopSpot.ProvisionAndActivateTopSpot;
            var apiResult = await ApiCommand.PostAsync<ProvisionAndActivateTopSpotResult>(url, request, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<ActivateUnclaimedImpulseFeatureResult> ActivateUnclaimedImpulseFeatureAsync(ActivateUnclaimedImpulseFeatureRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Products.TopSpot.ActivateUnclaimedImpulseFeature;
            var apiResult = await ApiCommand.PutAsync<ActivateUnclaimedImpulseFeatureResult>(url, request, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<CSAUserPCSStatusResult> GetUnitUserStatusPcsAsync(CSAUserPCSStatusRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Products.ProfilePro.GetUnitUserStatusPcsPath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<CSAUserPCSStatusResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<UnitUserStatusResult> GetUnitUserStatusAsync(UnitUserStatusRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Products.UnitProducts.GetUnitUserStatusPath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<UnitUserStatusResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<UnitDisplayStatusResult> GetUnitDisplayStatusAsync(UnitDisplayStatusRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Products.UnitProducts.GetUnitDisplayStatusPath + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<UnitDisplayStatusResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<GetStatusResult> GetImpulseFeaturesStatusAsync(GetStatusRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Products.ImpulseFeatures.StatusAndActivate + "/?" + request.ToQueryString();
            url = url.Replace("{productFeature}", request.ProductFeature.ToString().ToLowerInvariant());
            var apiResult = await ApiCommand.GetAsync<GetStatusResult>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<ActivateResult> ActivateImpulseFeatureAsync(ActivateRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.Products.ImpulseFeatures.StatusAndActivate;
            url = url.Replace("{productFeature}", request.ProductFeature.ToString().ToLowerInvariant());
            var apiResult = await ApiCommand.PutAsync<ActivateResult>(url, request, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task RedeemProfileProAsync(ProfileProLiteRedemption request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.ProfileProLite.Redemption;
            await ApiCommand.PostAsync<ActivateResult>(url, request, cancellationToken);
        }

        public async Task<ProfileProLiteRedemptionStatus> GetProfileProRedemptionsAsync(GetProfileProLiteRedemptionRequest request, CancellationToken? cancellationToken = null)
        {
            string url = ServiceUrl + ApiPathsV1.ProfileProLite.Redemption + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<ProfileProLiteRedemptionStatus>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<DateCoachStatusResponse> GetDateCoachStatusAsync(int userId, CancellationToken? cancellationToken = null)
        {
            var request = new DateCoachStatusRequest { UserId = userId };
            string url = ServiceUrl + ApiPathsV1.Products.DateCoach.GetDateCoachStatus + "/?" + request.ToQueryString();
            var apiResult = await ApiCommand.GetAsync<DateCoachStatusResponse>(url, cancellationToken);
            return apiResult.ResponseData;
        }

        public async Task<DateCoachRenewalToggleResponse> ActivateDateCoachAutoRenewalAsync(int userId, CancellationToken? cancellationToken = null)
        {
            var res = await ToggleAutoRenewalWorker(userId, DateCoachRenewalToggleRequestType.Activate);
            return res;
        }

        public async Task<DateCoachRenewalToggleResponse> DeactivateDateCoachAutoRenewalAsync(int userId, CancellationToken? cancellationToken = null)
        {
            var res = await ToggleAutoRenewalWorker(userId, DateCoachRenewalToggleRequestType.Deactivate);
            return res;
        }

        public async Task<DateCoachRenewalToggleResponse> ToggleDateCoachAutoRenewalAsync(int userId, CancellationToken? cancellationToken = null)
        {
            var res = await ToggleAutoRenewalWorker(userId, DateCoachRenewalToggleRequestType.Toggle);
            return res;
        }

        private async Task<DateCoachRenewalToggleResponse> ToggleAutoRenewalWorker(int userId, DateCoachRenewalToggleRequestType toggleType, CancellationToken? cancellationToken = null)
        {
            var request = new DateCoachRenewalToggleRequest { UserId = userId, ToggleType = toggleType };

            string url = ServiceUrl + ApiPathsV1.Products.DateCoach.ToggleDateCoachRenewal;
            url = url
                .Replace("{toggleType}", request.ToggleType.ToString().ToLowerInvariant());

            var res = await ApiCommand.PutAsync<DateCoachRenewalToggleResponse>(url, request, cancellationToken);
            return res.ResponseData;
        }

    }
}
